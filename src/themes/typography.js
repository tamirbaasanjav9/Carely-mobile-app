import {moderateScale} from '../common/constants';

// App Font-Family:
const fontWeights = {
  Regular: {
    fontFamily: 'SFPRODisplay-Regular',
  },
  Medium: {
    fontFamily: 'SFPRODisplay-Medium',
  },
  SemiBold: {
    fontFamily: 'Urbanist-SemiBold',
  },
  Bold: {
    fontFamily: 'SFPRODisplay-Bold',
  },
};

// App font sizes:
const fontSizes = {
  f5: {
    fontSize: moderateScale(5),
  },
  f11: {
    fontSize: moderateScale(11),
  },
  f12: {
    fontSize: moderateScale(12),
  },
  f13: {
    fontSize: moderateScale(13),
  },
  f14: {
    fontSize: moderateScale(14),
  },
  f15: {
    fontSize: moderateScale(15),
  },
  f16: {
    fontSize: moderateScale(16),
  },
  f17: {
    fontSize: moderateScale(17),
  },
  f18: {
    fontSize: moderateScale(18),
  },
  f20: {
    fontSize: moderateScale(20),
  },
  f22: {
    fontSize: moderateScale(20),
  },
  f24: {
    fontSize: moderateScale(24),
  },
  f26: {
    fontSize: moderateScale(26),
  },
  f28: {
    fontSize: moderateScale(28),
  },
  f30: {
    fontSize: moderateScale(30),
  },
  f32: {
    fontSize: moderateScale(32),
  },
  f34: {
    fontSize: moderateScale(34),
  },
  f35: {
    fontSize: moderateScale(35),
  },
  f36: {
    fontSize: moderateScale(36),
  },
  f40: {
    fontSize: moderateScale(40),
  },
  f46: {
    fontSize: moderateScale(46),
  },
  f66: {
    fontSize: moderateScale(66),
  },
};

const typography = {fontWeights, fontSizes};

export default typography;
