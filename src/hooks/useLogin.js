import {useState, useContext} from 'react';
import {yupResolver} from '@hookform/resolvers/yup';
import {useNavigation} from '@react-navigation/native';
import {loginSchema} from '../common/formSchema';
// import {handlerFailed, validationText} from '../commons/message';
import {useForm} from 'react-hook-form';
import {AuthContext} from '../contexts/authContext';
import {saveToken, saveUserId} from '../utils/asyncstorage';

const useLogin = () => {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({resolver: yupResolver(loginSchema)});
  const [loading, setloading] = useState(false);
  const {
    handlers: {LOGIN, signIn},
  } = useContext(AuthContext);
  const navigation = useNavigation();

  const onHandler = values => {
    console.log(values);
    try {
      setloading(true);
      LOGIN(`/customer_login`, values)
        .then(res => {
          if (res?.status === 200) {
            signIn({token: res?.data?.token, user: res?.data?.user_id});
            saveToken(res?.data?.token);
            // navigation.navigate('HomeScreen');
            saveUserId(res?.data?.user_id);
            //  setAsyncStorageData(ACCESS_TOKEN, 'access_token');
            navigation.reset({
              index: 0,
              routes: [
                {
                  name: StackNav.TabBar,
                },
              ],
            });
          }
        })
        .catch(e => {
          return validationText(e?.response?.data?.response_msg);
        })
        .finally(e => {
          setloading(false);
        });
    } catch (e) {
      setloading(false);
      //   handlerFailed();
    }
  };
  return {onHandler, loading, control, handleSubmit, errors};
};
export default useLogin;
