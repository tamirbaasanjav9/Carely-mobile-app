import {StatusBar} from 'react-native';
import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import AppNavigator from './navigation';
import {styles} from './themes';
import CSafeAreaView from './components/common/CSafeAreaView';
import {Api} from './api/Api';
import {AuthContext} from './contexts/authContext';
import {toastConfig} from './common/toast';
import Toast from 'react-native-toast-message';
import {getSavedToken} from './utils/asyncstorage';

const App = () => {
  const {handlers, state} = Api();
  const colors = useSelector(state => state.theme.theme);

  useEffect(() => {
    getSavedToken()?.then(token => {
      handlers.setSensetiveToken(token);
    });
  }, [state?.userToken]);

  return (
    <AuthContext.Provider value={{handlers, state}}>
      <CSafeAreaView style={styles.flex}>
        <StatusBar
          barStyle={colors.dark === 'dark' ? 'light-content' : 'dark-content'}
        />
        <AppNavigator />
      </CSafeAreaView>
      <Toast config={toastConfig} />
    </AuthContext.Provider>
  );
};

export default App;
