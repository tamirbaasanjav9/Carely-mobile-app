import {useContext} from 'react';
import {AuthContext} from './authContext';

export const useAuthContext = () => {
  const context = useContext(AuthContext);
  if (!context) throw new Error('aldaa');
  return context;
};
