const reducer = (state, action) => {
  console.log(state, 'reducer');
  console.log(action, 'checked action');

  switch (action.type) {
    case 'RESTORE_TOKEN':
      return {
        ...state,
        userToken: action.token,
        isLoading: false,
      };

    case 'SIGN_IN':
      return {
        ...state,
        userToken: action.payload.token,
        loggedUser: action.payload.data,
      };
    case 'SET_INITIAL_TOKEN':
      return {
        ...state,
        userToken: action.payload.token,
        loggedUser: action.payload.data,
      };

    case 'REGISTER_UPDATE_DATA':
      return {
        ...state,
        registerUser: action.payload,
      };

    case 'REGISTER_USER_UPDATE_DATA':
      return {
        ...state,
        registerUser: {
          ...state.registerUser,
          [action.payload.type]: action.payload.data,
        },
      };

    case 'LOGGED_USER_DATA':
      return {
        ...state,
        loggedUser: action.payload,
      };

    case 'LOGGED_USER_UPDATE_DATA':
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          [action.payload.type]: action.payload.data,
        },
      };

    case 'SIGN_OUT':
      return {
        ...state,
        userToken: null,
        user: null,
        registerUser: null,
        loggedUser: null,
      };
    default:
      return state;
  }
};

export default reducer;
