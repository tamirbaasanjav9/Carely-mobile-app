// useApi.js

import {useState, useContext} from 'react';
import {AuthContext} from './authContext';

const useService = () => {
  const {handlers} = useContext(AuthContext);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async (
    method,
    endpoint,
    token,
    requestData,
    pagination,
  ) => {
    setIsLoading(true);
    try {
      let response;

      if (method === 'GET') {
        response = await handlers.GETPROD(endpoint, token);
      } else if (method === 'POST') {
        response = await handlers.POSTGENERAL(endpoint, token, requestData);
      } else if (method === 'GETUSER') {
        response = await handlers.GETUSER(endpoint, token);
      } else if (method === 'POSTUSER') {
        response = await handlers.POST(endpoint, token, requestData);
      } else if (method === 'POSTBASE') {
        response = await handlers.POSTBASE(endpoint);
      }

      if (!!pagination) {
        setData(prevData => [...prevData, ...response?.data]);
      }
      setData(response?.data);
    } catch (err) {
      console.log(err);
      setError(err);
    } finally {
      setIsLoading(false);
    }
  };

  return {data, error, isLoading, fetchData};
};

export default useService;
