import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {StackRoute} from '../NavigationRoutes';
import {StackNav} from '../NavigationKeys';
import AuthStack from './AuthStack';
import SelectStack from './SelectStack';
import RegisterStack from './RegisterStack';

const Stack = createNativeStackNavigator();

export default function StackNavigation() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={StackNav.Splash}>
      <Stack.Screen name={StackNav.Splash} component={StackRoute.Splash} />
      <Stack.Screen
        name={StackNav.WelcomeScreen}
        component={StackRoute.WelcomeScreen}
      />
      <Stack.Screen
        name={StackNav.onBoarding}
        component={StackRoute.OnBoarding}
      />
      <Stack.Screen name={StackNav.SelectStack} component={SelectStack} />
      <Stack.Screen name={StackNav.RegisterStack} component={RegisterStack} />
      <Stack.Screen name={StackNav.Auth} component={AuthStack} />
      <Stack.Screen name={StackNav.TabBar} component={StackRoute.TabBar} />
      <Stack.Screen
        name={StackNav.SetUpProfile}
        component={StackRoute.SetUpProfile}
      />
      <Stack.Screen
        name={StackNav.AddAddress}
        component={StackRoute.AddAddress}
      />
      <Stack.Screen
        name={StackNav.AddNewCard}
        component={StackRoute.AddNewCard}
      />
      <Stack.Screen name={StackNav.Address} component={StackRoute.Address} />
      <Stack.Screen
        name={StackNav.HelpCenter}
        component={StackRoute.HelpCenter}
      />
      <Stack.Screen name={StackNav.Language} component={StackRoute.Language} />
      <Stack.Screen
        name={StackNav.NotificationSetting}
        component={StackRoute.NotificationSetting}
      />
      <Stack.Screen name={StackNav.Payment} component={StackRoute.Payment} />
      <Stack.Screen
        name={StackNav.PrivacyPolicy}
        component={StackRoute.PrivacyPolicy}
      />
      <Stack.Screen name={StackNav.Security} component={StackRoute.Security} />
      <Stack.Screen
        name={StackNav.CreateNewPassword}
        component={StackRoute.CreateNewPassword}
      />
      <Stack.Screen name={StackNav.SetPin} component={StackRoute.SetPin} />
      <Stack.Screen
        name={StackNav.SetValidation}
        component={StackRoute.SetValidation}
      />
      <Stack.Screen
        name={StackNav.InviteFriends}
        component={StackRoute.InviteFriends}
      />
      <Stack.Screen
        name={StackNav.CustomerService}
        component={StackRoute.CustomerService}
      />
      <Stack.Screen name={StackNav.EReceipt} component={StackRoute.EReceipt} />
      <Stack.Screen
        name={StackNav.TopUpEWallet}
        component={StackRoute.TopUpEWallet}
      />
      <Stack.Screen
        name={StackNav.TransactionHistory}
        component={StackRoute.TransactionHistory}
      />
      <Stack.Screen name={StackNav.OnGoing} component={StackRoute.OnGoing} />
      <Stack.Screen
        name={StackNav.PostFirstCate}
        component={StackRoute.PostFirstCate}
      />
      <Stack.Screen
        name={StackNav.PostSecCate}
        component={StackRoute.PostSecCate}
      />
      <Stack.Screen
        name={StackNav.PostThirdCate}
        component={StackRoute.PostThirdCate}
      />
      <Stack.Screen
        name={StackNav.PostFourthCate}
        component={StackRoute.PostFourthCate}
      />
      <Stack.Screen
        name={StackNav.PostFifthCate}
        component={StackRoute.PostFifthCate}
      />
      <Stack.Screen name={StackNav.Posting} component={StackRoute.Posting} />
      <Stack.Screen
        name={StackNav.Qualifications}
        component={StackRoute.Qualifications}
      />

      <Stack.Screen
        name={StackNav.Responsibilities}
        component={StackRoute.Responsibilities}
      />
      <Stack.Screen
        name={StackNav.PostingReview}
        component={StackRoute.PostingReview}
      />

      <Stack.Screen
        name={StackNav.AboutService}
        component={StackRoute.AboutService}
      />
      <Stack.Screen
        name={StackNav.HourlyRate}
        component={StackRoute.HourlyRate}
      />
      <Stack.Screen
        name={StackNav.Completed}
        component={StackRoute.Completed}
      />
      <Stack.Screen
        name={StackNav.TrackOrder}
        component={StackRoute.TrackOrder}
      />
      <Stack.Screen
        name={StackNav.MostPopular}
        component={StackRoute.MostPopular}
      />
      <Stack.Screen
        name={StackNav.MyWishlist}
        component={StackRoute.MyWishlist}
      />
      <Stack.Screen
        name={StackNav.Notification}
        component={StackRoute.Notification}
      />
      <Stack.Screen
        name={StackNav.CompanyDetail}
        component={StackRoute.CompanyDetail}
      />
      <Stack.Screen
        name={StackNav.SpecialOffers}
        component={StackRoute.SpecialOffers}
      />
      <Stack.Screen
        name={StackNav.ProductCategory}
        component={StackRoute.ProductCategory}
      />
      <Stack.Screen
        name={StackNav.ProductCategorySecond}
        component={StackRoute.ProductCategorySecond}
      />
      <Stack.Screen
        name={StackNav.SelectedTypeScreen}
        component={StackRoute.SelectedTypeScreen}
      />
      <Stack.Screen
        name={StackNav.ProductCategoryThird}
        component={StackRoute.ProductCategoryThird}
      />
      <Stack.Screen
        name={StackNav.ProductDetail}
        component={StackRoute.ProductDetail}
      />
      <Stack.Screen
        name={StackNav.ShopDetail}
        component={StackRoute.ShopDetail}
      />
      <Stack.Screen name={StackNav.Reviews} component={StackRoute.Reviews} />
      <Stack.Screen name={StackNav.CheckOut} component={StackRoute.CheckOut} />
      <Stack.Screen
        name={StackNav.FilterScreen}
        component={StackRoute.FilterScreen}
      />
      <Stack.Screen
        name={StackNav.FilterCategory}
        component={StackRoute.FilterCategory}
      />
      <Stack.Screen
        name={StackNav.ChooseShipping}
        component={StackRoute.ChooseShipping}
      />
      <Stack.Screen name={StackNav.AddPromo} component={StackRoute.AddPromo} />
    </Stack.Navigator>
  );
}
