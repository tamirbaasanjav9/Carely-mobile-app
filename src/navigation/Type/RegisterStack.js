import React from 'react';
import {StackNav} from '../NavigationKeys';
import {StackRoute} from '../NavigationRoutes';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function RegisterStack({route}) {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={StackNav.Register}>
      <Stack.Screen
        name={StackNav.Register}
        component={StackRoute.Register}
        initialParams={route?.params}
      />
    </Stack.Navigator>
  );
}
