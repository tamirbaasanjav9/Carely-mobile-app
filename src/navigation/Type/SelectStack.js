import React from 'react';
import {StackNav} from '../NavigationKeys';
import {StackRoute} from '../NavigationRoutes';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function SelectStack() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={StackNav.Type}>
      <Stack.Screen name={StackNav.Type} component={StackRoute.Type} />
      <Stack.Screen name={StackNav.Location} component={StackRoute.Location} />
      <Stack.Screen name={StackNav.When} component={StackRoute.When} />
      <Stack.Screen name={StackNav.Interest} component={StackRoute.Interest} />
    </Stack.Navigator>
  );
}
