// Tab Routes
import HomeTab from '../containers/tabbar/home/HomeTab';
import CartTab from '../containers/tabbar/cart/CartTab';
import OrderTab from '../containers/tabbar/order/OrderTab';
import WalletTab from '../containers/tabbar/wallet/WalletTab';
import ProfileTab from '../containers/tabbar/profile/ProfileTab';
import Search from '../containers/tabbar/home/Search';

// // Screens Route
import Splash from '../containers/auth/Splash';
import FilterScreen from '../containers/tabbar/home/FilterScreen';
import FilterCategory from '../containers/tabbar/home/FilterCategory';

import WelcomeScreen from '../containers/WelcomeScreen';
import OnBoarding from '../containers/OnBoarding';
import Login from '../containers/auth/Login';
import Register from '../containers/auth/Register';
import TabBar from './Type/TabBarNavigation';
import Connect from '../containers/auth/Connect';
import SelectInterest from '../containers/auth/SelectInterest';
import SetPin from '../containers/auth/SetPin';
import SetValidation from '../containers/auth/SetValidation';
import SetUpProfile from '../containers/auth/SetUpProfile';
import SetSecure from '../containers/auth/SetSecure';
import ForgotPassword from '../containers/auth/ForgotPassword';
import ForgotPasswordOtp from '../containers/auth/ForgotPasswordOtp';
import CreateNewPassword from '../containers/auth/CreateNewPassword';
import Birthday from '../containers/auth/Birthday';
import Gender from '../containers/auth/Gender';
import AddAddress from '../containers/tabbar/profile/AddAddress';
import AddNewCard from '../containers/tabbar/profile/AddNewCard';
import Address from '../containers/tabbar/profile/Address';
import HelpCenter from '../containers/tabbar/profile/HelpCenter';
import Language from '../containers/tabbar/profile/Language';
import NotificationSetting from '../containers/tabbar/profile/NotificationSetting';
import Payment from '../containers/tabbar/profile/Payment';
import PrivacyPolicy from '../containers/tabbar/profile/PrivacyPolicy';
import Security from '../containers/tabbar/profile/Security';
import InviteFriends from '../containers/tabbar/profile/InviteFriends';
import CustomerService from '../containers/tabbar/profile/CustomerService';
import EReceipt from '../containers/tabbar/wallet/EReceipt';
import TopUpEWallet from '../containers/tabbar/wallet/TopUpEWallet';
import TransactionHistory from '../containers/tabbar/wallet/TransactionHistory';
import Completed from '../containers/tabbar/order/Completed';
import OnGoing from '../containers/tabbar/order/OnGoing';
import TrackOrder from '../containers/tabbar/order/TrackOrder';
import MostPopular from '../containers/tabbar/home/MostPopular';
import MyWishlist from '../containers/tabbar/home/MyWishlist';
import Notification from '../containers/tabbar/home/Notification';
import CompanyDetail from '../containers/tabbar/home/CompanyDetail';
import SpecialOffers from '../containers/tabbar/home/SpecialOffers';
import ProductDetail from '../containers/tabbar/home/ProductDetail';
import ProductCategory from '../containers/tabbar/home/ProductCategory';
import ProductCategorySecond from '../containers/tabbar/home/ProductCategorySecond';
import ProductCategoryThird from '../containers/tabbar/home/ProductCategoryThird';
import Reviews from '../containers/tabbar/home/Reviews';
import CheckOut from '../containers/tabbar/cart/CheckOut';
import AddPromo from '../containers/tabbar/cart/AddPromo';
import ChooseShipping from '../containers/tabbar/cart/ChooseShipping';
import ShopDetail from '../components/detailProduct/ShopDetail';
import SelectedTypeScreen from '../components/cartComponent/SelectedTypeScreen';
import PostFirstCate from '../components/cartComponent/PostingCategory/PostFirstCate';
import PostSecCate from '../components/cartComponent/PostingCategory/PostSecCate';
import PostThirdCate from '../components/cartComponent/PostingCategory/PostThirdCate';
import PostFourthCate from '../components/cartComponent/PostingCategory/PostFourthCate';
import PostFifthCate from '../components/cartComponent/PostingCategory/PostFifthCate';
import Posting from '../components/cartComponent/PostingCategory/Posting';
import AboutService from '../components/cartComponent/PostingCategory/postingComponents/postingPages/AboutService';
import HourlyRate from '../components/cartComponent/PostingCategory/postingComponents/postingPages/HourlyRate';
import Responsibilities from '../components/cartComponent/PostingCategory/postingComponents/postingPages/Responsibilities';
import Qualifications from '../components/cartComponent/PostingCategory/postingComponents/postingPages/Qualifications';
import PostingReview from '../components/cartComponent/PostingCategory/PostingReview/';

// type screen route
import Type from '../containers/type/Type';
import Location from '../containers/type/Location';
import When from '../containers/type/When';
import Interest from '../containers/type/Interest';

export const TabRoute = {
  HomeTab,
  CartTab,
  OrderTab,
  WalletTab,
  ProfileTab,
  Search,
};

export const StackRoute = {
  Splash,
  WelcomeScreen,
  OnBoarding,
  Login,
  Register,
  TabBar,
  Connect,
  SelectInterest,
  SetPin,
  SetUpProfile,
  SetSecure,
  ForgotPassword,
  ForgotPasswordOtp,
  CreateNewPassword,
  Birthday,
  Gender,
  AddAddress,
  AddNewCard,
  Address,
  HelpCenter,
  Language,
  NotificationSetting,
  Payment,
  PrivacyPolicy,
  Security,
  InviteFriends,
  CustomerService,
  EReceipt,
  TopUpEWallet,
  TransactionHistory,
  Completed,
  OnGoing,
  TrackOrder,
  MostPopular,
  MyWishlist,
  Notification,
  SpecialOffers,
  ProductDetail,
  ProductCategory,
  ProductCategorySecond,
  ProductCategoryThird,
  Reviews,
  CheckOut,
  AddPromo,
  ChooseShipping,
  SetValidation,
  ShopDetail,
  SelectedTypeScreen,
  PostFirstCate,
  PostSecCate,
  PostThirdCate,
  PostFourthCate,
  Posting,
  PostFifthCate,
  AboutService,
  HourlyRate,
  Responsibilities,
  Qualifications,
  PostingReview,
  Type,
  Location,
  When,
  Interest,
  CompanyDetail,
  FilterScreen,
  FilterCategory,
};
