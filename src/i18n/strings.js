import LocalizedStrings from 'react-native-localization';
import enI18n from './en';
import mnI18n from './mn';

export default strings = new LocalizedStrings({
  en: mnI18n,
});
