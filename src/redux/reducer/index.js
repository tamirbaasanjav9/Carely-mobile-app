import {combineReducers} from 'redux';
import theme from './theme';
import profile from './profile';
import userInfo from './userInfo';

const rootReducer = combineReducers({
  theme,
  profile,
  userInfo,
});

export default rootReducer;
