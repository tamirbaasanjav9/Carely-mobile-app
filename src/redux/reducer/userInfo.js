import {USER_DATA} from '../types';

const INITIAL_STATE = {
  name: '',
  email: '',
  phone_number: '',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        name: action.payload.name,
        email: action.payload.email,
        email: action.payload.phone_number,
      };
    default:
      return state;
  }
}
