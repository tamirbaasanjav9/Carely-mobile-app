import React from 'react';
import {BaseToast} from 'react-native-toast-message';
export const toastConfig = {
  //toast info тохиргоо
  info: ({text1, isType, ...rest}) => (
    <BaseToast
      {...rest}
      text1={text1}
      text2NumberOfLines={40}
      style={{
        borderLeftColor: '#e74c3c',
        zIndex: 999999,
      }}
    />
  ),
  //toast success тохиргоо
  success: ({text1, isType, ...rest}) => (
    <BaseToast
      {...rest}
      text1={text1}
      text2NumberOfLines={40}
      style={{
        borderLeftColor: '#27ae60',
        zIndex: 999999,
      }}
    />
  ),
  //toast error тохиргоо
  error: ({text1, isType, ...rest}) => (
    <BaseToast
      {...rest}
      text1={text1}
      text2NumberOfLines={40}
      style={{
        borderLeftColor: '#e74c3c',
        zIndex: 999999,
      }}
    />
  ),
};
