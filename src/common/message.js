import Toast from 'react-native-toast-message';

//app code дунд алдаа гарах үед
export const handlerFailed = () => {
  return Toast.show({
    type: 'error',
    text1: 'Амжилтгүй',
    text2: 'Та дахин оролдоно уу.',
  });
};

//back дээр алдаа гарах үед /back алдаатай байх эсүүл back лүү явуулж байгаа url буруу байх/
export const backFailed = () => {
  return Toast.show({
    type: 'info',
    text1: 'Амжилтгүй',
    text2: 'Хүсэлтийг боловсруулж чадсангүй.',
  });
};

//back validation алдаа өгөх үед (parameter дамжуулж)
export const validationText = text => {
  return Toast.show({
    type: 'info',
    text1: 'Амжилтгүй',
    text2: text,
    text2NumberOfLines: 4,
  });
};

//success message өгөх (parameter дамжуулж)
export const successText = text => {
  return Toast.show({
    type: 'success',
    text1: 'Амжилттай',
    text2: text,
    text2NumberOfLines: 4,
  });
};

//info message ийг өгөх үед
export const infoText = text => {
  return Toast.show({
    type: 'info',
    text1: 'Анхааруулга',
    text2: text,
  });
};
