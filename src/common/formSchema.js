import * as yup from 'yup';

//LOGIN schema
export const loginSchema = yup.object().shape({
  email: yup.string().required().lowercase(),
  password: yup.string().required(),
});

//EDIT data
export const editDataSchema = yup.object().shape({
  name: yup.string().required(),
  password: yup.string().required(),
  email: yup.string().required(),
  phone_number: yup.string().required(),
});
