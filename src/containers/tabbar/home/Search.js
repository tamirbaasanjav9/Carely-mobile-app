import {StyleSheet, View, TouchableOpacity, FlatList} from 'react-native';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {FlashList} from '@shopify/flash-list';
import {StackNav} from '../../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import CText from '../../../components/common/CText';
import strings from '../../../i18n/strings';

// components
import RenderNullComponent from '../../../components/RenderNullComponent';

// redux
import useService from '../../../contexts/useService';
import useBoolean from '../../../contexts/useBoolean';
import {AuthContext} from '../../../contexts/authContext';
import {deviceWidth, getHeight, moderateScale} from '../../../common/constants';

//  asyncStorage
import {getUserId} from '../../../utils/asyncstorage';

// styling imports
import {commonColor, styles} from '../../../themes';
import SearchedItem from '../../../components/homeComponent/SearchedItem';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import {useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {useFocusEffect} from '@react-navigation/native';
import SortAndFilter from '../../../components/models/SortAndFilter';
import {useRef} from 'react';
import CDivider from '../../../components/common/CDivider';
import {colors} from 'react-native-swiper-flatlist/src/themes';
import {successText} from '../../../common/message';

export default function Search({navigation, route}) {
  const colors = useSelector(state => state.theme.theme);
  const ChildData = route?.params?.pawn;
  const {
    handlers: {GETUSER, GETPROD, POST},
  } = useContext(AuthContext);
  const [data, setData] = useState([]);
  const [element, setElement] = useState(0);
  const isLoading = useBoolean(false);
  const {
    data: aimagData,
    fetchData: fetchAimag,
    isLoading: isLoadingAimag,
  } = useService();
  const sortBoolean = useBoolean(false);
  const userLoading = useBoolean(false);
  const sortAndFilterRef = useRef();

  const onPressFilter = () => sortAndFilterRef?.current?.show();
  const onPressFilterHide = () => sortAndFilterRef?.current?.hide();

  const [states, setStates] = useState({
    ref_name: null,
    search: '',
    page: 0,
    pageNo: 0,
    perPage: 10,
    filteredData: null,
    loadingFilter: false,
    loadingRef: false,
    ref_id: null,
    aimag_name: null,
    res: {},
    liked: false,
    subject_id: [],
    subject_Name: [],
    aimag_id: null,
    data: null,
    total_element: 0,
  });

  useEffect(() => {
    if (ChildData) {
      setStates(prevState => ({
        ...prevState,
        ref_name: ChildData?.filter_name,
        ref_id: ChildData?.id,
        aimag_name: ChildData?.aimag_name,
        aimag_id: ChildData?.aimag_id,
      }));
    }
  }, [ChildData]);
  useFocusEffect(
    useCallback(() => {
      const fetchData = async () => {
        await getCustomer();
        getRef();
        getAimag();
      };

      fetchData();
    }, [states?.ref_id, ChildData, states?.aimag_id]),
  );
  useEffect(() => {
    getData();
  }, [
    states?.aimag_id,
    states,
    ChildData,
    states?.pageNo,
    clearOption,
    states?.subject_id,
    states?.subject_Name,
  ]);
  const getRef = async () => {
    try {
      setStates(prevState => ({
        ...prevState,
        loadingRef: true,
      }));
      await GETPROD(`/get_ref/${states?.ref_id}`, false, {}).then(res => {
        if (res?.status === 200) {
          setStates(prevState => ({
            ...prevState,
            ref_name: res?.data?.name,
            loadingRef: false,
          }));
        }
        setStates(prevState => ({
          ...prevState,
          loadingRef: false,
        }));
      });
    } catch (e) {
      setStates(prevState => ({
        ...prevState,
        loadingRef: false,
      }));
      return;
    }
  };
  const getCustomer = async () => {
    try {
      userLoading.setTrue();
      const user_id = await getUserId();
      await GETUSER(`/get_customer/${user_id}`, true, {})
        .then(res => {
          const getUserData = JSON.parse(res?.data?.filter_json);
          setStates(prevState => ({
            ...prevState, // Keep the previous state values
            ref_id: getUserData?.ref,
            res: getUserData?.responsibility,
            aimag_id: getUserData?.responsibility?.aimag_id,
          }));
          userLoading.setFalse();
        })
        .catch(e => {
          userLoading.setFalse();
        });
    } catch (e) {
      userLoading.setFalse();
      return;
    }
  };

  const getAimag = () => {
    const filter = {
      filter: [
        {
          field_name: 'id',
          operation: '=',
          value: `${states?.res?.aimag_id}`,
        },
      ],
    };
    fetchAimag('POST', '/get_aimag_list', false, filter);
  };

  // call datas
  const getData = async () => {
    try {
      isLoading.setTrue();
      const paging = {
        page_no: states?.pageNo,
        per_page: states?.perPage,
        sort: !sortBoolean.value ? 'created_at desc' : 'created_at asc',
        subjects: states?.subject_id || [],

        filter: [
          {
            field_name: 'aimag_id',
            value: ChildData?.aimag_id
              ? `${ChildData?.aimag_id}`
              : `${states?.aimag_id}`,
            operation: '=',
          },
        ],
      };
      await POST(
        `/get_customer_class_list/${ChildData?.id || states?.ref_id}`,
        false,
        paging,
      )
        .then(res => {
          if (res?.status === 200) {
            isLoading.setFalse();
            const arrivedData = res?.data?.data;
            setData(arrivedData);
            console.log(res);
            setElement(res?.data?.pagination?.total_elements);

            // setData(prevData => [...prevData, ...arrivedData]);
          } else if (res?.status === 400) {
            console.log('ontsolgtoi uilchilgee bhgui');
          }
        })
        .catch(e => {
          isLoading.setFalse();
        });
    } catch (e) {
      isLoading.setFalse();
      return;
    }
  };
  const handleLoadMore = () => {
    if (!isLoading.value) {
      setStates(prevState => ({
        ...prevState,
        page: states?.pageNo + 1,
      }));
    }
  };

  // on handle service detail screen navigation
  const onPressFinded = item => {
    navigation.navigate(StackNav.ProductDetail, {item: item});
  };

  const onPress = () => {
    navigation.navigate(StackNav.FilterScreen, {
      data: {
        states,
        aimag: aimagData?.data[0],
      },
    });
  };
  const onPressLike = async id => {
    try {
      const like_id = {
        class_id: id,
      };
      await POST(`/create_favorite`, true, like_id)
        .then(res => {
          setStates(prevState => ({
            ...prevState,
            liked: true,
          }));
          successText(res?.data?.response_msg);
        })
        .catch(e => {});
    } catch (e) {
      return;
    }
  };
  const onPressDislike = async id => {
    try {
      const like_id = {
        ids: [id],
      };
      await POST(`/delete_favorites`, true, like_id)
        .then(res => {
          setStates(prevState => ({
            ...prevState,
            liked: false,
          }));
        })
        .catch(e => {
          console.log(e);
        });
    } catch (e) {
      return;
    }
  };
  const favorite = id => {
    if (states?.liked === false) {
      onPressLike(id);
      setStates(prevState => ({
        ...prevState,
        liked: true,
      }));
    } else {
      onPressDislike(id);
      setStates(prevState => ({
        ...prevState,
        liked: false,
      }));
    }
  };
  const FilterOption = items => {
    setData([]);
    const idsArray = items.map(item => item.subject_id);
    onPressFilterHide();
    setStates(prevState => ({
      ...prevState,
      subject_id: idsArray,
      subject_Name: items,
    }));
  };

  const onPressOption = value => {
    const newData = states?.subject_Name.filter(
      itms => itms?.subject_id !== value?.subject_id,
    );
    const idsArray = newData.map(item => item.subject_id);
    setStates(prevState => ({
      ...prevState,
      subject_Name: newData,
      subject_id: idsArray,
    }));
  };
  const clearOption = () => {
    setData([]);
    setStates(prevState => ({
      ...prevState,
      subject_Name: [],
      subject_id: [],
    }));
  };
  const renderItem = ({item, index}) => {
    return (
      <View key={index}>
        <SearchedItem
          onPressFindedItem={() => {
            onPressFinded(item);
          }}
          onPressLike={() => {
            favorite(item?.id);
          }}
          data={item}
          liked={states?.liked}
        />
      </View>
    );
  };

  const renderFilter = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => onPressOption(item)}
        key={index}
        style={[localStyles.filteredButton]}>
        <Feather
          name="x"
          size={moderateScale(15)}
          color={commonColor.carelyLogoColor}
          style={styles.mh5}
        />
        <CText type={'m14'} style={[styles.mr5]}>
          {item?.option_name}
        </CText>
      </TouchableOpacity>
    );
  };
  return (
    <CSafeAreaView>
      <Spinner visible={isLoading.value} />
      <CHeader isHideBack title={strings.search} />
      <View style={localStyles.root}>
        <FlashList
          data={data}
          renderItem={renderItem}
          ListEmptyComponent={
            <View style={styles.pt40}>
              <RenderNullComponent
                title1={strings.notFound}
                title2={strings.notFoundDesc}
              />
            </View>
          }
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          bounces={false}
          contentContainerStyle={localStyles.contentContainerStyle}
          estimatedItemSize={20}
          // onEndReached={handleLoadMore}
          onEndReachedThreshold={0.1}
          ListHeaderComponent={
            <View style={styles.mv5}>
              <TouchableOpacity
                onPress={() => onPress()}
                style={[
                  localStyles.search,
                  styles.rowSpaceBetween,
                  {backgroundColor: colors.dark ? colors.dark2 : colors.white},
                ]}>
                <View style={styles.rowStart}>
                  <AntDesign
                    name="search1"
                    size={moderateScale(20)}
                    color={colors.grayScale4}
                    style={styles.mr15}
                  />
                  <CText type={'m16'} color={colors.placeHolderColor}>
                    {ChildData?.filter_name || states?.ref_name}
                  </CText>
                  <CText
                    color={colors.placeHolderColor}
                    type={'B5'}
                    style={styles.ph10}>
                    {'\u2B24'}
                  </CText>
                  {!!aimagData?.data && (
                    <CText type={'m16'} color={colors.placeHolderColor}>
                      {states?.aimag_name || aimagData?.data[0]?.aimag_name}
                    </CText>
                  )}
                </View>
              </TouchableOpacity>
              <View style={[localStyles.sort]}>
                <View
                  style={[
                    localStyles.filter,
                    {
                      backgroundColor: colors.dark
                        ? colors.dark2
                        : colors.white,
                    },
                  ]}>
                  <CText style={[styles.pl10]}>Нийт илэрц:</CText>
                  <CText style={[styles.pr10]}> {element}</CText>
                </View>
                <TouchableOpacity
                  onPress={onPressFilter}
                  style={[
                    localStyles.filter,
                    {
                      backgroundColor: colors.dark
                        ? colors.dark2
                        : colors.white,
                    },
                  ]}>
                  <MaterialIcons
                    name="format-list-bulleted"
                    size={moderateScale(18)}
                    color={commonColor.carelyLogoColor}
                    style={styles.mh5}
                  />
                  <CText style={[styles.pr10]}>Шүүлт</CText>
                </TouchableOpacity>
              </View>
              <CDivider style={[]} />
              {states?.subject_Name.length > 0 ? (
                <FlatList
                  data={states?.subject_Name}
                  renderItem={renderFilter}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  ListHeaderComponent={
                    <TouchableOpacity
                      onPress={clearOption}
                      style={[styles.flexCenter]}>
                      <CText
                        color={commonColor.redColor}
                        style={[styles.mt10, styles.mr10]}>
                        Цэвэрлэх
                      </CText>
                    </TouchableOpacity>
                  }
                />
              ) : null}
            </View>
          }
        />
      </View>
      <SortAndFilter
        SheetRef={sortAndFilterRef}
        filter_id={ChildData?.id || states?.ref_id}
        onPressDone={items => {
          FilterOption(items);
        }}
      />
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.ph10,
    ...styles.flex,
    marginTop: moderateScale(-10),
  },
  contentContainerStyle: {
    ...styles.pb20,
  },
  bottomContainer: {
    ...styles.pv10,
    ...styles.ph20,
    ...styles.rowSpaceBetween,
  },
  addToCartContainer: {
    width: deviceWidth / 2 + moderateScale(30),
    ...styles.shadowStyle,
  },
  priceContainer: {
    height: getHeight(50),
    ...styles.justifyEvenly,
  },
  sort: {
    ...styles.rowSpaceBetween,
    ...styles.mt10,
  },
  search: {
    ...styles.pv15,
    borderRadius: moderateScale(15),
    ...styles.ph10,
    ...styles.mt15,
  },
  filter: {
    borderRadius: moderateScale(20),
    ...styles.p5,
    ...styles.rowStart,
  },
  filteredButton: {
    ...styles.mr10,
    borderRadius: moderateScale(10),
    borderWidth: moderateScale(1),
    borderColor: colors.gray,
    ...styles.rowStart,
    ...styles.pv5,
    ...styles.mt10,
  },
});
