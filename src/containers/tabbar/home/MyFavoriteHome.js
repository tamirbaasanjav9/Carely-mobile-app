import {
  FlatList,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
// custom imports
import {moderateScale} from '../../../common/constants';
import CText from '../../../components/common/CText';
import {styles} from '../../../themes';
import variables from '../../../api/variables';

const MyFavoriteHome = ({data, isLoadingLiked, onPressItem}) => {
  const colors = useSelector(state => state.theme.theme);

  const renderFavorite = ({item, index}) => {
    const {Class} = item;
    return (
      <TouchableOpacity
        key={index}
        onPress={() => onPressItem(item)}
        style={localStyles.categoryRoot}>
        <View
          style={[
            localStyles.iconContainer,
            {
              backgroundColor: colors.dark ? colors.dark3 : colors.white,
            },
          ]}>
          <Image
            source={{
              uri: `${variables.IMAGEURL}/${Class?.Company?.image}`,
            }}
            style={[localStyles.productImageStyle]}
            onLoadStart={isLoadingLiked}
          />
        </View>
        <CText
          type="b14"
          numberOfLines={2}
          align={'center'}
          color={colors.textColor}
          style={[styles.mt10]}>
          {Class?.Company?.name}
        </CText>
      </TouchableOpacity>
    );
  };
  return (
    <View style={[localStyles.container]}>
      <FlatList
        data={data?.data}
        renderItem={renderFavorite}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default MyFavoriteHome;

const localStyles = StyleSheet.create({
  container: {
    ...styles.flex,
    ...styles.mv20,
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mr10,
  },
  iconContainer: {
    width: moderateScale(80),
    height: moderateScale(80),
    borderRadius: moderateScale(30),
    ...styles.center,
  },
  productImageStyle: {
    width: moderateScale(80),
    height: moderateScale(80),
    ...styles.selfCenter,
    borderRadius: moderateScale(30),
    resizeMode: 'cover',
  },
});
