import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import {deviceWidth, moderateScale} from '../../../common/constants';
import {commonColor, styles} from '../../../themes';
import CText from '../../../components/common/CText';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import strings from '../../../i18n/strings';
import CDivider from '../../../components/common/CDivider';
import CButton from '../../../components/common/CButton';
import {StackNav} from '../../../navigation/NavigationKeys';
import CalendarShort from '../../../components/detailProduct/CalendarShort';
import MapComponent from '../../../components/detailProduct/MapComponent';
import useBoolean from '../../../contexts/useBoolean';
import TopCard from '../../../components/homeComponent/TopCard';
import InstructorCard from '../../../components/homeComponent/InstructorCard';
import Octicons from 'react-native-vector-icons/Octicons';
import AgeGroupComponent from '../../../components/homeComponent/AgeGroupComponent';

export default function ProductDetail({navigation, route}) {
  const item = route?.params?.item;
  console.log(item, 'passed data ');
  const {
    AgeGroup,
    Company,
    District,
    Images,
    Subjects,
    Schedules,
    Khoroo,
    class_address,
    class_description,
    class_end_date,
    class_name,
    class_price,
    class_size,
    class_start_date,
    experience,
    has_meals,
    Aimag,
    Instructors,
    PaymentInterval,
    current_students,
  } = item;

  console.log(Schedules);
  const colors = useSelector(state => state.theme.theme);
  const isLiked = useBoolean(false);
  const showText = useBoolean(false);
  const formattedNumber = class_price.toLocaleString(); // Formats as per user's locale

  const [quantity, setQuantity] = useState(1);
  const [data, setData] = useState();

  // ------------------------custom functions
  const onPressLike = () => isLiked.toggle();
  const toggleShowFullText = () => {
    showText.toggle();
  };
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };
  const onPressAdd = () => {
    if (data?.stock !== quantity) {
      setQuantity(prev => prev + 1);
    }
  };
  const renderHeader = () => {
    return (
      <View style={styles.mh10}>
        <TopCard
          image={Images}
          Company={Company}
          Schedules={Schedules}
          Aimag={Aimag}
          District={District}
          Class_price={class_price}
          class_end_date={class_end_date}
          class_start_date={class_start_date}
          experience={experience}
          PaymentInterval={PaymentInterval}
          class_name={class_name}
        />

        <View style={localStyles.productText}>
          <CText style={[styles.flex, styles.mb5]} type={'b14'}>
            {class_name}
          </CText>
          <CText style={[styles.flex, styles.mb5]} type={'b13'}>
            Анги дүүргэлт {class_size}
            {current_students}
          </CText>
        </View>

        <CalendarShort
          timeData={Schedules}
          startDate={class_start_date}
          endDate={class_end_date}
        />
        <CDivider />
        <View style={localStyles.productText}>
          <CText style={[styles.flex, styles.mb5]} type={'b14'}>
            Дэлгэрэнгүй
          </CText>
          <View>
            <CText type={'m14'}>
              {showText.value
                ? class_description
                : class_description.slice(0, 150)}
              {!showText.value && class_description.length > 100 && (
                <CText>... </CText>
              )}
              {class_description.length > 150 && (
                <CText
                  type={'m14'}
                  onPress={toggleShowFullText}
                  color={commonColor.carelyLogoColor}>
                  {showText.value ? 'Хураангуй' : 'Бүгдийг харах'}
                </CText>
              )}
            </CText>
          </View>
        </View>
        <CDivider />

        <CText style={[styles.flex, styles.mb15]} type={'b14'}>
          Насны ангилал
        </CText>
        {AgeGroup && <AgeGroupComponent AgeGroup={AgeGroup} />}
        <CDivider />
        <CText style={[styles.flex, styles.mb15]} type={'b14'}>
          Багшийн мэдээлэл
        </CText>
        <FlatList
          data={Instructors}
          renderItem={({item}) => <InstructorCard instructor={item} />}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          horizontal
        />

        <CDivider />
        <CText style={[styles.flex, styles.mv5]} type={'m14'}>
          {class_address}
        </CText>
        <MapComponent />
        <CDivider />
        <View style={localStyles.productText}>
          <CText style={[styles.flex, styles.mb15]} type={'b14'}>
            Нэмэлт мэдээлэл
          </CText>
          <FlatList
            data={Subjects}
            renderItem={({item}) => (
              <View style={styles.flexRow}>
                <Octicons
                  name="shield-check"
                  size={moderateScale(18)}
                  color={commonColor.carelyLogoColor}
                  style={[styles.mr10, styles.mb10]}
                />
                <CText type={'b13'}>{item?.Subject?.subject_name}</CText>
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    );
  };
  //------------------render screen options
  return (
    <CSafeAreaView>
      <CHeader title={data?.name} rightIcon={<RightIcon />} />
      <FlatList
        data={Instructors}
        // renderItem={({item}) => <InstructorCard instructor={item} />}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={renderHeader}
      />
      <View style={styles.ph20}>
        <View style={localStyles.bottomContainer}>
          <View>
            <CText style={[styles.mb2]} type={'b16'}>
              ₮{formattedNumber} / {PaymentInterval?.const_value}
            </CText>
            <View style={[styles.rowStart]}>
              <Octicons
                name="star-fill"
                size={moderateScale(18)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText style={styles.mr5} type={'b13'}>
                4.85
              </CText>
              <CText color={commonColor.grayScale5} type={'b13'}>
                (124 үзсэн)
              </CText>
            </View>
          </View>
          <CButton
            onPress={onPressLike}
            type={'b15'}
            title={strings.sendRequist}
            containerStyle={localStyles.addToCartContainer}
          />
        </View>
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  productText: {
    ...styles.mv10,
  },
  bottomContainer: {
    ...styles.pv10,
    ...styles.rowSpaceBetween,
  },
  addToCartContainer: {
    width: deviceWidth / 2 - moderateScale(10),
    ...styles.shadowStyle,
  },
});
