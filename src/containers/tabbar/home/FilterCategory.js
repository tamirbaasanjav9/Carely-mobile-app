import {StyleSheet, View, FlatList} from 'react-native';
import React, {useEffect} from 'react';
import {StackNav} from '../../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import ServiceGoing from '../../../components/cartComponent/ServiceGoing';

// redux
import useService from '../../../contexts/useService';
import Spinner from 'react-native-loading-spinner-overlay';
import {styles} from '../../../themes';

export default function FilterScreen({navigation}) {
  const {data, fetchData, error, isLoading} = useService(); // call service custom
  useEffect(() => {
    // category service
    fetchData('POST', '/get_ref_list', true, {});
  }, []);
  const onSelectItem = item => {
    console.log(item);
    navigation.navigate(StackNav.FilterScreen, {
      filter: {
        name: item?.name,
        id: item?.id,
      },
    });
  };
  // search filtered button
  const renderCategory = ({item}) => {
    console.log(item);
    return (
      <>
        <ServiceGoing typeCare={item} onSelectItem={() => onSelectItem(item)} />
      </>
    );
  };
  return (
    <CSafeAreaView>
      <Spinner visible={isLoading} />
      <CHeader title={'Ямар төрлийн үйлчилгээ хайж байна'} />
      <View style={localStyles.root}>
        <FlatList
          numColumns={2}
          data={data?.data}
          renderItem={renderCategory}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.ph15,
    ...styles.flex,
  },
});
