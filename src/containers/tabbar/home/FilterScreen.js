import {StyleSheet, View, TouchableOpacity, FlatList} from 'react-native';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {StackNav, TabNav} from '../../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import CText from '../../../components/common/CText';
import {useSelector} from 'react-redux';
import {moderateScale} from '../../../common/constants';

// styling imports
import {commonColor, styles} from '../../../themes';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LocationSheet from '../../../components/models/LocationSheet';
import useService from '../../../contexts/useService';
import Spinner from 'react-native-loading-spinner-overlay';
import ChildFilterSheet from '../../../components/models/ChildFilterSheet';

export default function FilterScreen({navigation, route}) {
  const colors = useSelector(state => state.theme.theme);

  // from routes
  const myState = route?.params?.data;
  console.log(myState);
  const selectRef = route?.params?.filter;

  // call service custom
  const {data, fetchData, isLoading} = useService();

  // open && close modalSheet
  const locationRef = useRef();
  const ChildRef = useRef();

  // ref toggles
  const onPressFilter = () => locationRef?.current?.show();
  const onPressConfirm = () => locationRef?.current?.hide();
  const onPressChildCount = () => ChildRef?.current?.show();
  const onPressConfirmChild = () => ChildRef?.current?.hide();

  // states
  const [pawn, setPawn] = useState({
    district_name: null,
    aimag_name: null,
    district_id: null,
    aimag_id: null,
    id: null,
    filter_name: null,
    child: null,
  });
  useEffect(() => {
    if (data && data?.data && data?.data?.length > 0) {
      setPawn(prevState => ({
        ...prevState,
        district_name: data.data[0].district_name,
        aimag_name: data.data[0].Aimag?.aimag_name,
        id: myState?.states?.ref_id || selectRef?.id,
        filter_name: selectRef?.name || myState?.states?.ref_name,
      }));
    }
  }, [data, myState?.states?.ref_id, myState, selectRef]);

  // on handle service detail screen navigation

  useEffect(() => {
    fetchData('POST', '/get_district_list', false, {
      filter: [
        {
          field_name: 'id',
          operation: '=',
          value: `${myState?.states?.res?.district_id}`,
        },
      ],
    });
  }, []);
  // search filtered button
  const onPressCategory = () => {
    navigation.navigate(StackNav.FilterCategory);
  };
  const locationFunction = item => {
    console.log(item);
    setPawn({
      ...pawn, // Spread the current state to avoid overwriting other keys
      aimag_name: item?.aimag_id?.aimag_name, // Update the specific key
      district_name: item?.district_id?.district_name,
      aimag_id: item?.aimag_id?.id, // Update the specific key
      district_id: item?.district_id?.id,
    });
  };
  const ChildCount = item => {
    setPawn({
      ...pawn,
      child: item,
    });
  };

  const onSearch = () => {
    navigation.navigate(TabNav.SearchTab, {pawn});
  };
  return (
    <CSafeAreaView>
      <Spinner visible={!!isLoading} />
      <CHeader title={'Ямар төрлийн үйлчилгээ хайж байна'} />
      <View style={localStyles.root}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={
            <View style={[styles.flex]}>
              <View
                style={[
                  localStyles.subContainer,
                  {backgroundColor: colors.dark ? colors.dark2 : colors.white},
                ]}>
                <TouchableOpacity
                  onPress={onPressCategory}
                  style={[
                    localStyles.listContainer,
                    {
                      borderBottomWidth: moderateScale(1),
                      borderColor: commonColor.grayScale5,
                    },
                  ]}>
                  <AntDesign
                    name="search1"
                    size={moderateScale(20)}
                    color={colors.grayScale4}
                    style={styles.mr15}
                  />
                  <CText>{pawn?.filter_name}</CText>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={onPressFilter}
                  style={[
                    localStyles.listContainer,
                    {
                      borderBottomWidth:
                        pawn?.id === 10 ? moderateScale(1) : null,
                      borderColor: commonColor.grayScale5,
                    },
                  ]}>
                  <EvilIcons
                    name="location"
                    size={moderateScale(20)}
                    color={colors.grayScale4}
                    style={styles.mr15}
                  />
                  {!!pawn?.aimag_name && !!pawn?.district_name && (
                    <CText>
                      {pawn?.aimag_name}, {pawn?.district_name}
                    </CText>
                  )}
                </TouchableOpacity>
                {pawn?.id === 10 ? (
                  <TouchableOpacity
                    onPress={onPressChildCount}
                    style={[localStyles.listContainer]}>
                    <MaterialCommunityIcons
                      name="teddy-bear"
                      size={moderateScale(20)}
                      color={colors.grayScale4}
                      style={styles.mr15}
                    />
                    <CText>
                      {pawn?.child !== null ? pawn?.child : null}{' '}
                      {pawn?.child === null
                        ? 'Xүүхдийн тоо оруулаагүй'
                        : 'Xүүхэд'}
                    </CText>
                  </TouchableOpacity>
                ) : null}
              </View>

              <TouchableOpacity
                onPress={onSearch}
                style={[
                  localStyles.button,
                  {backgroundColor: colors.carelyLogoColor},
                ]}>
                <CText color={colors.white} type={'m15'}>
                  Хайх
                </CText>
              </TouchableOpacity>
            </View>
          }
        />
      </View>
      <ChildFilterSheet
        SheetRef={ChildRef}
        onPressConfirm={onPressConfirmChild}
        ref_id={myState?.states?.ref_id}
        onDataReceived={ChildCount}
      />
      <LocationSheet
        SheetRef={locationRef}
        onDataReceived={locationFunction}
        onPressConfirm={onPressConfirm}
      />
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.ph10,
    ...styles.flex,
  },
  subContainer: {
    borderRadius: moderateScale(15),
    ...styles.mh20,
    ...styles.mv10,
  },
  listContainer: {
    ...styles.rowStart,
    ...styles.pv15,
    ...styles.mh15,
  },
  button: {
    ...styles.center,
    ...styles.mh20,
    borderRadius: moderateScale(15),
    ...styles.pv10,
    ...styles.mt10,
  },
});
