import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {FlashList} from '@shopify/flash-list';
import Feather from 'react-native-vector-icons/Feather';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import {StackNav, TabNav} from '../../../navigation/NavigationKeys';
import CText from '../../../components/common/CText';

// Colors and Styling imports
import {moderateScale} from '../../../common/constants';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import {styles} from '../../../themes';

export default function ProductCategorySecond({navigation, route}) {
  const {ChildRef, name, item} = route?.params?.ChildData;
  const colors = useSelector(state => state.theme.theme);

  // onPress search navigation components
  const onPressSearch = () => {
    navigation.navigate(TabNav.SearchTab, {item: item});
  };

  // render on top header items
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  // rendering mapping datas on flex
  const renderItem = ({item, index}) => {
    const {name, has_child} = item;
    const onPressCategoryChild = () =>
      navigation.navigate(
        has_child === true ? StackNav.ProductCategoryThird : TabNav.SearchTab,
        {
          ChildData: item,
        },
      );
    return (
      <TouchableOpacity
        key={index}
        onPress={onPressCategoryChild}
        style={[
          localStyles.contactUsContainer,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.white,
          },
        ]}>
        <CText type={'b15'} color={colors.textColor}>
          {name}
        </CText>
        {has_child === true ? (
          <Feather
            name="chevron-right"
            size={moderateScale(20)}
            color={colors.textColor}
          />
        ) : null}
      </TouchableOpacity>
    );
  };
  // render screen UI
  return (
    <CSafeAreaView>
      <CHeader title={name} rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlashList
          data={ChildRef}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          estimatedItemSize={10}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  contactUsContainer: {
    ...styles.mt20,
    ...styles.pv20,
    borderRadius: moderateScale(15),
    ...styles.flexRow,
    ...styles.ph10,
    ...styles.justifyBetween,
  },
});
