// Library Imports
import {FlatList, StyleSheet, View} from 'react-native';
import React, {useEffect, useCallback, memo, useContext} from 'react';

// General usage imports
import {useSelector} from 'react-redux';
import {FlashList} from '@shopify/flash-list';
import {useFocusEffect, useNavigation} from '@react-navigation/native';

// Redux
import {styles} from '../../../themes';
import {StackNav} from '../../../navigation/NavigationKeys';
import useService from '../../../contexts/useService';

// Custom components
import DetialHeader from '../../../components/homeComponent/DetialHeader';
import HomeInterView from '../../../components/homeComponent/HomeInterView';
import MyFavoriteHome from './MyFavoriteHome';

// Default components
import SubHeader from '../../../components/SubHeader';
import HomeHeader from '../../../components/homeComponent/HomeHeader';
import HomeCategoryList from './HomeCategoryList';

// Commons Imports
import strings from '../../../i18n/strings';
import {useState} from 'react';
import {getUserId} from '../../../utils/asyncstorage';
import {AuthContext} from '../../../contexts/authContext';
import Carousel from '../../../components/homeComponent/Carousel';

// dependencies
import * as Animatable from 'react-native-animatable';
import SearchedItem from '../../../components/homeComponent/SearchedItem';

// rendering top header -------------------------------------
const RenderHeaderItem = memo(() => {
  const navigation = useNavigation();
  const onPressSpecialOffer = useCallback(
    () => navigation.navigate(StackNav.SpecialOffers),
    [],
  );
  return (
    <Animatable.View>
      {/* <SearchComponentButton /> */}
      <SubHeader
        title1={strings.nextPlan}
        title2={strings.seeAll}
        onPressSeeAll={onPressSpecialOffer}
        header={true}
      />
      <HomeInterView />
      <SubHeader title1={'Үйлчилгээ'} style={styles.mv20} header={true} />
    </Animatable.View>
  );
});

// rendering Footer -------------------------------
const RenderFooterItem = memo(() => {
  const navigation = useNavigation();
  const {data: company, fetchData: getCompany} = useService();
  const {data, fetchData, error} = useService();
  const {handlers} = useContext(AuthContext);
  const [filterData, setFilterData] = useState();
  const {
    data: likedData,
    fetchData: fetchLiked,
    isLoading: isLoadingLiked,
    error: errorLiked,
  } = useService();
  // get user information
  const getCustomer = async () => {
    try {
      const user_id = await getUserId();
      await handlers.GETUSER(`/get_customer/${user_id}`, true, {}).then(res => {
        let filter_id = res?.data?.filter_json;
        setFilterData(JSON.parse(filter_id));
      });
    } catch (e) {
      return e;
    }
  };

  // get data class data
  const getData = () => {
    const paging = {
      page_no: 0,
      per_page: 5,
      sort: 'created_at asc',
    };
    fetchData(
      'POSTUSER',
      `/get_customer_class_list/${filterData?.ref}`,
      true,
      paging,
    );
  };

  // added new companies
  const fetchCompany = async () => {
    await getCompany('GETUSER', `/get_customer_company_list`, false);
  };

  useFocusEffect(
    useCallback(() => {
      const paging = {
        per_page: 4,
      };
      const fetchDatas = async () => {
        await getCustomer();
        getData();
        fetchCompany();
        fetchLiked('POSTUSER', '/get_favorite_list', true, paging);
      };
      fetchDatas();
    }, [filterData?.ref]),
  );
  // navigation to Detail screen
  const onPressItem = item => {
    navigation.navigate(StackNav.ProductDetail, {item: item});
  };
  const onPressSpecialOffer = useCallback(
    () => navigation.navigate(StackNav.Notification),
    [],
  );

  return (
    <Animatable.View style={[styles.mv15]}>
      <SubHeader
        title1={strings.newCompanies}
        title2={strings.seeAll}
        onPressSeeAll={onPressSpecialOffer}
        style={styles.mv20}
      />
      <Carousel company={company} />

      {likedData?.data?.length !== 0 ? (
        <>
          <SubHeader
            title1={strings.myFavorite}
            title2={strings.seeAll}
            onPressSeeAll={onPressSpecialOffer}
          />
          <MyFavoriteHome
            data={likedData}
            isLoading={isLoadingLiked}
            onPressItem={onPressItem}
          />
        </>
      ) : null}
      <SubHeader
        title1={strings.recommendedYou}
        title2={strings.seeAll}
        onPressSeeAll={onPressSpecialOffer}
      />
      <FlatList
        data={data?.data}
        renderItem={({item}) => (
          <SearchedItem
            onPressFindedItem={() => {
              onPressItem(item);
            }}
            data={item}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.ph20}
      />
    </Animatable.View>
  );
});

// rendering main return ----------------------------------------------------------------------------
export default function HomeTab({navigation}) {
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData, error} = useService(); // call service custom

  // Call category service
  useEffect(() => {
    fetchData('POST', '/get_ref_list', true, {});
  }, []); // Empty dependency array means it runs once when the component mounts

  // Select Category item navigation function
  const onPressItem = item => {
    console.log(item);
    navigation.navigate(StackNav.ProductCategory, {
      item: item,
    });
  };
  // render items
  const renderCategoryItem = ({item, index}) => {
    const {name, image} = item;
    return (
      <>
        <HomeCategoryList
          index={index}
          name={name}
          image={image}
          onPressItem={() => onPressItem(item)}
        />
      </>
    );
  };

  return (
    <Animatable.View
      animation="fadeInUp"
      style={[localStyles.root, {backgroundColor: colors.backgroundColor}]}>
      <HomeHeader />
      <FlashList
        data={data?.data}
        renderItem={renderCategoryItem}
        keyExtractor={(item, index) => index.toString()}
        numColumns={3}
        ListHeaderComponent={<RenderHeaderItem />}
        ListFooterComponent={<RenderFooterItem />}
        showsVerticalScrollIndicator={false}
      />
    </Animatable.View>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.ph10,
    ...styles.flex,
  },
});
