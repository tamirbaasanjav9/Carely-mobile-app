import {StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';

// Custom imports
import {moderateScale} from '../../../common/constants';
import {styles} from '../../../themes';
import CText from '../../../components/common/CText';
import {useSelector} from 'react-redux';
import {Image} from 'react-native';
import variables from '../../../api/variables';

const HomeCategoryList = ({onPressItem, image, index, name}) => {
  const colors = useSelector(state => state.theme.theme);

  return (
    <TouchableOpacity
      key={index}
      onPress={onPressItem}
      style={localStyles.categoryRoot}>
      <Image
        source={{uri: `${variables.GENERAL_IMAGE_URL}/${image}`}}
        style={[localStyles.productImageStyle]}
      />
      <CText
        type="b14"
        numberOfLines={2}
        align={'center'}
        color={colors.textColor}
        style={[styles.mt10]}>
        {name}
      </CText>
    </TouchableOpacity>
  );
};

export default HomeCategoryList;

const localStyles = StyleSheet.create({
  categoryRoot: {
    ...styles.flexCenter,
    ...styles.mt20,
  },
  productImageStyle: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(100),
    ...styles.selfCenter,
  },
});
