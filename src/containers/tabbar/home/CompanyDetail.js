import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {Menu_Dark, Menu_Light} from '../../../assets/svgs';
import {commonColor, styles} from '../../../themes';
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import {deviceWidth, getHeight, moderateScale} from '../../../common/constants';
import variables from '../../../api/variables';
import CText from '../../../components/common/CText';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import useService from '../../../contexts/useService';
import Spinner from 'react-native-loading-spinner-overlay';

export default function CompanyDetail({route, navigation}) {
  const {id} = route?.params?.item;
  const [index, setIndex] = useState(0);
  const colors = useSelector(state => state.theme.theme);
  const {
    data: company,
    fetchData: CompanyDetail,
    isLoading: isLoading,
  } = useService();
  console.log(company);
  useEffect(() => {
    const fetchCompany = async () => {
      await CompanyDetail('GETUSER', `/get_customer_company/${id}`, false);
    };
    fetchCompany();
  }, []);

  const PhotosRoutes = () => (
    <View style={{flex: 1}}>
      {/* <FlatList
        data={photos}
        numColumns={3}
        renderItem={({item, index}) => (
          <View
            style={{
              flex: 1,
              aspectRatio: 1,
              margin: 3,
            }}>
            <Image
              key={index}
              source={item}
              style={{width: '100%', height: '100%', borderRadius: 12}}
            />
          </View>
        )}
      /> */}
    </View>
  );

  const LikesRoutes = () => (
    <View
      style={{
        flex: 1,
        backgroundColor: 'blue',
      }}
    />
  );

  const renderScene = SceneMap({
    first: PhotosRoutes,
    second: LikesRoutes,
  });

  const [routes] = useState([
    {key: 'first', title: 'Photos'},
    {key: 'second', title: 'Likes'},
  ]);

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{
        backgroundColor: colors.carelyLogoColor,
      }}
      style={{
        backgroundColor: colors.backgroundColor,
        height: 44,
      }}
      renderLabel={({focused, route}) => (
        <CText
          style={[
            {color: focused ? colors.carelyLogoColor : colors.grayScale7},
          ]}>
          {route.title}
        </CText>
      )}
    />
  );
  return (
    <CSafeAreaView>
      <Spinner visible={!!isLoading} />
      <View style={localStyles.root}>
        <Image
          resizeMode="cover"
          source={{uri: `${variables.IMAGEURL}/${company?.Owner?.image}`}}
          style={[
            localStyles.swiperImageStyle,
            {
              backgroundColor: colors.dark ? colors.dark2 : colors.white,
            },
          ]}
        />
        <View style={styles.center}>
          <View
            style={[
              localStyles.imageContainer,
              {
                backgroundColor: colors.dark
                  ? colors.backgroundColor
                  : colors.backgroundColor,
              },
            ]}>
            <Image
              resizeMode="contain"
              source={{uri: `${variables.IMAGEURL}/${company?.logo}`}}
              style={[
                localStyles.profileImage,
                {
                  borderColor: colors.dark
                    ? colors.carelyLogoColor
                    : colors.carelyLogoColor,
                },
              ]}
            />
          </View>
          <CText>{company?.name}</CText>
          <View style={styles.rowSpaceBetween}>
            <Ionicons
              name="location"
              size={moderateScale(18)}
              color={commonColor.carelyLogoColor}
            />
            <CText style={styles.mh5}>{company?.Aimag?.aimag_name},</CText>
            <CText>{company?.District?.district_name}</CText>
          </View>
          <View style={[styles.rowSpaceBetween]}>
            <View style={[styles.center, localStyles.detail]}>
              <CText>122</CText>
              <CText>Followers</CText>
            </View>
            <View style={[styles.center, localStyles.detail]}>
              <CText>122</CText>
              <CText>following</CText>
            </View>
            <View style={[styles.center, localStyles.detail]}>
              <CText>122</CText>
              <CText>likes</CText>
            </View>
          </View>
          <View style={[styles.rowSpaceBetween, styles.pt10]}>
            <TouchableOpacity
              style={[
                localStyles.button,
                {
                  backgroundColor: colors.dark
                    ? colors.carelyLogoColor
                    : colors.carelyLogoColor,
                },
              ]}>
              <CText style={styles.p10}>following</CText>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                localStyles.button,
                {
                  backgroundColor: colors.dark
                    ? colors.carelyLogoColor
                    : colors.carelyLogoColor,
                },
              ]}>
              <CText style={styles.p10}>Add friend</CText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={localStyles.tabbar}>
        <TabView
          navigationState={{index, routes}}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={{width: deviceWidth}}
          renderTabBar={renderTabBar}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.flex,
  },
  swiperImageStyle: {
    width: deviceWidth + 2,
    height: getHeight(200),
  },
  profileImage: {
    width: moderateScale(150),
    height: moderateScale(150),
    borderRadius: moderateScale(999),
    borderWidth: moderateScale(2),
    ...styles.m10,
  },
  button: {
    borderRadius: moderateScale(10),
  },
  detail: {
    ...styles.mr10,
  },
  imageContainer: {
    marginTop: moderateScale(-60),
    borderRadius: moderateScale(999),
  },
  tabbar: {
    ...styles.flex,
    ...styles.mh20,
    ...styles.mt100,
  },
});
