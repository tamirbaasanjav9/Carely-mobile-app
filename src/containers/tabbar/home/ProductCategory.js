import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import React, {useMemo} from 'react';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import {StackNav, TabNav} from '../../../navigation/NavigationKeys';
import CText from '../../../components/common/CText';
import SubHeader from '../../../components/SubHeader';
import HomeBanner from '../../../components/homeComponent/HomeBanner';

// styles and images
import variables from '../../../api/variables';
import images from '../../../assets/images';
import {deviceWidth, moderateScale} from '../../../common/constants';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import {styles} from '../../../themes';

// dependencies
import Feather from 'react-native-vector-icons/Feather';
import {useSelector} from 'react-redux';

export default function ProductCategory({navigation, route}) {
  const {ChildRef, name, item, image, id} = route?.params?.item;
  console.log(item);
  console.log(ChildRef);

  const colors = useSelector(state => state.theme.theme);
  const bannerImage = useMemo(() => {
    return colors.dark ? images.swiperImageDark1 : images.swiperImageLight2;
  }, [colors]);
  const onPressSearch = () => {
    navigation.navigate(TabNav.SearchTab, {item: item});
  };

  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  const renderHeader = () => {
    const onPressSpecialOffer = () => {
      navigation.navigate(TabNav.SearchTab, {ChildData: {id}});
    };
    return (
      <View style={[styles.flex]}>
        <Image
          source={{uri: `${variables.GENERAL_IMAGE_URL}/${image}`}}
          style={[localStyles.productImageStyle]}
        />
        <SubHeader
          title1={name}
          title2={strings.seeAll}
          onPressSeeAll={onPressSpecialOffer}
        />
      </View>
    );
  };
  const renderFooter = () => {
    return (
      <View style={[styles.pv10, styles.mt20]}>
        <HomeBanner image={bannerImage} />
      </View>
    );
  };
  const renderItem = ({item}) => {
    const {name, has_child} = item;
    const onPressCategoryChild = () =>
      navigation.navigate(
        has_child === true ? StackNav.ProductCategorySecond : TabNav.SearchTab,
        {
          ChildData: item,
        },
      );
    return (
      <TouchableOpacity
        onPress={onPressCategoryChild}
        style={[
          localStyles.contactUsContainer,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.white,
          },
        ]}>
        <CText type={'b15'}>{name}</CText>
        {has_child === true ? (
          <Feather
            name="chevron-right"
            size={moderateScale(20)}
            color={colors.textColor}
          />
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader title={name} rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlatList
          data={ChildRef}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={renderHeader}
          ListFooterComponent={renderFooter}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  contactUsContainer: {
    ...styles.mt20,
    ...styles.pv20,
    borderRadius: moderateScale(15),
    ...styles.flexRow,
    ...styles.ph10,
    ...styles.justifyBetween,
  },
  productImageStyle: {
    width: deviceWidth - moderateScale(40),
    height: moderateScale(150),
  },
});
