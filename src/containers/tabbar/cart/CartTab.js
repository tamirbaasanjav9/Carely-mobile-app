// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {FlashList} from '@shopify/flash-list';
import {StackNav} from '../../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../../components/common/CSafeAreaView';
import CHeader from '../../../components/common/CHeader';
import SubHeader from '../../../components/SubHeader';

// Style and Weight imports
import useService from '../../../contexts/useService';
import {styles} from '../../../themes';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import {deviceWidth, getHeight, moderateScale} from '../../../common/constants';
import images from '../../../assets/images';
import ServiceGoing from '../../../components/cartComponent/ServiceGoing';

//dependencies
import Octicons from 'react-native-vector-icons/Octicons';

export default function CartTab({navigation}) {
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData} = useService(); // call service custom
  const {data: type, fetchData: getFetchType} = useService(); // call service custom

  // Call service
  useEffect(() => {
    fetchData('POST', '/get_sale_type_list', false, {});
  }, []);
  useEffect(() => {
    // category service
    getFetchType('POST', '/get_ref_list', true, {});
  }, []); // Empty dependency array means it runs once when the component mounts

  // Render header section items
  const onPressSearch = () => navigation.navigate(StackNav.Search);

  const RightIcon = () => {
    return (
      <TouchableOpacity style={[styles.ph10]} onPress={onPressSearch}>
        {colors.dark ? (
          <>
            <Octicons
              name="plus"
              size={moderateScale(25)}
              color={colors.white}
              style={styles.mr5}
            />
          </>
        ) : (
          <>
            <Octicons
              name="plus"
              size={moderateScale(25)}
              color={colors.grayScale7}
              style={styles.mr5}
            />
          </>
        )}
      </TouchableOpacity>
    );
  };

  const LeftIcon = () => {
    return (
      <View style={styles.pr10}>
        <Image
          source={colors.dark ? images.carelyLogo : images.carelyLogo}
          style={localStyles.userImageStyle}
        />
      </View>
    );
  };

  // Render item
  const renderItem = ({item}) => {
    // const onSelectItem = value => {
    //   // select posting screen by id
    //   const categoryToRenderFunction = {
    //     10: StackNav?.Posting, // posting child care
    //     9: StackNav?.PostFirstCate, // posting houseKeeper
    //     8: StackNav?.PostSecCate, // pet and animals
    //     2: StackNav?.PostThirdCate, // postin instractor
    //     7: StackNav?.PostFourthCate, // senior orphan
    //   };
    //   const selectedScreen = categoryToRenderFunction[value?.id];
    //   if (selectedScreen) {
    //     navigation.navigate(selectedScreen, {ref: value});
    //   } else {
    //     console.warn(`Screen not found for item ID ${value}`);
    //   }
    // };
    const onSelectItem = value => {
      navigation.navigate(StackNav?.Posting, {ref: value});
    };
    return (
      <ServiceGoing typeCare={item} onSelectItem={() => onSelectItem(item)} />
    );
  };

  // Rendering ------------------------------------------
  return (
    <CSafeAreaView>
      <CHeader
        isHideBack={true}
        isLeftIcon={<LeftIcon />}
        rightIcon={<RightIcon />}
      />
      <View style={localStyles.root}>
        <FlashList
          data={type?.data}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          bounces={false}
          numColumns={2}
          contentContainerStyle={localStyles.contentContainerStyle}
          estimatedItemSize={20}
          ListHeaderComponent={
            <View style={styles.ph10}>
              <SubHeader
                type={'b20'}
                title1={'Халамжын төрөл'}
                style={styles.mv5}
              />
              <SubHeader
                style={[styles.mv5, styles.mb5]}
                type={'b15'}
                title1={'Ямар төрлийн үйлчилгээ хайж байгаа вэ?'}
              />
            </View>
          }
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.flex,
    ...styles.ph15,
  },
  contentContainerStyle: {
    ...styles.pb20,
  },
  userImageStyle: {
    width: moderateScale(100),
    height: moderateScale(60),
  },
  buttonContainer: {
    ...styles.mv10,
    height: getHeight(40),
    width: deviceWidth - moderateScale(50),
  },
});
