import React, {useState, useRef, useCallback} from 'react';
import {StyleSheet, FlatList, Image, View} from 'react-native';
import {useSelector} from 'react-redux';
import {OnBoardingSlide} from '../api/constant';
import {deviceWidth, getHeight, moderateScale} from '../common/constants';
import CButton from '../components/common/CButton';
import CSafeAreaView from '../components/common/CSafeAreaView';
import strings from '../i18n/strings';
import {StackNav} from '../navigation/NavigationKeys';
import {commonColor, styles} from '../themes';
import CText from '../components/common/CText';

const OnBoarding = ({navigation}) => {
  const colors = useSelector(state => state.theme.theme);
  const [currentIndex, setCurrentIndex] = useState(0);
  const slideRef = useRef(null);

  const _onViewableItemsChanged = useCallback(({viewableItems}) => {
    setCurrentIndex(viewableItems[0]?.index);
  }, []);
  const _viewabilityConfig = {itemVisiblePercentThreshold: 50};

  const onPressRightArrow = async () => {
    if (currentIndex === 2) {
      navigation.reset({
        index: 0,
        routes: [{name: StackNav.SelectStack}],
      });
    } else {
      slideRef.current._listRef._scrollRef.scrollTo({
        x: deviceWidth * (currentIndex + 1),
      });
    }
  };

  const RenderOnboardingItem = useCallback(
    ({item, index}) => {
      return (
        <View key={index} style={localStyles.rendetItemConatiner}>
          <Image
            source={item.image}
            resizeMode="contain"
            style={localStyles.imageStyle}
          />
          <CText
            color={commonColor.carelyLogoColor}
            align={'center'}
            type={'b28'}>
            {item?.title}
          </CText>
          <CText
            color={commonColor.carelyLogoColor}
            align={'center'}
            type={'b28'}>
            {item?.title1}
          </CText>
          <CText
            type={'r15'}
            align={'center'}
            style={[{...styles.ph25}, {...styles.mt10}]}>
            {item?.text}
          </CText>
        </View>
      );
    },
    [OnBoardingSlide],
  );

  return (
    <CSafeAreaView style={styles.flex}>
      <FlatList
        data={OnBoardingSlide}
        ref={slideRef}
        renderItem={({item, index}) => (
          <RenderOnboardingItem item={item} index={index} />
        )}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        bounces={false}
        horizontal
        onViewableItemsChanged={_onViewableItemsChanged}
        viewabilityConfig={_viewabilityConfig}
        pagingEnabled
      />

      <View style={styles.rowCenter}>
        {OnBoardingSlide.map((_, index) => (
          <View
            key={index}
            style={[
              localStyles.bottomIndicatorStyle,
              {
                width:
                  index !== currentIndex
                    ? moderateScale(10)
                    : moderateScale(20),
                backgroundColor: commonColor.carelyLogoColor,
              },
            ]}
          />
        ))}
      </View>

      <CButton
        title={currentIndex === 2 ? strings.getStarted : strings.next}
        containerStyle={localStyles.submitButton}
        type={'M18'}
        color={colors.white}
        onPress={onPressRightArrow}
      />
    </CSafeAreaView>
  );
};

const localStyles = StyleSheet.create({
  submitButton: {
    ...styles.mb15,
    ...styles.mh25,
    height: moderateScale(55),
    borderRadius: moderateScale(50),
  },
  rendetItemConatiner: {
    width: deviceWidth,
    ...styles.ph10,
    ...styles.center,
  },
  imageStyle: {
    height: '66%',
    width: deviceWidth - moderateScale(10),
  },
  bottomIndicatorStyle: {
    height: getHeight(10),
    ...styles.mb30,
    ...styles.mt10,
    borderRadius: moderateScale(10),
    ...styles.mh5,
  },
});

export default OnBoarding;
