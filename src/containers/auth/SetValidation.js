// Library import
import {StyleSheet, View} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import OTPInputView from '@twotalltotems/react-native-otp-input';

// Local import
import CSafeAreaView from '../../components/common/CSafeAreaView';
import CHeader from '../../components/common/CHeader';
import strings from '../../i18n/strings';
import CText from '../../components/common/CText';
import KeyBoardAvoidWrapper from '../../components/common/KeyBoardAvoidWrapper';
import {styles} from '../../themes';
import {getHeight, moderateScale} from '../../common/constants';
import {StackNav, TabNav} from '../../navigation/NavigationKeys';
import CButton from '../../components/common/CButton';
import typography from '../../themes/typography';
import {AuthContext} from '../../contexts/authContext';
import Spinner from 'react-native-loading-spinner-overlay';
import {successText, validationText} from '../../common/message';
import SuccessModal from '../../components/models/SuccessModal';

const SetValidation = ({navigation, route}) => {
  const colors = useSelector(state => state.theme.theme);
  const data = route.params;
  const {id} = route.params;
  const [pin, setPin] = useState('');
  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setloading] = useState(false);
  const onPressModalClose = () => {
    setModalVisible(false);
    navigation.navigate(StackNav.Login);
  };
  const onPinChange = code => setPin(code);
  useEffect(() => {
    if (pin.length > 5) {
      setIsSubmitDisabled(false);
    } else {
      setIsSubmitDisabled(true);
    }
  }, [pin]);
  const {
    handlers: {POST},
  } = useContext(AuthContext);
  const ValidationConfirm = async () => {
    try {
      setloading(true);
      POST(`/validate_customer/${id}`, false, {
        validation_code: pin,
      })
        .then(res => {
          if (res?.status === 200) {
            console.log(res);
            setloading(false);
            setModalVisible(true);
          }
        })
        .catch(e => {
          console.log(e);
          setloading(false);
          return validationText(e?.response?.data?.response_msg);
        });
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };
  return (
    <CSafeAreaView>
      <Spinner visible={loading} />
      <CHeader title={strings.verifyEmail} />
      <KeyBoardAvoidWrapper contentContainerStyle={styles.flexGrow1}>
        <View style={localStyles.root}>
          <CText type={'r18'} align={'center'}>
            {strings.validationText} {data?.email}
          </CText>
          <OTPInputView
            pinCount={6}
            code={pin}
            onCodeChanged={onPinChange}
            autoFocusOnLoad={false}
            codeInputFieldStyle={[
              localStyles.pinInputStyle,
              {
                color: colors.textColor,
                backgroundColor: colors.inputBg,
                borderColor: colors.borderColor,
              },
            ]}
            codeInputHighlightStyle={{
              borderColor: colors.textColor,
            }}
            style={localStyles.inputStyle}
            secureTextEntry={true}
          />
        </View>
        <CButton
          type={'S16'}
          title={strings.verifyEmail}
          onPress={ValidationConfirm}
          containerStyle={localStyles.btnContainerStyle}
          disabled={isSubmitDisabled}
          bgColor={isSubmitDisabled && colors.disabledColor}
        />
      </KeyBoardAvoidWrapper>
      <SuccessModal
        visible={modalVisible}
        onPressModalClose={onPressModalClose}
        headerTitle={'Амжилттай бүртгэгдлээ'}
        subTitle={
          'Таны хэрэглэгчийн хаяг идэвхжлээ. Хэрэглэчийн нэвтрэх хуудсаар нэвтэрч орно уу.'
        }
      />
    </CSafeAreaView>
  );
};

export default SetValidation;

const localStyles = StyleSheet.create({
  root: {
    ...styles.ph20,
    ...styles.justifyCenter,
    ...styles.flex,
  },
  pinInputStyle: {
    height: getHeight(60),
    width: moderateScale(50),
    borderRadius: moderateScale(15),
    ...typography.fontSizes.f36,
    ...typography.fontWeights.SemiBold,
  },
  btnContainerStyle: {
    ...styles.mh20,
    ...styles.mb10,
  },
  inputStyle: {
    height: getHeight(60),
    ...styles.mv30,
  },
});
