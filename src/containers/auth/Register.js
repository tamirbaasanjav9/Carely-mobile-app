import {useState} from 'react';
// Library Imports
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import React, {memo, useContext, useEffect} from 'react';
import {useSelector} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

// Local Imports
import strings from '../../i18n/strings';
import {styles} from '../../themes';
import CText from '../../components/common/CText';
import {getHeight, moderateScale} from '../../common/constants';
import CHeader from '../../components/common/CHeader';
import CSafeAreaView from '../../components/common/CSafeAreaView';
import {
  Google_Icon,
  Facebook_Icon,
  Apple_Light,
  Apple_Dark,
} from '../../assets/svgs';
import {StackNav} from '../../navigation/NavigationKeys';
import CInput from '../../components/common/CInput';
import KeyBoardAvoidWrapper from '../../components/common/KeyBoardAvoidWrapper';
import {
  validateConfirmPassword,
  validateEmail,
  validatePassword,
  validatePhone,
  validateUserName,
} from '../../utils/validators';
import CButton from '../../components/common/CButton';
import {AuthContext} from '../../contexts/authContext';
import Spinner from 'react-native-loading-spinner-overlay';
import {successText, validationText} from '../../common/message';
import images from '../../assets/images';
import {setOnBoarding} from '../../utils/asyncstorage';

const Register = ({navigation, route}) => {
  console.log(route);
  const ref_id = route?.params?.id;
  const responsibility = route?.params?.responsibilty_ids;

  const colors = useSelector(state => state.theme.theme);

  const BlurredStyle = {
    backgroundColor: colors.inputBg,
    borderColor: colors.bColor,
  };
  const FocusedStyle = {
    borderColor: colors.textColor,
  };
  const BlurredIconStyle = colors.grayScale5;
  const FocusedIconStyle = colors.textColor;

  const socialIcon = [
    {
      icon: <Facebook_Icon />,
      onPress: () => console.log('Facebook'),
    },
    {
      icon: <Google_Icon />,
      onPress: () => console.log('Google'),
    },
    {
      icon: colors.dark === 'dark' ? <Apple_Light /> : <Apple_Dark />,
      onPress: () => console.log('Apple'),
    },
  ];
  //states
  const [state, setState] = useState({
    userName: {
      name: '',
      icon: BlurredIconStyle,
      inputStyle: BlurredIconStyle,
      error: '',
    },
    userPhoneNumber: {
      number: '',
      icon: BlurredIconStyle,
      inputStyle: BlurredIconStyle,
      error: '',
    },
    userEmail: {
      email: '',
      icon: BlurredIconStyle,
      inputStyle: BlurredIconStyle,
      error: '',
    },
    userPassword: {
      password: '',
      icon: BlurredIconStyle,
      inputStyle: BlurredIconStyle,
      error: '',
    },
    rePassword: {
      password: '',
      icon: BlurredIconStyle,
      inputStyle: BlurredIconStyle,
      error: '',
    },
  });
  //icons
  const [emailIcon, setEmailIcon] = useState(BlurredIconStyle);
  const [phoneIcon, setPhoneIcon] = useState(BlurredIconStyle);
  const [userNameIcon, setUserIcon] = useState(BlurredIconStyle);
  const [passwordIcon, setPasswordIcon] = useState(BlurredIconStyle);
  const [rePasswordIcon, setRePasswordIcon] = useState(BlurredIconStyle);

  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);
  //input style
  const [emailInputStyle, setEmailInputStyle] = useState(BlurredStyle);
  const [userInputStyle, setUserInputStyle] = useState(BlurredStyle);
  const [phoneInputStyle, setPhoneInputStyle] = useState(BlurredStyle);
  const [passwordInputStyle, setPasswordInputStyle] = useState(BlurredStyle);
  const [repasswordInputStyle, setRePasswordInputStyle] =
    useState(BlurredStyle);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isRePasswordVisible, setReIsPasswordVisible] = useState(false);

  const [loading, setloading] = useState(false);

  const onFocusInput = onHighlight => onHighlight(FocusedStyle);
  const onFocusIcon = onHighlight => onHighlight(FocusedIconStyle);
  const onBlurInput = onUnHighlight => onUnHighlight(BlurredStyle);
  const onBlurIcon = onUnHighlight => onUnHighlight(BlurredIconStyle);

  useEffect(() => {
    if (
      state?.userEmail?.email.length > 0 &&
      state?.userPassword?.password.length > 0 &&
      state?.rePassword?.password.length > 0 &&
      state?.userName?.name.length > 0 &&
      state?.userPhoneNumber?.number.length > 0 &&
      !state?.userEmail?.error &&
      !state?.userPassword?.error &&
      !state?.rePassword?.error &&
      !state?.userName?.error &&
      !state?.userPhoneNumber?.error
    ) {
      setIsSubmitDisabled(false);
    } else {
      setIsSubmitDisabled(true);
    }
  }, [
    state?.userName?.name,
    state?.userEmail?.email,
    state?.userPassword?.password,
    state?.userPhoneNumber?.number,
    state?.userName?.error,
    state?.userEmail?.error,
    state?.userPassword?.error,
    state?.rePassword?.error,
    state?.userPhoneNumber?.error,
  ]);
  const {
    handlers: {POST},
  } = useContext(AuthContext);
  //хэрэглэгчийн бүртгэл
  const CreateUser = async () => {
    const jsonData = {
      ref: ref_id,
      responsibility: responsibility,
    };
    const myJSON = JSON.stringify(jsonData);
    const body = new FormData();
    body.append('Email', state?.userEmail?.email);
    body.append('Password', state?.userPassword?.password);
    body.append('Name', state?.userName?.name);
    body.append('PhoneNumber', state?.userPhoneNumber?.number);
    body.append('FilterJson', myJSON);
    console.log(body, 'check register body');
    try {
      setloading(true);
      await POST(`/create_customer`, false, body, 'multipart/form-data')
        .then(res => {
          if (res?.status === 200) {
            setOnBoarding(true);
            setloading(false);
            navigation.navigate(StackNav.Auth, res?.data);
            return successText('Амжилттай бүртгэгдлээ');
          }
        })
        .catch(e => {
          console.log(e);
          setloading(false);
          return validationText(e?.response?.data?.response_msg);
        });
    } catch (e) {
      console.log(e);
      setloading(false);
      return e;
    }
  };
  // onHandling inputs
  const onChangedEmail = val => {
    const {msg} = validateEmail(val.trim());
    setState({
      ...state,
      userEmail: {
        ...state.userEmail,
        email: val.trim(),
        error: msg,
      },
    });
  };
  const onChangedUserName = val => {
    const {msg} = validateUserName(val.trim());
    setState({
      ...state,
      userName: {
        ...state.userName,
        name: val.trim(),
        error: msg,
      },
    });
  };
  const onChangedPhoneNumber = val => {
    const {msg} = validatePhone(val.trim());
    setState({
      ...state,
      userPhoneNumber: {
        ...state.userPhoneNumber,
        number: val.trim(),
        error: msg,
      },
    });
  };
  const onChangedPassword = val => {
    const {msg} = validatePassword(val.trim());
    setState({
      ...state,
      userPassword: {
        ...state.userPassword,
        password: val.trim(),
        error: msg,
      },
    });
  };

  const onChangedRePassword = val => {
    const {msg} = validateConfirmPassword(
      val.trim(),
      state?.userPassword?.password,
    );
    setState({
      ...state,
      rePassword: {
        ...state.rePassword,
        password: val.trim(),
        error: msg,
      },
    });
  };

  const RenderSocialBtn = memo(({item, index}) => {
    return (
      <TouchableOpacity
        key={index}
        onPress={item.onPress}
        style={[
          localStyles.socialBtn,
          {
            backgroundColor: colors.inputBg,
            borderColor: colors.bColor,
          },
        ]}>
        {item.icon}
      </TouchableOpacity>
    );
  });

  const onPressPasswordEyeIcon = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };
  const onRePressPasswordEyeIcon = () => {
    setReIsPasswordVisible(!isRePasswordVisible);
  };

  const EmailIcon = () => {
    return <Ionicons name="mail" size={moderateScale(20)} color={emailIcon} />;
  };
  const UserIcon = () => {
    return (
      <FontAwesome5 name="user" size={moderateScale(20)} color={userNameIcon} />
    );
  };
  const PhoneIcon = () => {
    return (
      <MaterialIcons name="phone" size={moderateScale(20)} color={phoneIcon} />
    );
  };
  const PasswordIcon = () => (
    <Ionicons
      name="lock-closed"
      size={moderateScale(20)}
      color={passwordIcon}
    />
  );
  const RePasswordIcon = () => (
    <Ionicons
      name="lock-closed"
      size={moderateScale(20)}
      color={rePasswordIcon}
    />
  );
  // email focus
  const onFocusEmail = () => {
    onFocusInput(setEmailInputStyle);
    onFocusIcon(setEmailIcon);
  };

  const onBlurEmail = () => {
    onBlurInput(setEmailInputStyle);
    onBlurIcon(setEmailIcon);
  };
  //user name focus
  const onFocusUserName = () => {
    onFocusInput(setUserInputStyle);
    onFocusIcon(setUserIcon);
  };
  const onBlurUser = () => {
    onBlurInput(setUserInputStyle);
    onBlurIcon(setUserIcon);
  };
  // phone number container focus
  const onFocusPhoneNumber = () => {
    onFocusInput(setPhoneInputStyle);
    onFocusIcon(setPhoneIcon);
  };
  const onBlurPhoneNumber = () => {
    onBlurInput(setPhoneInputStyle);
    onBlurIcon(setPhoneIcon);
  };

  const onFocusPassword = () => {
    onFocusInput(setPasswordInputStyle);
    onFocusIcon(setPasswordIcon);
  };
  const onBlurPassword = () => {
    onBlurInput(setPasswordInputStyle);
    onBlurIcon(setPasswordIcon);
  };
  const onFocusRePassword = () => {
    onFocusInput(setRePasswordInputStyle);
    onFocusIcon(setRePasswordIcon);
  };
  const onBlurRePassword = () => {
    onBlurInput(setRePasswordInputStyle);
    onBlurIcon(setRePasswordIcon);
  };
  const RightPasswordEyeIcon = () => (
    <TouchableOpacity
      onPress={onPressPasswordEyeIcon}
      style={localStyles.eyeIconContainer}>
      <Ionicons
        name={isPasswordVisible ? 'eye-off' : 'eye'}
        size={moderateScale(20)}
        color={passwordIcon}
      />
    </TouchableOpacity>
  );
  const RightRePasswordEyeIcon = () => (
    <TouchableOpacity
      onPress={onRePressPasswordEyeIcon}
      style={localStyles.eyeIconContainer}>
      <Ionicons
        name={isRePasswordVisible ? 'eye-off' : 'eye'}
        size={moderateScale(20)}
        color={rePasswordIcon}
      />
    </TouchableOpacity>
  );

  const onPressSignIn = () => {
    navigation.navigate(StackNav.Auth);
  };

  return (
    <CSafeAreaView>
      <Spinner visible={loading} />
      <CHeader />
      <KeyBoardAvoidWrapper>
        <View style={localStyles.mainContainer}>
          <Image
            source={colors.dark ? images.carelyLogo : images.carelyLogo}
            style={localStyles.logoStyle}
          />
          <CInput
            placeHolder={strings.email}
            keyBoardType={'email-address'}
            _value={state?.userEmail?.email}
            _errorText={state?.userEmail?.error}
            autoCapitalize={'none'}
            insideLeftIcon={() => <EmailIcon />}
            toGetTextFieldValue={onChangedEmail}
            inputContainerStyle={[
              {backgroundColor: colors.inputBg},
              localStyles.inputContainerStyle,
              emailInputStyle,
            ]}
            inputBoxStyle={[localStyles.inputBoxStyle]}
            _onFocus={onFocusEmail}
            onBlur={onBlurEmail}
          />
          <CInput
            placeHolder={strings.UserName}
            keyBoardType={'default'}
            _value={state?.userName?.name}
            _errorText={state?.userName?.error}
            autoCapitalize={'none'}
            insideLeftIcon={() => <UserIcon />}
            toGetTextFieldValue={onChangedUserName}
            inputContainerStyle={[
              {backgroundColor: colors.inputBg},
              localStyles.inputContainerStyle,
              userInputStyle,
            ]}
            inputBoxStyle={[localStyles.inputBoxStyle]}
            _onFocus={onFocusUserName}
            onBlur={onBlurUser}
          />
          <CInput
            placeHolder={strings.phoneNumber}
            keyBoardType={'number-pad'}
            _value={state?.userPhoneNumber?.number}
            _errorText={state?.userPhoneNumber?.error}
            autoCapitalize={'none'}
            insideLeftIcon={() => <PhoneIcon />}
            toGetTextFieldValue={onChangedPhoneNumber}
            inputContainerStyle={[
              {backgroundColor: colors.inputBg},
              localStyles.inputContainerStyle,
              phoneInputStyle,
            ]}
            inputBoxStyle={[localStyles.inputBoxStyle]}
            _onFocus={onFocusPhoneNumber}
            onBlur={onBlurPhoneNumber}
          />

          <CInput
            placeHolder={strings.password}
            keyBoardType={'default'}
            _value={state?.userPassword?.password}
            _errorText={state?.userPassword?.error}
            autoCapitalize={'none'}
            insideLeftIcon={() => <PasswordIcon />}
            toGetTextFieldValue={onChangedPassword}
            inputContainerStyle={[
              {backgroundColor: colors.inputBg},
              localStyles.inputContainerStyle,
              passwordInputStyle,
            ]}
            _isSecure={!isPasswordVisible}
            inputBoxStyle={[localStyles.inputBoxStyle]}
            _onFocus={onFocusPassword}
            onBlur={onBlurPassword}
            rightAccessory={() => <RightPasswordEyeIcon />}
          />
          <CInput
            placeHolder={strings.rePassword}
            keyBoardType={'default'}
            _value={state?.rePassword?.password}
            _errorText={state?.rePassword?.error}
            autoCapitalize={'none'}
            insideLeftIcon={() => <RePasswordIcon />}
            toGetTextFieldValue={onChangedRePassword}
            inputContainerStyle={[
              {backgroundColor: colors.inputBg},
              localStyles.inputContainerStyle,
              repasswordInputStyle,
            ]}
            _isSecure={!isRePasswordVisible}
            inputBoxStyle={[localStyles.inputBoxStyle]}
            _onFocus={onFocusRePassword}
            onBlur={onBlurRePassword}
            rightAccessory={() => <RightRePasswordEyeIcon />}
          />
          <CButton
            title={strings.signUp}
            type={'S16'}
            color={isSubmitDisabled && colors.white}
            containerStyle={[localStyles.signBtnContainer]}
            // onPress={CreateUser()}
            onPress={() => {
              CreateUser();
            }}
            bgColor={isSubmitDisabled && colors.disabledColor}
            disabled={isSubmitDisabled}
          />
          <View style={localStyles.divider}>
            <View
              style={[
                localStyles.orContainer,
                {backgroundColor: colors.bColor},
              ]}
            />
            <CText type={'m17'} style={styles.mh10}>
              {strings.orContinueWith}
            </CText>
            <View
              style={[
                localStyles.orContainer,
                {backgroundColor: colors.bColor},
              ]}
            />
          </View>

          <View style={localStyles.socialBtnContainer}>
            {socialIcon.map((item, index) => (
              <RenderSocialBtn item={item} key={index.toString()} />
            ))}
          </View>

          <TouchableOpacity
            onPress={onPressSignIn}
            style={localStyles.signUpContainer}>
            <CText
              type={'b16'}
              color={colors.dark ? colors.grayScale7 : colors.grayScale5}>
              {strings.AlreadyHaveAccount}
            </CText>
            <CText type={'b16'}> {strings.signIn}</CText>
          </TouchableOpacity>
        </View>
      </KeyBoardAvoidWrapper>
    </CSafeAreaView>
  );
};

export default Register;

const localStyles = StyleSheet.create({
  mainContainer: {
    ...styles.ph20,
  },
  divider: {
    ...styles.rowCenter,
    ...styles.mv20,
  },
  orContainer: {
    height: moderateScale(1),
    width: '30%',
  },
  signBtnContainer: {
    ...styles.center,
    width: '100%',
    ...styles.mv20,
  },
  signUpContainer: {
    ...styles.rowCenter,
    ...styles.mv20,
  },
  inputContainerStyle: {
    height: getHeight(60),
    borderRadius: moderateScale(12),
    borderWidth: moderateScale(1),
    ...styles.ph15,
  },
  inputBoxStyle: {
    ...styles.ph15,
  },
  socialBtnContainer: {
    ...styles.rowCenter,
    ...styles.mv20,
  },
  socialBtn: {
    ...styles.center,
    height: getHeight(60),
    width: moderateScale(90),
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
    ...styles.mh10,
  },
  logoStyle: {
    width: moderateScale(200),
    height: moderateScale(100),
    resizeMode: 'contain',
    ...styles.selfCenter,
  },
});
