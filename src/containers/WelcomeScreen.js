import {Image, StyleSheet, View} from 'react-native';
import React, {useEffect} from 'react';
import {styles} from '../themes';
import {deviceHeight, deviceWidth} from '../common/constants';
import images from '../assets/images';
import CText from '../components/common/CText';
import {useSelector} from 'react-redux';
import {StackNav} from '../navigation/NavigationKeys';
import padding from '../themes/padding';

export default function WelcomeScreen({navigation}) {
  const colors = useSelector(state => state.theme.theme);

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate(StackNav.onBoarding);
    }, 300);
  }, []);

  return (
    <View style={[styles.flex]}>
      <View style={{padding: 50}}>
        <Image
          source={images.carelyLogo}
          style={localStyles.imageStyle}
          resizeMode="contain"
        />
      </View>
      <View style={localStyles.bottomTextContainer}></View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  imageStyle: {
    width: deviceWidth - 100,
    height: deviceHeight - 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomTextContainer: {
    position: 'absolute',
    bottom: 0,
    ...styles.ph20,
    ...styles.pb30,
  },
});
