// Libraries import
import React from 'react';
import {Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

// Local import
import {getHeight, moderateScale} from '../../../common/constants';
import CButton from '../../../components/common/CButton';
import CText from '../../../components/common/CText';
import {Modal_IconDark, Modal_IconLight} from '../../../assets/svgs';
import {commonColor, styles} from '../../../themes';
import {Image} from 'react-native';
import variables from '../../../api/variables';

const TypeModal = props => {
  const colors = useSelector(state => state.theme.theme);

  const {
    visible,
    onPressModalClose,
    btnText1 = false,
    btnText2 = false,
    btnText3 = false,
    onPressBtn1,
    onPressBtn2,
    onPressBtn3,
    headerTitle,
    subTitle,
    itemImage,
  } = props;

  return (
    <Modal animationType="fade" transparent={true} visible={visible}>
      <TouchableOpacity
        style={localStyles.modalMainContainer}
        activeOpacity={1}
        onPress={onPressModalClose}>
        <TouchableOpacity
          activeOpacity={1}
          style={[localStyles.root, {backgroundColor: colors.inputBg}]}>
          {!!itemImage ? (
            <Image
              source={{
                uri: `${variables.GENERAL_IMAGE_URL}${itemImage}`,
              }}
              style={[
                styles.selfCenter,
                {
                  height: moderateScale(150),
                  width: moderateScale(200),
                  borderRadius: moderateScale(15),
                },
              ]}
            />
          ) : colors.dark ? (
            <Modal_IconDark style={styles.selfCenter} />
          ) : (
            <Modal_IconLight style={styles.selfCenter} />
          )}

          {!!headerTitle && (
            <CText type={'b20'} align={'center'} style={styles.mt25}>
              {headerTitle}
            </CText>
          )}
          <CText type={'r15'} align={'center'} style={styles.mt25}>
            {!!subTitle ? subTitle : strings.modalDesc}
          </CText>
          {!!btnText1 && (
            <CButton
              title={btnText1}
              type={'S14'}
              containerStyle={localStyles.signBtnContainer}
              onPress={onPressBtn1}
            />
          )}
          {!!btnText2 && (
            <CButton
              title={btnText2}
              type={'S14'}
              // color={!!colors.dark ? colors.white : colors.primary}
              bgColor={colors.dark3}
              containerStyle={localStyles.signBtnContainer}
              onPress={onPressBtn2}
            />
          )}
          {!!btnText3 && (
            <CButton
              title={btnText3}
              type={'S14'}
              // color={!!colors.dark ? colors.white : colors.primary}
              bgColor={colors.dark3}
              containerStyle={localStyles.signBtnContainer}
              onPress={onPressBtn3}
            />
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    </Modal>
  );
};

const localStyles = StyleSheet.create({
  root: {
    ...styles.p30,
    ...styles.mh30,
    borderRadius: moderateScale(15),
  },
  modalMainContainer: {
    ...styles.flex,
    ...styles.center,
    backgroundColor: commonColor.modalBg,
  },
  signBtnContainer: {
    ...styles.mt20,
    ...styles.mh10,
    height: getHeight(45),
    borderRadius: moderateScale(22),
    width: moderateScale(200),
    ...styles.selfCenter,
  },
});

export default TypeModal;
