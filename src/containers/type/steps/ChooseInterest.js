import React, {useState} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';

// Custom Imports
import CText from '../../../components/common/CText';
import PaymentSelect from '../../../components/cartComponent/PaymentSelect';

// redux
import {styles} from '../../../themes';
import {deviceWidth, moderateScale} from '../../../common/constants';

const ChooseInterest = ({data, ratio, onRadioData, isbuttonBoolean}) => {
  const [isSelected, setIsSelected] = useState([]);

  const [singleselect, setSingleSelect] = useState([]);

  const onRadioSelect = value => {
    // Clear the array and add the new selected item
    setSingleSelect(value === singleselect ? null : value);
    onRadioData(value.id);
    isbuttonBoolean(true);
  };
  const onCheckItems = value => {
    if (isSelected.includes(value)) {
      console.log('already in the array');
      // If the value is already in the array, remove it
      setIsSelected(prevSelectedData =>
        prevSelectedData.filter(item => item !== value),
      );
    } else {
      console.log('not value here in array');

      // If the value is not in the array, add it
      setIsSelected(prevSelectedData => [...prevSelectedData, value]);
      onRadioData(value.id);
      isbuttonBoolean(true);
    }
  };
  //render next input
  const renderPaymentItem = ({item, index}) => {
    return (
      <PaymentSelect
        item={item}
        isSelected={
          ratio === false ? isSelected.includes(item) : singleselect === item
        }
        onPressItem={() =>
          ratio === false ? onCheckItems(item) : onRadioSelect(item)
        }
        ratio={ratio}
      />
    );
  };

  return (
    <View style={{}}>
      <FlatList
        data={data?.Responsibility?.ResponsibilityOption}
        renderItem={renderPaymentItem}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={
          <View>
            <CText type={'m18'} align={'left'} style={styles.mv25}>
              {data?.Responsibility?.responsibility_question}
            </CText>
          </View>
        }
        // ListFooterComponent={
        //   <View>
        //     {isSelected !== null ? (
        //       <View
        //         style={[
        //           localStyles.renderText,
        //           {backgroundColor: commonColor.primaryTransparent},
        //         ]}>
        //         <AntDesign
        //           style={styles.m15}
        //           color={commonColor.grayScale5}
        //           name="questioncircle"
        //           size={moderateScale(25)}
        //         />
        //         <CText
        //           type={'m15'}
        //           align={'center'}
        //           style={[styles.mb15, styles.mh15]}>
        //           {isSelected?.responsibility_description}
        //         </CText>
        //       </View>
        //     ) : null}
        //   </View>
        // }
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};
export default ChooseInterest;
const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mh5,
    ...styles.mb25,
    width: deviceWidth / 2 - moderateScale(30),
  },
  iconContainer: {
    width: deviceWidth / 2 - moderateScale(50),
    height: deviceWidth / 2 - moderateScale(50),
    borderRadius: moderateScale(45),
    ...styles.center,
  },
  renderText: {
    ...styles.mt10,
    ...styles.flexCenter,
    borderRadius: moderateScale(15),
  },
});
