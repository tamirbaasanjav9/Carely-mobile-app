import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';

// Custom Imports
import CText from '../../../components/common/CText';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Dropdown} from 'react-native-element-dropdown';

// redux
import {commonColor, styles} from '../../../themes';
import {getHeight, moderateScale} from '../../../common/constants';
import useBoolean from '../../../contexts/useBoolean';

const ChoosePet = ({
  data,
  ratio,
  secondData,
  thirdData,
  DogCount,
  PussCount,
  OtherPet,
  isbuttonBoolean,
}) => {
  // boolean onFocus on dropdown
  const isFocus = useBoolean(false);
  const isFocusSecond = useBoolean(false);
  const isFocusthird = useBoolean(false);

  // selected Value change
  const [value, setValue] = useState(null);
  const [secondValue, setSecondValue] = useState(null);
  const [thirdValue, setThirdValue] = useState(null);

  useEffect(() => {
    if (!!value && !!secondValue && !!thirdValue) {
      isbuttonBoolean(true);
    } else {
      isbuttonBoolean(false);
    }
  }, [value, secondValue, thirdValue]);
  // Render dropDown lists
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>
          {item?.responsibility_option}
        </CText>
        {item.id === value && (
          <AntDesign
            // style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderSecondItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>
          {item?.responsibility_option}
        </CText>
        {item.id === secondValue && (
          <AntDesign
            // style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderThirdItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>
          {item?.responsibility_option}
        </CText>
        {item.id === thirdValue && (
          <AntDesign
            // style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  return (
    <View>
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={
          // first section
          <View>
            <CText type={'m18'} align={'left'} style={styles.mv25}>
              {data?.Responsibility?.responsibility_question}
            </CText>
            <Dropdown
              style={[
                localStyles.dropdown,
                isFocus.value && {borderColor: commonColor.carelyLogoColor},
              ]}
              placeholderStyle={localStyles.placeholderStyle}
              selectedTextStyle={localStyles.selectedTextStyle}
              inputSearchStyle={localStyles.inputSearchStyle}
              iconStyle={localStyles.iconStyle}
              data={data?.Responsibility?.ResponsibilityOption}
              search
              maxHeight={300}
              labelField="responsibility_option"
              valueField="id"
              placeholder={!isFocus.value ? 'Сонгонo уу...' : '...'}
              searchPlaceholder="Хайх..."
              value={value}
              onFocus={() => isFocus.setTrue()}
              onBlur={() => isFocus.setFalse()}
              onChange={item => {
                setValue(item.id);
                isFocus.setFalse();
                DogCount(item.id);
              }}
              renderLeftIcon={() => (
                <Ionicons
                  style={{...styles.mr10}}
                  color={commonColor.carelyLogoColor}
                  name="add-circle-outline"
                  size={moderateScale(25)}
                />
              )}
              renderItem={renderItem}
            />
          </View>
        }
        ListFooterComponent={
          // second section
          <View>
            <CText type={'m18'} align={'left'} style={styles.mv25}>
              {secondData?.Responsibility?.responsibility_question}
            </CText>
            <Dropdown
              style={[
                localStyles.dropdown,
                isFocusSecond.value && {
                  borderColor: commonColor.carelyLogoColor,
                },
              ]}
              placeholderStyle={localStyles.placeholderStyle}
              selectedTextStyle={localStyles.selectedTextStyle}
              inputSearchStyle={localStyles.inputSearchStyle}
              iconStyle={localStyles.iconStyle}
              data={secondData?.Responsibility?.ResponsibilityOption}
              search
              maxHeight={300}
              labelField="responsibility_option"
              valueField="id"
              placeholder={!isFocusSecond.value ? 'Сонгонo уу...' : '...'}
              searchPlaceholder="Хайх..."
              value={secondValue}
              onFocus={() => isFocusSecond.setTrue()}
              onBlur={() => isFocusSecond.setFalse()}
              onChange={item => {
                setSecondValue(item.id);
                isFocusSecond.setFalse();
                PussCount(item.id);
              }}
              renderLeftIcon={() => (
                <Ionicons
                  style={{...styles.mr10}}
                  color={commonColor.carelyLogoColor}
                  name="add-circle-outline"
                  size={moderateScale(25)}
                />
              )}
              renderItem={renderSecondItem}
            />

            {!!thirdData && (
              //third section
              <View>
                <CText type={'m18'} align={'left'} style={styles.mv25}>
                  {thirdData?.Responsibility?.responsibility_question}
                </CText>
                <Dropdown
                  style={[
                    localStyles.dropdown,
                    isFocusthird.value && {
                      borderColor: commonColor.carelyLogoColor,
                    },
                  ]}
                  placeholderStyle={localStyles.placeholderStyle}
                  selectedTextStyle={localStyles.selectedTextStyle}
                  inputSearchStyle={localStyles.inputSearchStyle}
                  iconStyle={localStyles.iconStyle}
                  data={thirdData?.Responsibility?.ResponsibilityOption}
                  search
                  maxHeight={300}
                  labelField="responsibility_option"
                  valueField="id"
                  placeholder={!isFocusthird.value ? 'Сонгонo уу...' : '...'}
                  searchPlaceholder="Хайх..."
                  value={thirdValue}
                  onFocus={() => isFocusthird.setTrue()}
                  onBlur={() => isFocusthird.setFalse()}
                  onChange={item => {
                    setThirdValue(item.id);
                    isFocusthird.setFalse();
                    OtherPet(item.id);
                  }}
                  renderLeftIcon={() => (
                    <Ionicons
                      style={{...styles.mr10}}
                      color={commonColor.carelyLogoColor}
                      name="add-circle-outline"
                      size={moderateScale(25)}
                    />
                  )}
                  renderItem={renderThirdItem}
                />
              </View>
            )}
          </View>
        }
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};
export default ChoosePet;
const localStyles = StyleSheet.create({
  quantityButton: {
    height: getHeight(30),
    ...styles.ph10,
    ...styles.rowCenter,
    borderRadius: moderateScale(15),
  },
  quantityText: {
    width: moderateScale(20),
  },
  quantityContainer: {
    height: moderateScale(30),
    width: moderateScale(30),
    ...styles.center,
    borderRadius: moderateScale(15),
  },
  dropdown: {
    height: moderateScale(50),
    borderColor: commonColor.grayScale5,
    borderWidth: 1,
    borderRadius: moderateScale(10),
    ...styles.ph10,
    ...styles.mb25,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
});
