import React, {useState} from 'react';
import {View} from 'react-native';

// Custom Imports
import CText from '../../../components/common/CText';

// redux
import {styles} from '../../../themes';
import {FlatList} from 'react-native';
import PaymentSelect from '../../../components/cartComponent/PaymentSelect';

const ChooseWhen = ({
  data,
  ratio,
  onRadioData,
  isbuttonBoolean,
  onWhenFunction,
  PetService,
}) => {
  const [isSelected, setIsSelected] = useState([]);
  console.log(isSelected);
  const [singleselect, setSingleSelect] = useState([]);

  const onRadioSelect = value => {
    // Clear the array and add the new selected item
    setSingleSelect(value);
    onRadioData(value.id);
    onWhenFunction(value?.id);
    isbuttonBoolean(true);
    console.log('asd');
  };
  const onCheckItems = value => {
    console.log(value);
    if (isSelected.includes(value)) {
      console.log('already in the array');
      // If the value is already in the array, remove it
      setIsSelected(prevSelectedData =>
        prevSelectedData.filter(item => item !== value),
      );
    } else {
      // If the value is not in the array, add it
      setIsSelected(prevSelectedData => [...prevSelectedData, value]);
      isbuttonBoolean(true);
      PetService(isSelected);
    }
  };
  //render next input
  const renderPaymentItem = ({item, index}) => {
    return (
      <PaymentSelect
        item={item}
        isSelected={
          ratio === false ? isSelected.includes(item) : singleselect === item
        }
        onPressItem={() =>
          ratio === false ? onCheckItems(item) : onRadioSelect(item)
        }
        ratio={ratio}
      />
    );
  };

  return (
    <View style={{...styles.mb55}}>
      <FlatList
        data={data?.Responsibility?.ResponsibilityOption}
        renderItem={renderPaymentItem}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={
          <View>
            <CText type={'m18'} align={'left'} style={styles.mv25}>
              {data?.Responsibility?.responsibility_question}
            </CText>
          </View>
        }
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};
export default ChooseWhen;
