import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';

// dependencies npm
import {Dropdown} from 'react-native-element-dropdown';

// icon import dependencies
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

// Custom Imports
import CText from '../../../components/common/CText';
import {moderateScale} from '../../../common/constants';

// redux
import {commonColor, styles} from '../../../themes';
import useBoolean from '../../../contexts/useBoolean';
import useService from '../../../contexts/useService';

const ChooseLocation = ({
  data,
  isbuttonBoolean,
  onRadioData,
  onSelectAimag,
  onSelectDistrict,
  onSelectKhoroo,
}) => {
  // onFocus change color boolean
  const [isFocus, setIsFocus] = useState(false);

  // render selected value
  const [value, setValue] = useState(null);
  const [districtValue, setDistrictValue] = useState(null);
  const [khorooValue, setKhorooValue] = useState(null);

  //render select dropdown boolean
  const districtState = useBoolean();
  const districtKhoroo = useBoolean();
  const loadingState = useBoolean();

  // fetch data
  const {data: aimagData, fetchData: fetchAimag, isLoading} = useService();
  const {
    data: districtData,
    fetchData: fetchDistrict,
    isLoading: districtLoading,
  } = useService();
  const {
    data: khorooData,
    fetchData: fetchKhoroo,
    isLoading: khorooLoading,
  } = useService();

  // Call service
  useEffect(() => {
    fetchAimag('POST', '/get_aimag_list', false, {});
  }, []);

  // handle on district item call service
  const handleDistrict = async id => {
    loadingState.setTrue();
    await fetchDistrict('POST', '/get_district_list', false, {
      filter: [
        {
          field_name: 'aimag_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  // handle on Khoroo item call service
  const handleKhoroo = async id => {
    loadingState.setTrue();
    await fetchKhoroo('POST', '/get_khoroo_list', false, {
      filter: [
        {
          field_name: 'district_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  // render data on dropDown
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.aimag_name}</CText>
        {item.id === value && (
          <AntDesign
            // style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderDistrict = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.district_name}</CText>
        {item.id === districtValue && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderKhoroo = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.khoroo_name}</CText>
        {item.id === khorooValue && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };

  // Render on screen items -------------------------------------------
  return (
    <View>
      <CText type={'m18'} align={'left'} style={styles.mv25}>
        {data?.Responsibility?.responsibility_question}
      </CText>
      {isLoading !== true ? (
        <Dropdown
          style={[
            localStyles.dropdown,
            isFocus && {borderColor: commonColor.carelyLogoColor},
          ]}
          placeholderStyle={localStyles.placeholderStyle}
          selectedTextStyle={localStyles.selectedTextStyle}
          inputSearchStyle={localStyles.inputSearchStyle}
          iconStyle={localStyles.iconStyle}
          data={aimagData?.data}
          search
          maxHeight={300}
          labelField="aimag_name"
          valueField="id"
          placeholder={!isFocus ? 'Аймаг, хот сонгох' : '...'}
          searchPlaceholder="Хайх..."
          value={value}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            setValue(item.id);
            handleDistrict(item?.id);
            setIsFocus(false);
            districtState.setTrue();
            districtKhoroo.setFalse();
            onSelectAimag(item.id);
            isbuttonBoolean(false);
          }}
          renderLeftIcon={() => (
            <EvilIcons
              style={{...styles.mr10}}
              color={commonColor.carelyLogoColor}
              name="location"
              size={moderateScale(25)}
            />
          )}
          renderItem={renderItem}
        />
      ) : null}
      {districtState.value === true && districtLoading !== true ? (
        <Dropdown
          style={[
            localStyles.dropdown,
            isFocus && {borderColor: commonColor.carelyLogoColor},
          ]}
          placeholderStyle={localStyles.placeholderStyle}
          selectedTextStyle={localStyles.selectedTextStyle}
          inputSearchStyle={localStyles.inputSearchStyle}
          iconStyle={localStyles.iconStyle}
          data={districtData?.data}
          search
          maxHeight={300}
          labelField="district_name"
          valueField="id"
          placeholder={!isFocus ? 'Сум, дүүрэг сонгох' : '...'}
          searchPlaceholder="Хайх..."
          value={districtValue}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            onSelectDistrict(item.id);
            setDistrictValue(item.id);
            handleKhoroo(item.id);
            setIsFocus(false);
            districtKhoroo.setTrue();
          }}
          renderLeftIcon={() => (
            <EvilIcons
              style={{...styles.mr10}}
              color={commonColor.carelyLogoColor}
              name="location"
              size={moderateScale(25)}
            />
          )}
          renderItem={renderDistrict}
        />
      ) : null}
      {districtKhoroo.value === true && khorooLoading !== true ? (
        <Dropdown
          style={[
            localStyles.dropdown,
            isFocus && {borderColor: commonColor.carelyLogoColor},
          ]}
          placeholderStyle={localStyles.placeholderStyle}
          selectedTextStyle={localStyles.selectedTextStyle}
          inputSearchStyle={localStyles.inputSearchStyle}
          iconStyle={localStyles.iconStyle}
          data={khorooData?.data}
          search
          maxHeight={300}
          labelField="khoroo_name"
          valueField="id"
          placeholder={!isFocus ? 'Баг, Хороо сонгох' : '...'}
          searchPlaceholder="Хайх..."
          value={khorooValue}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            onSelectKhoroo(item.id);
            setKhorooValue(item.id);
            setIsFocus(false);
            isbuttonBoolean(true);
          }}
          renderLeftIcon={() => (
            <EvilIcons
              style={{...styles.mr10}}
              color={commonColor.carelyLogoColor}
              name="location"
              size={moderateScale(25)}
            />
          )}
          renderItem={renderKhoroo}
        />
      ) : null}
    </View>
  );
};
export default ChooseLocation;

const localStyles = StyleSheet.create({
  dropdown: {
    height: moderateScale(50),
    borderColor: commonColor.grayScale5,
    borderWidth: 1,
    borderRadius: moderateScale(10),
    ...styles.ph10,
    ...styles.mb25,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
});
