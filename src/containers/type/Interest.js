import {FlatList, StyleSheet, View} from 'react-native';
import React, {useState} from 'react';

// redux and custom dialog
import {colors, commonColor, styles} from '../../themes';
import {StackNav} from '../../navigation/NavigationKeys';
import AntDesign from 'react-native-vector-icons/AntDesign';

// Custom Imports
import {deviceWidth, moderateScale} from '../../common/constants';
import CSafeAreaView from '../../components/common/CSafeAreaView';
import CHeader from '../../components/common/CHeader';
import CText from '../../components/common/CText';
import PaymentSelect from '../../components/cartComponent/PaymentSelect';
import CButton from '../../components/common/CButton';
import strings from '../../i18n/strings';

export default function Interest({navigation, route}) {
  const {index2} = route?.params;
  const [isSelected, setIsSelected] = useState(null);

  // onpress bottom button
  const onPressAdd = () => {
    navigation.navigate(StackNav.When, {});
  };

  // toggle selection items
  const onHandleResponsibility = value => {
    console.log(value);
    // Clear the array and add the new selected item
    setIsSelected(value === isSelected ? null : value);
  };

  // render maping items
  const renderPaymentItem = ({item, index}) => {
    return (
      <PaymentSelect
        item={item}
        isSelected={isSelected === item}
        onPressItem={() => onHandleResponsibility(item)}
      />
    );
  };

  // render main  ----------------------------------------
  return (
    <CSafeAreaView>
      <CHeader />
      <View style={localStyles.root}>
        <FlatList
          data={index2?.Responsibility?.ResponsibilityOption}
          renderItem={renderPaymentItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={
            <View>
              <CText type={'m17'} align={'left'} style={styles.mb10}>
                {'Алхам 1-4'}
              </CText>
              <CText type={'s18'} align={'left'} style={styles.mb25}>
                {index2?.Responsibility?.responsibility_question}
              </CText>
            </View>
          }
          ListFooterComponent={
            <View>
              {isSelected !== null ? (
                <View
                  style={[
                    localStyles.renderText,
                    {backgroundColor: commonColor.primaryTransparent},
                  ]}>
                  <AntDesign
                    style={styles.m15}
                    color={commonColor.grayScale5}
                    name="questioncircle"
                    size={moderateScale(25)}
                  />
                  <CText
                    type={'m15'}
                    align={'center'}
                    style={[styles.mb15, styles.mh15]}>
                    {isSelected?.responsibility_description}
                  </CText>
                </View>
              ) : null}
            </View>
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={styles.ph20}>
        <CButton
          title={strings.apply}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mh5,
    ...styles.mb25,
    width: deviceWidth / 2 - moderateScale(30),
  },
  iconContainer: {
    width: deviceWidth / 2 - moderateScale(50),
    height: deviceWidth / 2 - moderateScale(50),
    borderRadius: moderateScale(45),
    ...styles.center,
  },
  renderText: {
    ...styles.mt10,
    ...styles.flexCenter,
    borderRadius: moderateScale(15),
  },
});
