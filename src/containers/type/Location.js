// General imports
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import React, {useState, useEffect} from 'react';

// library dependencies
import StepIndicator from 'react-native-step-indicator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {StackNav} from '../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../components/common/CSafeAreaView';
import ChooseLocation from './steps/ChooseLocation';
import ChooseWhen from './steps/ChooseWhen';
import ChooseInterest from './steps/ChooseInterest';
import ChooseCount from './steps/ChooseCount';

// layout and color styling imports
import strings from '../../i18n/strings';
import {commonColor, styles} from '../../themes';
import {moderateScale, wp} from '../../common/constants';
import useService from '../../contexts/useService';

//default Components
import CHeader from '../../components/common/CHeader';
import CButton from '../../components/common/CButton';
import {
  PetLabelName,
  careChildLabelName,
  customStylesStep,
  houseKeeperLabelNames,
  seniorLabelName,
  seniorOrpanLabelName,
  teacherLabelName,
} from '../../api/constant';
import useBoolean from '../../contexts/useBoolean';
import CText from '../../components/common/CText';
import ChooseType from './steps/ChooseType';
import ChoosePet from './steps/CountPet';

export default function Location({navigation, route}) {
  // Arrived data previous Screen
  const {category_id, headerTitle} = route?.params;
  const {data, fetchData, isLoading} = useService(); // call service custom
  // custom states {counting components && checking label names }
  const [currentPosition, setCurrentPosition] = useState(0);
  const [labelNames, setLabelNames] = useState([null]);
  // Pass data to register &&states
  const [selectedValue, setSelectedValue] = useState([]);
  const [isBooleanButton, setIsBooleanButton] = useState(false);
  const alertBoolean = useBoolean();
  const [state, setStates] = useState({
    when_id: null,
    trainingType_id: null,
    type_id: null,
    aimag_id: null,
    district_id: null,
    Khoroo_id: null,
    toilet_id: null,
    bedroom_id: null,
    dog_id: null,
    puss_id: null,
    petOther_id: null,
    petService_ids: [],
    HouseKeeperResponsibility_id: [],
    WishList_id: [],
    seniorCareHelp_id: null,
    careWho_ids: [],
    lessonTopic_ids: [],
    helpOrphan_id: null,
    seniorAge_id: null,
    skill_id: null,
  });

  console.log(state);

  // Define the function to toggle the boolean value
  const toggleBooleanValue = value => {
    setIsBooleanButton(value);
  };

  ///location functions
  const onSelectAimag = value => {
    setStates(prevState => ({
      ...prevState,
      aimag_id: value,
    }));
  };
  const onSelectDistrict = value => {
    setStates(prevState => ({
      ...prevState,
      district_id: value,
    }));
  };
  const onSelectKhoroo = value => {
    setStates(prevState => ({
      ...prevState,
      Khoroo_id: value,
    }));
  };
  // type and when
  const onWhenFunction = value => {
    setStates(prevState => ({
      ...prevState,
      when_id: value,
    }));
  };
  const TrainingType = value => {
    setStates(prevState => ({
      ...prevState,
      trainingType_id: value,
    }));
  };
  const OrpanWhosCare = value => {
    setStates(prevState => ({
      ...prevState,
      helpOrphan_id: value,
    }));
  };
  const OrphanAge = value => {
    setStates(prevState => ({
      ...prevState,
      seniorAge_id: value,
    }));
  };
  const SkillOrphanSenior = value => {
    setStates(prevState => ({
      ...prevState,
      skill_id: value,
    }));
  };
  const SeniorCare = value => {
    setStates(prevState => ({
      ...prevState,
      seniorCareHelp_id: value,
    }));
  };
  const onTypeId = value => {
    setStates(prevState => ({
      ...prevState,
      type_id: value,
    }));
  };
  // custom
  const bedRoom = value => {
    setStates(prevState => ({
      ...prevState,
      bedroom_id: value,
    }));
  };
  const ToiletRoom = value => {
    setStates(prevState => ({
      ...prevState,
      toilet_id: value,
    }));
  };
  const DogCount = value => {
    setStates(prevState => ({
      ...prevState,
      dog_id: value,
    }));
  };
  const PussCount = value => {
    setStates(prevState => ({
      ...prevState,
      puss_id: value,
    }));
  };
  const OtherPet = value => {
    setStates(prevState => ({
      ...prevState,
      petOther_id: value,
    }));
  };
  const PetService = value => {
    setStates(prevState => ({
      ...prevState,
      petService_ids: value,
    }));
  };
  const Responsibilty = value => {
    setStates(prevState => ({
      ...prevState,
      HouseKeeperResponsibility_id: value,
    }));
  };
  const WhosCare = value => {
    setStates(prevState => ({
      ...prevState,
      careWho_ids: value,
    }));
  };
  const LessonTopic = value => {
    setStates(prevState => ({
      ...prevState,
      lessonTopic_ids: value,
    }));
  };
  const WishList = value => {
    setStates(prevState => ({
      ...prevState,
      WishList_id: value,
    }));
  };

  const onRadioSelect1 = value => {
    if (selectedValue.includes(value)) {
      setSelectedValue(prevSelectedData =>
        prevSelectedData.filter(item => item !== value),
      );
    } else {
      setSelectedValue(prevSelectedData => [...prevSelectedData, value]);
      setStates(prevState => ({
        ...prevState,
        when_id: value,
      }));
    }
  };
  const onLocationSelect = value => {
    console.log(value);

    if (selectedValue.includes(value)) {
      setStates(prevState => ({
        ...prevState,
        location_id: prevState.location_id.filter(item => item !== value),
      }));
    } else {
      console.log('not value here in array');
      setStates(prevState => ({
        ...prevState,
        location_id: [...prevState.location_id, value],
      }));
    }
  };
  // dynamic decrease index check
  const _index = category_id === 9 ? 2 : category_id === 8 ? 3 : 1;
  const labelStepCount =
    category_id === 9
      ? data.length - 1
      : category_id === 8
      ? data.length - 2
      : data.length;

  // Call Service
  useEffect(() => {
    const endpoint = `/get_ref_responsibility_list/${category_id}`;
    fetchData('GET', endpoint, false);
  }, []); // Empty dependency array means it runs once when the component mounts

  // Render label name
  useEffect(() => {
    const categoryToLabelMap = {
      104: seniorLabelName,
      129: seniorOrpanLabelName,
      10: careChildLabelName,
      9: houseKeeperLabelNames,
      8: PetLabelName,
      2: teacherLabelName,
    };

    const selectedLabelNames = categoryToLabelMap[category_id] || [];
    setLabelNames(selectedLabelNames);
  }, [category_id]);

  // dynamic selected function
  const dynamicScreen = () => {
    const categoryToRenderFunction = {
      104: renderSeniorCare,
      129: renderOrpan,
      10: renderChildCare,
      9: renderHouseKeeper,
      8: renderPet,
      2: renderTeacher,
    };
    // Default to renderPet if category_id doesn't match any key
    const renderFunction = categoryToRenderFunction[category_id] || [];

    return renderFunction(currentPosition);
  };
  // Render components by ID
  const renderChildCare = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChooseType
        data={data[2]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onTypeId={onTypeId}
      />,
    ];

    return components[step] || null;
  };
  const renderOrpan = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChooseInterest
        data={data[2]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        PetService={WhosCare}
      />,
      <ChooseWhen
        data={data[3]}
        onRadioData={onRadioSelect1}
        onWhenFunction={OrpanWhosCare}
        isbuttonBoolean={toggleBooleanValue}
      />,
      <ChooseWhen
        data={data[4]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={OrphanAge}
      />,
      <ChooseWhen
        data={data[5]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={SkillOrphanSenior}
      />,
      // <ChooseInterest
      //   data={data[5]}
      //   onRadioData={onRadioSelect1}
      //   isbuttonBoolean={toggleBooleanValue}
      //   onWhenFunction={OrphanAge}
      // />,
    ];

    return components[step] || null;
  };
  const renderSeniorCare = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChooseWhen
        data={data[2]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={SeniorCare}
      />,
      // <ChooseInterest data={data[3]} />,
    ];

    return components[step] || null;
  };
  const renderPet = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChoosePet
        data={data[2]}
        secondData={data[3]}
        thirdData={data[4]}
        DogCount={DogCount}
        PussCount={PussCount}
        OtherPet={OtherPet}
        isbuttonBoolean={toggleBooleanValue}
      />,
      <ChooseWhen
        data={data[5]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        PetService={PetService}
      />,
    ];

    return components[step] || null;
  };

  const renderHouseKeeper = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChooseCount
        data={data[2]}
        secondData={data[3]}
        isbuttonBoolean={toggleBooleanValue}
        bedRoom={bedRoom}
        ToiletRoom={ToiletRoom}
      />,
      <ChooseWhen
        data={data[4]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        PetService={Responsibilty}
      />,
      <ChooseWhen
        data={data[5]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        PetService={WishList}
      />,
    ];

    return components[step] || null;
  };
  const renderTeacher = step => {
    const components = [
      <ChooseLocation
        data={data[0]}
        onRadioData={onLocationSelect}
        isbuttonBoolean={toggleBooleanValue}
        onSelectAimag={onSelectAimag}
        onSelectDistrict={onSelectDistrict}
        onSelectKhoroo={onSelectKhoroo}
      />,
      <ChooseWhen
        data={data[1]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={onWhenFunction}
      />,
      <ChooseWhen
        data={data[2]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        PetService={LessonTopic}
      />,
      <ChooseWhen
        data={data[3]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
        onWhenFunction={TrainingType}
      />,
      <ChooseWhen
        data={data[4]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
      />,
      <ChooseWhen
        data={data[5]}
        ratio={false}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
      />,
      <ChooseWhen
        data={data[6]}
        onRadioData={onRadioSelect1}
        isbuttonBoolean={toggleBooleanValue}
      />,
    ];

    return components[step] || null;
  };

  // handle next button function
  const handleNext = async () => {
    if (currentPosition < data?.length - _index) {
      if (isBooleanButton === true) {
        setCurrentPosition(currentPosition + 1);
        setIsBooleanButton(false);
        alertBoolean.setFalse();
      } else {
        console.log('talbaraa nbuglun');
        alertBoolean.setTrue();
      }
    } else if (isBooleanButton !== false) {
      navigation.navigate(StackNav.RegisterStack, {
        id: category_id,
        responsibilty_ids: state,
      });
    } else {
      console.log('222talbaraa nbuglun');
      alertBoolean.setTrue();
    }
  };

  // handle Back button function
  const handleBack = () => {
    if (currentPosition > 0) {
      setCurrentPosition(currentPosition - 1);
    }
  };

  // Rendering screen items && components
  return (
    <CSafeAreaView>
      <CHeader title={headerTitle} />
      <View style={localStyles.root}>
        {isLoading === true ? (
          <View style={styles.flexCenter}>
            <ActivityIndicator
              size="large"
              color={commonColor.carelyLogoColor}
            />
          </View>
        ) : (
          <View>
            <StepIndicator
              customStyles={customStylesStep}
              currentPosition={currentPosition}
              stepCount={labelStepCount}
              labels={labelNames}
            />
            {dynamicScreen()}
          </View>
        )}
      </View>
      {alertBoolean.value === true ? (
        <View
          style={[
            localStyles.renderText,
            {backgroundColor: commonColor.primaryTransparent},
          ]}>
          <AntDesign
            style={styles.m10}
            color={commonColor.grayScale5}
            name="questioncircle"
            size={moderateScale(25)}
          />
          <CText type={'m15'} align={'center'}>
            {'Талбар сонгоно уу... '}
          </CText>
        </View>
      ) : null}
      <View style={[localStyles.bottomButtonContainer]}>
        <CButton
          title={'Буцах'}
          type={'m16'}
          containerStyle={[localStyles.buttns]}
          onPress={handleBack}
        />
        <CButton
          title={
            currentPosition === data?.length - _index
              ? strings.update
              : strings.continue
          }
          type={'m16'}
          containerStyle={[localStyles.buttns]}
          onPress={handleNext}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
    ...styles.pb50,
  },
  buttns: {
    width: wp(43),
  },
  bottomButtonContainer: {
    ...styles.ph10,
    ...styles.pt10,
    ...styles.rowSpaceBetween,
  },
  renderText: {
    height: 100,
    ...styles.mt10,
    // ...styles.flexCenter,
    borderRadius: moderateScale(15),
    ...styles.center,
    ...styles.m15,
  },
});
