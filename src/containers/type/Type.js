import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// redux and custom dialog
import useBoolean from '../../contexts/useBoolean';
import useService from '../../contexts/useService';
import {commonColor, styles} from '../../themes';
import {StackNav} from '../../navigation/NavigationKeys';

// Custom Imports
import {deviceWidth, moderateScale} from '../../common/constants';
import CSafeAreaView from '../../components/common/CSafeAreaView';
import CText from '../../components/common/CText';
import TypeModal from './modal/TypeModal';
import images from '../../assets/images';

export default function Type({navigation}) {
  const colors = useSelector(state => state.theme.theme);
  const handleModal = useBoolean(false);
  const {data, fetchData, error} = useService(); // call service custom
  console.log(data);
  const [select, setSelect] = useState([]);
  const houseKeeperId = 104;
  const seniorCare = 129;
  const filteredData = select[1]?.ChildRef.filter(
    item => item.id === houseKeeperId || item.id === seniorCare,
  );
  // onpress modal
  const onPressModalClose = () => handleModal.setFalse();

  // select items
  const onSelectItems = value => {
    if (select.includes(value)) {
      setSelect(select.filter(item => item !== value));
    } else {
      setSelect([setSelect, value]);
      handleModal.setTrue();
    }
  };
  // onpress modal navigation
  const onPress = () => {
    handleModal.setFalse();
    navigation.navigate(StackNav.Location, {
      category_id: select[1]?.id === 7 ? filteredData[0]?.id : select[1]?.id,
      headerTitle: select[1]?.name,
    });
  };
  const onPressWorkPlace = () => {
    handleModal.setFalse();
    navigation.navigate(StackNav.Location, {
      category_id: select[1]?.id === 7 ? filteredData[1]?.id : select[1]?.id,
    });
  };

  // call service category use
  useEffect(() => {
    fetchData('POSTBASE', '/get_ref_list');
  }, []);

  // render maping items
  const renderChildAge = ({item, index}) => {
    const {name, mobile_icon} = item;
    return (
      <TouchableOpacity
        key={index}
        onPress={() => onSelectItems(item)}
        style={[localStyles.categoryRoot]}>
        <View
          style={[
            localStyles.iconContainer,
            {
              backgroundColor: colors.dark ? colors.dark2 : colors.white,
            },
            {
              backgroundColor: select.includes(item)
                ? colors.dark
                  ? colors.carelyLogoColor
                  : colors.carelyLogoColor
                : commonColor.carelyLight,
            },
          ]}>
          <MaterialCommunityIcons
            name={mobile_icon}
            size={moderateScale(50)}
            color={
              select.includes(item) ? 'white' : commonColor.carelyLogoColor
            }
          />
        </View>
        <CText
          type="b13"
          numberOfLines={3}
          align={'center'}
          color={
            select.includes(item)
              ? commonColor.carelyLogoColor
              : commonColor.grayScale5
          }
          style={[styles.mt10]}>
          {name}
        </CText>
      </TouchableOpacity>
    );
  };

  // render main  ----------------------------------------
  return (
    <CSafeAreaView>
      {/* <CHeader isHideBack /> */}
      <Image
        source={colors.dark ? images.carelyLogo : images.carelyLogo}
        style={localStyles.logoStyle}
      />
      <View style={localStyles.root}>
        <FlatList
          data={data?.data}
          renderItem={renderChildAge}
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          ListHeaderComponent={
            <View>
              <CText type={'b18'} align={'left'} style={styles.mb25}>
                {'Та ямар төрлийн үйлчилгээ хайж байна? '}
              </CText>
            </View>
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
      <TypeModal
        btnText1={select[1]?.id === 7 ? filteredData[0]?.name : select[1]?.name}
        btnText2={
          select[1]?.id === 7 ? filteredData[1]?.name : 'Ажлын байр хайх '
        }
        btnText3={select[1]?.id === 7 ? 'Бусад ' : false}
        visible={handleModal.value}
        onPressModalClose={onPressModalClose}
        onPressBtn2={onPressWorkPlace}
        onPressBtn1={onPress}
        subTitle={`Та "${select[1]?.name}" сонирхож буй үйлчилгээг сонгоно уу...`}
        itemImage={select[1]?.image}
      />
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mh5,
    ...styles.mb25,
    width: deviceWidth / 2 - moderateScale(30),
  },
  iconContainer: {
    width: deviceWidth / 2 - moderateScale(50),
    height: deviceWidth / 2 - moderateScale(50),
    borderRadius: moderateScale(45),
    ...styles.center,
  },
  logoStyle: {
    width: moderateScale(150),
    height: moderateScale(70),
    resizeMode: 'contain',
    ...styles.selfCenter,
  },
});
