import {FlatList, StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';

// redux and custom dialog
import useService from '../../contexts/useService';
import {styles} from '../../themes';
import {StackNav} from '../../navigation/NavigationKeys';

// Custom Imports
import {deviceWidth, moderateScale} from '../../common/constants';
import CSafeAreaView from '../../components/common/CSafeAreaView';
import CHeader from '../../components/common/CHeader';
import CText from '../../components/common/CText';
import PaymentSelect from '../../components/cartComponent/PaymentSelect';
import CButton from '../../components/common/CButton';
import strings from '../../i18n/strings';

export default function When({navigation, route}) {
  const {data, fetchData} = useService(); // call service custom
  const [isSelected, setIsSelected] = useState(null);

  // onpress bottom button
  const onPressAdd = () => {
    navigation.navigate(StackNav.Interest, {
      index2: data[2],
    });
  };

  // toggle selection items
  const onHandleResponsibility = value => {
    // Clear the array and add the new selected item
    setIsSelected(value === isSelected ? null : value);
  };

  // call service category use
  useEffect(() => {
    const endpoint = '/get_ref_responsibility_list/129';
    fetchData('GET', endpoint, true);
  }, []); // Empty dependency array means it runs once when the component mounts

  // render maping items
  const renderPaymentItem = ({item, index}) => {
    return (
      <PaymentSelect
        item={item}
        isSelected={isSelected === item}
        onPressItem={() => onHandleResponsibility(item)}
      />
    );
  };

  // render main  ----------------------------------------
  return (
    <CSafeAreaView>
      <CHeader />
      <View style={localStyles.root}>
        <FlatList
          data={data[1]?.Responsibility?.ResponsibilityOption}
          renderItem={renderPaymentItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={
            <View>
              <CText type={'m17'} align={'left'} style={styles.mb10}>
                {'Алхам 1-3'}
              </CText>
              <CText type={'m18'} align={'left'} style={styles.mb25}>
                {data[1]?.Responsibility?.responsibility_question}
              </CText>
            </View>
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={styles.ph20}>
        <CButton
          title={strings.apply}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mh5,
    ...styles.mb25,
    width: deviceWidth / 2 - moderateScale(30),
  },
  iconContainer: {
    width: deviceWidth / 2 - moderateScale(50),
    height: deviceWidth / 2 - moderateScale(50),
    borderRadius: moderateScale(45),
    ...styles.center,
  },
});
