import React, {useReducer} from 'react';
import AuthReducer from '../contexts/AuthReducer';
import axios from 'axios';
import variables from './variables';

const instance = axios.create({
  baseURL: variables.APP_BASE_URL,
  timeout: 30000,
  headers: {'Content-Type': 'application/json '},
});
const instanceLogin = axios.create({
  baseURL: variables.LOGIN,
  timeout: 30000,
  headers: {'Content-Type': 'application/json '},
});
const instanceGeneral = axios.create({
  baseURL: variables.GENERAL,
  timeout: 30000,
  headers: {'Content-Type': 'application/json '},
});
const instanceProduct = axios.create({
  baseURL: variables.PRODUCT,
  timeout: 30000,
  headers: {'Content-Type': 'application/json '},
});

export const Api = () => {
  const initialState = {
    userToken: null,
    registerUser: null,
    loggedUser: null,
  };
  const [state, dispatch] = useReducer(AuthReducer, initialState);
  console.log(state);
  const handlers = React.useMemo(
    () => ({
      //хэрэглэгчийн нэвтрэх
      signIn: async data => {
        console.log(data);
        let payload = {
          token: data.token,
          data: data.user,
        };
        //successText('Тавтай морилно уу 😊');
        dispatch({type: 'SIGN_IN', payload});
      },
      setSensetiveToken: async data => {
        let payload = {
          token: data,
        };
        dispatch({type: 'SET_INITIAL_TOKEN', payload});
      },

      //хэрэглэгч гарах
      signOut: () => dispatch({type: 'SIGN_OUT'}),

      //бүртгэл хийгдэж байгаа бүртгэлийн мэдээллийг хадгалах
      registerUserUpdateData: (type, data) => {
        let obj = {
          type,
          data,
        };
        dispatch({type: 'REGISTER_USER_UPDATE_DATA', payload: obj});
      },

      //бүртгэл хийгдэж байгаа хэрэглэгчийн мэдээллийг шинэчлэх
      registerUpdateData: data => {
        dispatch({type: 'REGISTER_UPDATE_DATA', payload: data});
      },

      //login хийгдэж байгаа хэрэглэгчийн бүртгэлийн мэдээллийг хадгалах
      loggedInUserData: data => {
        dispatch({type: 'LOGGED_USER_DATA', payload: data});
      },

      //нэвтэрсэн байгаа хэрэглэгчийн мэдээллийг dynamic аар солих
      loggedInUserDataUpdate: (type, data) => {
        let obj = {
          type,
          data,
        };
        dispatch({type: 'LOGGED_USER_UPDATE_DATA', payload: obj});
      },

      signUp: () => {
        dispatch({type: 'SIGN_IN'});
      },

      //LOGIN action
      LOGIN: async (url, data) => {
        var res = await instanceLogin.post(url, data, {
          headers: {
            Authorization: `Basic ZGFpOjIwMjM=`,
            'Content-Type': 'application/json',
          },
        });
        // dispatch({type: 'LOGGED_USER_UPDATE_DATA', payload: {data:res,token:res?.data?.token}});
        return res;
      },

      GET: async (url, isToken = false) => {
        return instanceProduct.get(
          url,
          isToken
            ? {
                headers: {
                  Authorization: `Bearer ${state.userToken}`,
                },
              }
            : '',
        );
      },

      GETPROD: async (url, isToken = false) => {
        return instanceGeneral.get(
          url,
          isToken
            ? {
                headers: {
                  Authorization: `Bearer ${state.userToken}`,
                },
              }
            : '',
        );
      },
      GETUSER: async (url, isToken = false) => {
        return instance.get(
          url,
          isToken
            ? {
                headers: {
                  Authorization: `Bearer ${state.userToken}`,
                },
              }
            : '',
        );
      },

      POST: async (
        url,
        isToken = false,
        data,
        contentType = 'application/json',
      ) => {
        console.log(isToken);
        console.log(state);
        return await instance.post(
          url,
          data,
          isToken === true
            ? {
                headers: {
                  Authorization: `Bearer ${state?.userToken}`,
                  'Content-Type': contentType,
                },
              }
            : {
                headers: {
                  'Content-Type': contentType,
                },
              },
        );
      },
      POSTBASE: async url => {
        return await instanceGeneral.post(url, {
          headers: {
            Authorization: `Basic ZGFpOjIwMjM=`,
            'Content-Type': 'application/json',
          },
        });
      },
      POSTGENERAL: async (
        url,
        isToken = false,
        data,
        contentType = 'application/json',
      ) => {
        return await instanceGeneral.post(
          url,
          data,
          isToken === true
            ? {
                headers: {
                  Authorization: `Bearer ${state?.userToken}`,
                  'Content-Type': contentType,
                },
              }
            : {
                headers: {
                  'Content-Type': contentType,
                },
              },
        );
      },
      POSTPRODUCT: async (
        url,
        isToken = false,
        data,
        contentType = 'application/json',
      ) => {
        return await instanceProduct.post(
          url,
          data,
          isToken
            ? {
                headers: {
                  Authorization: `Bearer ${state?.payload?.token}`,
                  'Content-Type': contentType,
                },
              }
            : {
                headers: {
                  'Content-Type': contentType,
                },
              },
        );
      },
      PUT: async (url, isToken = false, data) => {
        return await instance.put(
          url,
          data,
          isToken
            ? {
                headers: {
                  Authorization: `Bearer ${state.userToken}`,
                },
              }
            : '',
        );
      },
    }),
    [state],
  );
  return {handlers, state};
};
