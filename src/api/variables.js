// export default {
//   //carely testing api url
//   APP_BASE_URL: 'http://192.168.50.2:3090/user',
//   LOGIN: 'http://192.168.50.2:3009/oauth',
//   GENERAL: 'http://192.168.50.2:3093/general',
//   PRODUCT: 'http://192.168.50.2:8010/shop',
//   IMAGEURL: 'http://192.168.50.2:3090',
//   GENERAL_IMAGE_URL: 'http://192.168.50.2:3093',
// };
export default {
  //carely production api url
  APP_BASE_URL: 'http://43.231.114.35:3090/user',
  LOGIN: 'http://43.231.114.35:3009/oauth',
  GENERAL: 'http://43.231.114.35:3093/general',
  PRODUCT: 'http://43.231.114.35:8010/shop',
  IMAGEURL: 'http://43.231.114.35:3090/carely-user-images',
  GENERAL_IMAGE_URL: 'http://43.231.114.35:3093/carely-general-images',
};
