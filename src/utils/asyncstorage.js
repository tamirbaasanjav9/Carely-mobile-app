import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  ACCESS_TOKEN,
  ON_BOARDING,
  THEME,
  SELECT_TYPE,
} from '../common/constants';

// const setUserDetail = async value => {
//   const stringifyData = JSON.stringify(value);
//   await AsyncStorage.setItem(USER_DETAIL, stringifyData);
//   return true;
// };
// const getUserDetail = async () => {
//   const getUserData = await AsyncStorage.getItem(USER_DETAIL);
//   if (!!getUserData) {
//     return JSON.parse(getUserData);
//   } else {
//     return false;
//   }
// };

const removeUserDetail = async key => {
  await AsyncStorage.removeItem(key);
};

const initialStorageValueGet = async () => {
  let asyncData = await AsyncStorage.multiGet([
    THEME,
    ON_BOARDING,
    ACCESS_TOKEN,
    SELECT_TYPE,
  ]);
  console.log('asyncData ', asyncData);
  const themeColor = JSON.parse(asyncData[0][1]);
  const onBoardingValue = JSON.parse(asyncData[1][1]);
  const acessTokenValue = JSON.parse(asyncData[2][1]);
  return {themeColor, onBoardingValue, acessTokenValue};
};

// setOnBoarding;

const setOnBoarding = async value => {
  console.log(value, 'onboarding checking');
  const stringifyData = JSON.stringify(value);
  await AsyncStorage.setItem(ON_BOARDING, stringifyData);
  return;
};
const setSelectType = async value => {
  console.log(value, 'selectType checking');
  const stringifyData = JSON.stringify(value);
  await AsyncStorage.setItem(SELECT_TYPE, stringifyData);
  return;
};
const addToRecentlyViewed = async (itemId, itemName) => {
  try {
    // Get existing recently viewed items
    const existingItems = await AsyncStorage.getItem('recentlyViewed');

    let recentlyViewed = [];

    if (existingItems) {
      // Parse existing items if they exist
      recentlyViewed = JSON.parse(existingItems);
    }

    // Add the new item to the recently viewed list
    recentlyViewed.unshift({id: itemId, name: itemName});

    // Limit the list to, let's say, the last 5 items
    recentlyViewed = recentlyViewed.slice(0, 5);

    // Save the updated recently viewed list to AsyncStorage
    await AsyncStorage.setItem(
      'recentlyViewed',
      JSON.stringify(recentlyViewed),
    );
  } catch (error) {
    console.error('Error saving recently viewed item:', error);
  }
};

export const saveToken = async value => {
  try {
    await AsyncStorage.setItem('token', value);
    return true;
  } catch (err) {
    return false;
  }
};

export const getSavedToken = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    return token;
  } catch (err) {
    return false;
  }
};

export const deleteToken = async () => {
  try {
    await AsyncStorage.removeItem('token');
    return true;
  } catch (err) {
    return false;
  }
};

export const saveId = async value => {
  try {
    await AsyncStorage.setItem('value0', value);
  } catch (err) {
    return false;
  }
};

export const getId = async () => {
  try {
    return await AsyncStorage.getItem('value0');
  } catch (err) {
    return false;
  }
};

export const deleteId = async () => {
  try {
    await AsyncStorage.removeItem('value0');
  } catch (err) {
    return false;
  }
};
export const saveUserId = id => {
  AsyncStorage.setItem('userId', id)
    .then(() => {
      console.log('User ID saved successfully');
    })
    .catch(error => {
      console.log(error);
    });
};
export const getUserId = async () => {
  try {
    const userId = await AsyncStorage.getItem('userId');
    console.log(`Stored user ID: ${userId}`);
    return userId;
  } catch (error) {
    console.log(error);
  }
};

export {
  // setUserDetail, getUserDetail,
  initialStorageValueGet,
  setOnBoarding,
  removeUserDetail,
  setSelectType,
  addToRecentlyViewed,
};
