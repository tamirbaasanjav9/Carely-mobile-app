import {View, StyleSheet} from 'react-native';
import React, {useEffect} from 'react';
import {styles} from '../themes';
import LottieView from 'lottie-react-native';
import {Animated_loader} from '../assets/lottie';
const Loader = ({loading, loop = true}) => {
  let animation;
  useEffect(() => {
    // Start the animation when the component mounts
    animation?.play();
  }, []);
  return (
    <View style={[styles.flexCenter, localStyles.container]}>
      <LottieView
        // ref={ref => {
        //   animation = ref;
        // }}
        source={require('../assets/lottie/animatedLoader.json')}
        loop={loop}
      />
    </View>
  );
};

export default Loader;

const localStyles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
  },
});
