// Library import
import React, {useState} from 'react';
import {Keyboard, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {useSelector} from 'react-redux';
import ActionSheet from 'react-native-actions-sheet';
// Local import
import {moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import CText from '../common/CText';
import strings from '../../i18n/strings';
import CDivider from '../common/CDivider';
import {useEffect} from 'react';
import {memo} from 'react';

export default function SortModal(props) {
  const {SheetRef, item, chipsData} = props;
  const colors = useSelector(state => state.theme.theme);
  const [selectedChips, setSelectedChips] = useState([strings.all]);
  const [extraData, setExtraData] = useState(true);

  useEffect(() => {
    setExtraData(!extraData);
  }, [selectedChips, colors]);

  const onPressChips = value => {
    if (selectedChips.includes(value)) {
      setSelectedChips(selectedChips.filter(item => item !== value));
    } else {
      setSelectedChips([value]);
    }
  };

  const SortingDatas = memo(() => {
    return chipsData.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          onPress={() => onPressChips(item)}
          style={[
            localStyles.contactUsContainer,
            {borderColor: colors.dark ? colors.dark3 : colors.black},
            {
              backgroundColor: selectedChips.includes(item)
                ? colors.dark
                  ? colors.dark3
                  : colors.black
                : colors.inputBg,
            },
          ]}>
          <CText
            style={styles.ph15}
            color={
              selectedChips.includes(item)
                ? colors.dark
                  ? colors.white
                  : colors.white
                : colors.dark
                ? colors.white
                : colors.black
            }
            type={'b14'}>
            {item?.title}
          </CText>
        </TouchableOpacity>
      );
    });
  });
  return (
    <ActionSheet
      ref={SheetRef}
      gestureEnabled={true}
      indicatorStyle={{
        backgroundColor: colors.dark ? colors.dark3 : colors.grayScale3,
        width: moderateScale(60),
        ...styles.mv10,
      }}
      containerStyle={[
        localStyles.actionSheetContainer,
        {backgroundColor: colors.backgroundColor},
      ]}>
      <TouchableOpacity
        style={{
          ...styles.mb30,
        }}
        activeOpacity={1}
        onPress={() => Keyboard.dismiss()}>
        <CText type={'B17'} style={localStyles.headerText} align={'center'}>
          {strings.sorting}
        </CText>
        <CDivider style={styles.mv5} />
        <SortingDatas />
      </TouchableOpacity>
    </ActionSheet>
  );
}

const localStyles = StyleSheet.create({
  actionSheetContainer: {
    ...styles.ph20,
    ...styles.pb30,
  },
  headerText: {
    ...styles.mt5,
    ...styles.mb10,
  },
  contactUsContainer: {
    ...styles.mt15,
    ...styles.pv15,
    borderRadius: moderateScale(15),
    ...styles.flexRow,
    ...styles.contentCenter,
  },
});
