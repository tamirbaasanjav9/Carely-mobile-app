// Library import
import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View, Switch} from 'react-native';
import {useSelector} from 'react-redux';
import ActionSheet from 'react-native-actions-sheet';

// Local import
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import CText from '../common/CText';
import strings from '../../i18n/strings';
import CButton from '../common/CButton';
import CDivider from '../common/CDivider';
import HandleCategoryModal from '../detailProduct/HandleCategoryModal';
import useService from '../../contexts/useService';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import useBoolean from '../../contexts/useBoolean';
import {FlatList} from 'react-native';

const SortAndFilter = props => {
  const {SheetRef, onPressCancel, onPressDone, filter_id} = props;
  const [subjectId, setSubjectId] = useState([]);
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData} = useService(); // call service custom
  const {data: filterListData, fetchData: fetchFilterData} = useService(); // call service custom
  const {
    data: experience,
    fetchData: fetchExperience,
    error: experienceError,
    isLoading: experienceLoading,
  } = useService(); // call service custom
  useEffect(() => {
    fetchData('POST', '/get_ref_list_fordropdown', false, {
      parent_id: 10,
    });
  }, []); // Empty dependency array means it runs once when the component mounts
  useEffect(() => {
    fetchFilterData('GET', `/get_filter_list_fordropdown/${filter_id}`, false);
    fetchExperience('GET', `/get_const_config_list_fordropdown/3`, false);
  }, [filter_id]); // Empty dependency array means it runs once when the component mounts

  const subjectFuntion = e => {
    if (subjectId.includes(e)) {
      setSubjectId(
        subjectId.filter(item => item?.subject_id !== e?.subject_id),
      );
    } else {
      setSubjectId([...subjectId, e]);
    }
  };
  const Done = () => {
    onPressDone(subjectId);
    setSubjectId([]);
  };
  const customMarker = event => {
    return (
      <View style={localStyles.markerContainer}>
        <View
          style={[
            localStyles.sliderLength,
            {
              backgroundColor: colors.dark ? colors.dark3 : colors.imageBg,
              borderColor: colors.dark ? colors.white : colors.textColor,
            },
          ]}
        />
        <CText color={colors.darkGray} type={'m12'} style={[styles.mt5]}>
          {'₮' + event.currentValue}
        </CText>
      </View>
    );
  };
  const renderData = ({item, index}) => {
    return (
      <HandleCategoryModal
        mappedData={item}
        index={index}
        onArrivedArray={subjectFuntion}
      />
    );
  };

  return (
    <ActionSheet
      ref={SheetRef}
      gestureEnabled={true}
      indicatorStyle={{
        backgroundColor: colors.dark ? colors.dark3 : colors.grayScale3,
        ...styles.actionSheetIndicator,
      }}
      containerStyle={[
        localStyles.actionSheetContainer,
        {backgroundColor: colors.backgroundColor},
      ]}>
      <View style={localStyles.bottomContainer}>
        <CText align={'center'} type={'b15'} style={styles.pv5}>
          {strings.filter}
        </CText>
        <CDivider style={styles.mv10} />
        <FlatList
          data={filterListData}
          renderItem={renderData}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={
            <View style={styles.mb10}>
              <CText type={'b14'} style={styles.mb10}>
                {strings.priceRange}
              </CText>
              <MultiSlider
                sliderLength={deviceWidth * moderateScale(2)}
                values={[10000, 20000]}
                min={5000}
                max={50000}
                onValuesChange={values => console.log(values)}
                step={1000}
                markerOffsetY={20}
                selectedStyle={{backgroundColor: colors.textColor}}
                trackStyle={[
                  localStyles.sliderContainer,
                  {
                    backgroundColor: colors.dark
                      ? colors.dark3
                      : colors.imageBg,
                  },
                ]}
                minMarkerOverlapDistance={50}
                customMarker={customMarker}
              />
            </View>
          }
        />

        <CDivider style={styles.mb20} />
        <View style={localStyles.btnContainer}>
          <CButton
            title={strings.reset}
            type={'S13'}
            containerStyle={localStyles.skipBtnContainer}
            bgColor={colors.dark3}
            onPress={onPressCancel}
          />
          <CButton
            title={'Хайх'}
            type={'s14'}
            containerStyle={localStyles.skipBtnContainer}
            onPress={Done}
          />
        </View>
      </View>
    </ActionSheet>
  );
};

const localStyles = StyleSheet.create({
  actionSheetContainer: {
    ...styles.ph15,
  },
  textStyles: {
    ...styles.mb15,
  },
  categoryContainer: {
    ...styles.mb15,
    ...styles.pt20,
  },
  btnContainer: {
    ...styles.pb30,
    ...styles.rowSpaceAround,
  },
  skipBtnContainer: {
    width: '45%',
  },
  bottomContainer: {
    ...styles.pv10,
  },
  sliderContainer: {
    height: moderateScale(4),
    borderRadius: moderateScale(6),
  },
  sliderLength: {
    height: moderateScale(20),
    width: moderateScale(20),
    borderRadius: moderateScale(12),
    borderWidth: moderateScale(4),
  },
  markerContainer: {
    height: getHeight(60),
    ...styles.center,
    ...styles.justifyStart,
  },
  rightContainer: {
    ...styles.flex,
    ...styles.rowSpaceBetween,
  },
});

export default SortAndFilter;
