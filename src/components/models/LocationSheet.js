// Library import
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';
import ActionSheet from 'react-native-actions-sheet';

// Local import
import {commonColor, styles} from '../../themes';
import CText from '../common/CText';
import {moderateScale} from '../../common/constants';
import {Dropdown} from 'react-native-element-dropdown';
import {useEffect} from 'react';
import useBoolean from '../../contexts/useBoolean';
import {useState} from 'react';
import useService from '../../contexts/useService';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

const LocationSheet = ({SheetRef, onPressConfirm, onDataReceived}) => {
  const colors = useSelector(state => state.theme.theme);
  const [isFocus, setIsFocus] = useState(false);

  // render selected value
  const [value, setValue] = useState(null);
  const [districtValue, setDistrictValue] = useState(null);
  const [khorooValue, setKhorooValue] = useState(null);
  const districtState = useBoolean();
  const districtKhoroo = useBoolean();
  const loadingState = useBoolean();
  const confirmBoolean = useBoolean();
  const okBoolean = useBoolean();

  // call service
  const {data: aimagData, fetchData: fetchAimag, isLoading} = useService();
  const {
    data: districtData,
    fetchData: fetchDistrict,
    isLoading: districtLoading,
  } = useService();
  const {
    data: khorooData,
    fetchData: fetchKhoroo,
    isLoading: khorooLoading,
  } = useService();

  useEffect(() => {
    fetchAimag('POST', '/get_aimag_list', false, {});
  }, []);
  const handleDistrict = async id => {
    loadingState.setTrue();
    await fetchDistrict('POST', '/get_district_list', false, {
      filter: [
        {
          field_name: 'aimag_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  const lastSelect = item => {
    setKhorooValue(item);
    setIsFocus(false);
    confirmBoolean.setTrue();
    okBoolean.setTrue();
  };
  const confrimButton = () => {
    okBoolean.setFalse();
    onPressConfirm();
    onDataReceived({
      khoroo_id: khorooValue,
      district_id: districtValue,
      aimag_id: value,
    });
  };
  // handle on Khoroo item call service
  const handleKhoroo = async id => {
    loadingState.setTrue();
    await fetchKhoroo('POST', '/get_khoroo_list', false, {
      filter: [
        {
          field_name: 'district_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  // render data on dropDown
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.aimag_name}</CText>
        {item.id === value && (
          <AntDesign
            // style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderDistrict = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.district_name}</CText>
        {item.id === districtValue && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderKhoroo = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.khoroo_name}</CText>
        {item.id === khorooValue && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  return (
    <ActionSheet
      ref={SheetRef}
      gestureEnabled={true}
      indicatorStyle={{
        backgroundColor: colors.dark ? colors.dark3 : colors.grayScale3,
        ...styles.actionSheetIndicator,
      }}
      containerStyle={[
        localStyles.actionSheetContainer,
        {backgroundColor: colors.backgroundColor},
      ]}>
      <CText type={'M16'} align={'center'} style={styles.mt15}>
        Байршлаар хайх
      </CText>
      <View style={styles.pv30}>
        {isLoading !== true ? (
          <Dropdown
            dropdownPosition="bottom"
            style={[
              localStyles.dropdown,
              isFocus && {borderColor: commonColor.carelyLogoColor},
              {backgroundColor: colors.input2},
            ]}
            placeholderStyle={[
              localStyles.placeholderStyle,
              {
                color: colors.textColor,
              },
            ]}
            selectedTextStyle={[
              localStyles.selectedTextStyle,
              {
                color: colors.textColor,
              },
            ]}
            inputSearchStyle={localStyles.inputSearchStyle}
            iconStyle={localStyles.iconStyle}
            data={aimagData?.data}
            search
            containerStyle={[
              localStyles.dropdownstyle,
              {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
            ]}
            maxHeight={300}
            labelField="aimag_name"
            valueField="id"
            placeholder={!isFocus ? 'Аймаг, хот сонгох' : '...'}
            searchPlaceholder="Хайх..."
            activeColor={colors.dark3}
            value={value}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              setValue(item);
              handleDistrict(item?.id);
              setIsFocus(false);
              districtState.setTrue();
              districtKhoroo.setFalse();
            }}
            renderLeftIcon={() => (
              <EvilIcons
                style={{...styles.mr10}}
                color={commonColor.carelyLogoColor}
                name="location"
                size={moderateScale(25)}
              />
            )}
            renderItem={renderItem}
          />
        ) : null}
        {districtState.value === true && districtLoading !== true ? (
          <Dropdown
            style={[
              localStyles.dropdown1,
              isFocus && {borderColor: commonColor.carelyLogoColor},
              {backgroundColor: colors.input2},
            ]}
            placeholderStyle={[
              localStyles.placeholderStyle,
              {
                color: colors.textColor,
              },
            ]}
            selectedTextStyle={[
              localStyles.selectedTextStyle,
              {
                color: colors.textColor,
              },
            ]}
            inputSearchStyle={localStyles.inputSearchStyle}
            iconStyle={localStyles.iconStyle}
            data={districtData?.data}
            search
            containerStyle={[
              localStyles.dropdownstyle,
              {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
            ]}
            maxHeight={300}
            activeColor={colors.dark3}
            labelField="district_name"
            valueField="id"
            placeholder={!isFocus ? 'Сум, дүүрэг сонгох' : '...'}
            searchPlaceholder="Хайх..."
            value={districtValue}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              setDistrictValue(item);
              handleKhoroo(item.id);
              setIsFocus(false);
              districtKhoroo.setTrue();
            }}
            renderLeftIcon={() => (
              <EvilIcons
                style={{...styles.mr10}}
                color={commonColor.carelyLogoColor}
                name="location"
                size={moderateScale(25)}
              />
            )}
            renderItem={renderDistrict}
          />
        ) : null}
        {districtKhoroo.value === true && khorooLoading !== true ? (
          <Dropdown
            style={[
              localStyles.dropdownLast,
              isFocus && {borderColor: commonColor.carelyLogoColor},
              {backgroundColor: colors.input2},
            ]}
            placeholderStyle={[
              localStyles.placeholderStyle,
              {
                color: colors.textColor,
              },
            ]}
            selectedTextStyle={[
              localStyles.selectedTextStyle,
              {
                color: colors.textColor,
              },
            ]}
            inputSearchStyle={localStyles.inputSearchStyle}
            iconStyle={localStyles.iconStyle}
            data={khorooData?.data}
            search
            activeColor={colors.dark3}
            maxHeight={300}
            containerStyle={[
              localStyles.dropdownstyle,
              {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
            ]}
            labelField="khoroo_name"
            valueField="id"
            placeholder={!isFocus ? 'Баг, Хороо сонгох' : '...'}
            searchPlaceholder="Хайх..."
            value={khorooValue}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              lastSelect(item);
            }}
            renderLeftIcon={() => (
              <EvilIcons
                style={{...styles.mr10}}
                color={commonColor.carelyLogoColor}
                name="location"
                size={moderateScale(25)}
              />
            )}
            renderItem={renderKhoroo}
          />
        ) : null}
      </View>
      <TouchableOpacity
        onPress={confrimButton}
        disabled={!okBoolean.value}
        style={[
          localStyles.confirmButton,
          {
            backgroundColor: !okBoolean.value
              ? colors.inactive
              : colors.carelyLogoColor,
          },
        ]}>
        <CText color={colors.white} type={'m16'}>
          Сонгох
        </CText>
      </TouchableOpacity>
    </ActionSheet>
  );
};

const localStyles = StyleSheet.create({
  actionSheetContainer: {
    ...styles.ph20,
  },
  dropdown: {
    height: moderateScale(50),
    borderRadius: moderateScale(12),
    ...styles.mb25,
    ...styles.ph10,
  },
  dropdown1: {
    height: moderateScale(50),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.mb25,
  },
  confirmed: {
    height: moderateScale(50),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.rowSpaceBetween,
    ...styles.mb25,
  },
  dropdownLast: {
    height: moderateScale(50),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.mb25,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  dropdownstyle: {
    borderRadius: moderateScale(10),
    borderWidth: 1,
    borderColor: commonColor.carelyLogoColor,
  },
  confirmButton: {
    ...styles.center,
    ...styles.mb40,
    ...styles.pv10,
    borderRadius: moderateScale(20),
  },
});

export default LocationSheet;
