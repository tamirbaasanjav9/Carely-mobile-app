// Library import
import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import ActionSheet from 'react-native-actions-sheet';

// Local import
import {commonColor, styles} from '../../themes';
import CText from '../common/CText';
import {moderateScale} from '../../common/constants';
import useService from '../../contexts/useService';
import AntDesign from 'react-native-vector-icons/AntDesign';

const ChildFilterSheet = ({
  SheetRef,
  onPressConfirm,
  onDataReceived,
  ref_id,
}) => {
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData, isLoading} = useService();
  console.log(data);
  const [quantities, setQuantities] = useState([0, 0, 0, 0, 0]);
  const sum = quantities.reduce((acc, current) => acc + current, 0);

  // call service
  useEffect(() => {
    fetchData('GET', `/get_ref_age_group_list/${ref_id}`, false, {});
  }, []);
  const confrimButton = () => {
    onPressConfirm();
    if (sum === 0) {
      onDataReceived(null);
    } else {
      onDataReceived(sum);
    }
  };

  const onPressRemove = index => {
    if (quantities[index] !== 0) {
      setQuantities(prevQuantities => {
        const updatedQuantities = [...prevQuantities];
        updatedQuantities[index] = prevQuantities[index] - 1;
        return updatedQuantities;
      });
    }
  };

  const onPressAdd = (index, item) => {
    console.log(item);

    if (quantities[index] !== 4) {
      setQuantities(prevQuantities => {
        const updatedQuantities = [...prevQuantities];
        updatedQuantities[index] = prevQuantities[index] + 1;

        return updatedQuantities;
      });
    }
  };

  const renderItems = ({item, index}) => {
    const {age_group_name, age_range_max, age_range_min, AgeUnit} =
      item?.AgeGroup;
    return (
      <View style={[localStyles.list]}>
        <View style={styles.rowStart}>
          <CText style={styles.mr20} type={'M16'}>
            {quantities[index]}
          </CText>
          <View>
            <CText type={'M16'}>{age_group_name}</CText>
            <View style={styles.rowStart}>
              <CText>
                {age_range_min}
                {age_range_max !== null ? '-' : null}
              </CText>
              <CText>
                {age_range_max === null ? '+ ' : null}
                {age_range_max}
              </CText>
              <CText> {AgeUnit?.const_value}</CText>
            </View>
          </View>
        </View>
        <View style={[styles.rowSpaceBetween]}>
          <TouchableOpacity onPress={() => onPressRemove(index)}>
            <AntDesign
              name={'minuscircleo'}
              size={moderateScale(24)}
              color={commonColor.carelyLogoColor}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onPressAdd(index, item?.id)}>
            <AntDesign
              name={'pluscircle'}
              size={moderateScale(24)}
              color={commonColor.carelyLogoColor}
              style={styles.pl10}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  return (
    <ActionSheet
      ref={SheetRef}
      gestureEnabled={true}
      indicatorStyle={{
        backgroundColor: colors.dark ? colors.dark3 : colors.grayScale3,
        ...styles.actionSheetIndicator,
      }}
      containerStyle={[
        localStyles.actionSheetContainer,
        {backgroundColor: colors.backgroundColor},
      ]}>
      <CText type={'M16'} align={'center'} style={styles.mt15}>
        Хэнд халамж хэрэгтэй вэ?
      </CText>
      {!isLoading ? (
        <FlatList
          renderItem={renderItems}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          style={styles.pv30}
        />
      ) : (
        <ActivityIndicator
          size="large"
          color={colors.carelyLogoColor}
          style={styles.pv30}
        />
      )}
      <TouchableOpacity
        onPress={confrimButton}
        style={[
          localStyles.confirmButton,
          {
            backgroundColor: colors.carelyLogoColor,
          },
        ]}>
        <CText color={colors.white} type={'m16'}>
          Сонгох
        </CText>
      </TouchableOpacity>
    </ActionSheet>
  );
};

const localStyles = StyleSheet.create({
  actionSheetContainer: {
    ...styles.ph20,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  confirmButton: {
    ...styles.center,
    ...styles.mb40,
    ...styles.pv10,
    borderRadius: moderateScale(20),
  },
  list: {
    ...styles.rowSpaceBetween,
    ...styles.mb5,
    ...styles.p10,
  },
});

export default ChildFilterSheet;
