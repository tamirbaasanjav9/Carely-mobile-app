// Library import
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {useSelector} from 'react-redux';
import ActionSheet from 'react-native-actions-sheet';

// Local import
import {styles} from '../../themes';
import CText from '../common/CText';
import strings from '../../i18n/strings';
import CButton from '../common/CButton';
import CDivider from '../common/CDivider';

const LogOut = props => {
  console.log(props);
  const {SheetRef, onPressCancel, onPressLogOut} = props;
  const colors = useSelector(state => state.theme.theme);

  return (
    <ActionSheet
      ref={SheetRef}
      gestureEnabled={true}
      indicatorStyle={{
        backgroundColor: colors.dark ? colors.dark3 : colors.grayScale3,
        ...styles.actionSheetIndicator,
      }}
      containerStyle={[
        localStyles.actionSheetContainer,
        {backgroundColor: colors.backgroundColor},
      ]}>
      <View style={localStyles.bottomContainer}>
        <CText
          type={'B15'}
          style={styles.mt5}
          color={colors.alertColor}
          align={'center'}>
          {strings.logout}
        </CText>
        <CDivider style={styles.mv20} />
        <CText type={'b15'} align={'center'}>
          {strings.logOutDescription}
        </CText>
        <View style={localStyles.btnContainer}>
          <CButton
            title={strings.cancel}
            type={'S14'}
            containerStyle={localStyles.skipBtnContainer}
            color={colors.dark ? colors.white : colors.white}
            bgColor={colors.dark3}
            onPress={onPressCancel}
          />
          <CButton
            title={strings.yesLogOut}
            color={colors.dark ? colors.white : colors.white}
            type={'S13'}
            containerStyle={localStyles.skipBtnContainer}
            onPress={onPressLogOut}
          />
        </View>
      </View>
    </ActionSheet>
  );
};

const localStyles = StyleSheet.create({
  actionSheetContainer: {
    ...styles.ph20,
  },
  btnContainer: {
    ...styles.pv30,
    ...styles.rowSpaceAround,
  },
  skipBtnContainer: {
    width: '45%',
  },
  bottomContainer: {
    ...styles.pv10,
  },
});

export default LogOut;
