// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {deviceWidth, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import variables from '../../api/variables';
import CalendarShort from '../detailProduct/CalendarShort';

// Vector icons
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function TopCard(route) {
  const {
    Company,
    image,
    Aimag,
    District,
    Class_price,
    experience,
    PaymentInterval,
    class_name,
  } = route;

  const formattedNumber = Class_price.toLocaleString(); // Formats as per user's locale
  const colors = useSelector(state => state.theme.theme);
  return (
    <View
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.white},
      ]}>
      <View style={localStyles.headerColumn}>
        <View style={[styles.pt10]}>
          <Image
            source={{
              uri: `${variables.IMAGEURL}/${image[0]?.image_url}`,
            }}
            style={[
              localStyles.productImageStyle,
              {backgroundColor: colors.dark ? colors.imageBg : colors.white},
            ]}
          />
        </View>
        <View style={localStyles.titleContainer}>
          <View style={[styles.rowSpaceBetween, styles.pb10]}>
            <CText style={{}} numberOfLines={2} type={'b16'}>
              {Company?.name}
            </CText>
            <View style={[styles.rowSpaceBetween]}>
              <Octicons
                name="star-fill"
                size={moderateScale(18)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText type={'b13'} style={{}}>
                4.85
              </CText>
            </View>
          </View>
          <View style={localStyles.subItemStyleTextLocation}>
            <View style={[localStyles.circleContainer]}>
              <Ionicons
                name="ribbon-outline"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'}>{experience} жилийн туршлагатай</CText>
          </View>
          <View style={localStyles.subItemStyleTextLocation}>
            <View style={[localStyles.circleContainer]}>
              <Ionicons
                name="location"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'} style={{...styles.mr5}}>
              {District?.district_name}
            </CText>
            <CText type={'b5'} style={{...styles.mr5}}>
              {'\u2B24'}
            </CText>
            <CText type={'b13'}>{Aimag?.aimag_name}</CText>
          </View>
          <View style={[styles.flexRow, styles.itemsCenter]}>
            <View style={[localStyles.circleContainer]}>
              <FontAwesome5
                name="comment-dollar"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'} style={{...styles.mr5}}>
              {formattedNumber}₮ / {PaymentInterval?.const_value}
            </CText>
          </View>
        </View>
      </View>
      {/* <View style={localStyles.descriptionContainer}>
        <CText numberOfLines={3} type={'b13'} style={{...styles.mb10}}>
          {class_name}
        </CText>
      </View> */}
    </View>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    ...styles.mv15,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
    width: deviceWidth - moderateScale(20),
  },
  productImageStyle: {
    height: moderateScale(60),
    width: moderateScale(60),
    borderRadius: moderateScale(20),
    marginRight: moderateScale(5),
  },
  titleContainer: {
    ...styles.flex,
    ...styles.p10,
  },
  headerColumn: {
    flexDirection: 'row',
    ...styles.flex,
    ...styles.ph10,
    ...styles.pt10,
    justifyContent: 'space-between',
  },
  circleContainer: {
    ...styles.mr10,
  },
  subItemStyleTextLocation: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mb5,
  },
  descriptionContainer: {
    ...styles.ph10,
  },
});
