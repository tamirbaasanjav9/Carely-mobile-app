// Library Imports
import {Image, StyleSheet, View} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {deviceWidth, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import CText from '../common/CText';

export default function HomeInterView(props) {
  const colors = useSelector(state => state.theme.theme);
  const [quantity, setQuantity] = useState(1);

  const onPressRemove = () => {
    if (quantity > 1) {
      setQuantity(prev => prev - 1);
    }
  };

  const onPressAdd = () => setQuantity(prev => prev + 1);

  return (
    <View
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.white},
      ]}>
      <View style={localStyles.rightContainer}>
        <CText type={'b15'} style={styles.mb5} color={colors.orange}>
          {strings.interView}
        </CText>
        <CText type={'b16'} style={styles.mb5}>
          Батаа Дорж
        </CText>
        <View style={localStyles.timeText}>
          <CText type={'m14'}>Өнөөдөр</CText>
          <CText type={'b5'} style={localStyles.dot}>
            {'\u2B24'}
          </CText>
          <CText type={'m14'}>17:00 - 17:30</CText>
        </View>
      </View>
      <View>
        <Image
          source={{
            uri: 'https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1160&q=80',
          }}
          style={[
            localStyles.productImageStyle,
            {backgroundColor: colors.dark ? colors.imageBg : colors.white},
          ]}
        />
      </View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    ...styles.p15,
    ...styles.flexRow,
    ...styles.mv10,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
  },
  productImageStyle: {
    height: moderateScale(80),
    width: moderateScale(80),
    borderRadius: moderateScale(20),
    resizeMode: 'contain',
  },
  rightContainer: {
    ...styles.flex,
  },

  timeText: {
    ...styles.flexRow,
  },
  dot: {...styles.mh10, ...styles.selfCenter},
});
