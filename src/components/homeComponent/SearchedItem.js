import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {deviceWidth, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import variables from '../../api/variables';

// Vector icons
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function SearchedItem({
  onPressFindedItem,
  data,
  onPressLike,
  liked,
}) {
  const colors = useSelector(state => state.theme.theme);
  const {
    Aimag,
    District,
    experience,
    class_price,
    Instructors,
    PaymentInterval,
    class_name,
    is_customer,
    Company,
  } = data;
  console.log(data);
  const instructor = Instructors[0]?.Instructor;
  const formattedNumber = class_price.toLocaleString(); // Formats as per user's locale
  return (
    <TouchableOpacity
      key={data?.id}
      onPress={onPressFindedItem}
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.white},
      ]}>
      <View style={[styles.rowStart, styles.p10]}>
        <View style={[styles.p10]}>
          <CText style={styles.mb5} type={'M16'}>
            {instructor?.first_name}
          </CText>
          <CText style={styles.mb5} type={'M14'}>
            {class_name}
          </CText>
          <View style={[styles.rowSpaceBetween, styles.mb5]}>
            <View style={[styles.rowStart, styles.pr10]}>
              <AntDesign
                name="star"
                size={moderateScale(15)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText>5.0</CText>
            </View>
            <View style={[styles.rowStart, styles.pr10]}>
              <Ionicons
                name="shield-checkmark"
                size={moderateScale(15)}
                color={colors.carelyLogoColor}
                style={styles.mr5}
              />
              <CText>Care-Check</CText>
            </View>
            <View style={[styles.rowStart]}>
              <AntDesign
                name="codepen-circle"
                size={moderateScale(15)}
                color={colors.carelyLogoColor}
                style={styles.mr5}
              />
              <CText>Premium</CText>
            </View>
          </View>
          <View>
            <View style={[styles.rowStart, styles.mb5]}>
              <Ionicons
                name="time-outline"
                size={moderateScale(15)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText> Туршлага ({experience}) жил</CText>
            </View>
            {!is_customer && (
              <View style={[styles.rowStart, styles.mb5]}>
                <MaterialCommunityIcons
                  name="account-group"
                  size={moderateScale(15)}
                  color={colors.orange}
                  style={styles.mr5}
                />
                <CText> {Company?.name}</CText>
              </View>
            )}
            <View style={[styles.rowStart, styles.mb5]}>
              <Entypo
                name="compass"
                size={moderateScale(15)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText>{Aimag?.aimag_name}</CText>
              <CText type={'B5'} style={styles.ph10}>
                {'\u2B24'}
              </CText>
              <CText>{District?.district_name}</CText>
            </View>
            <View style={styles.rowStart}>
              <AntDesign
                name="star"
                size={moderateScale(15)}
                color={colors.orange}
                style={styles.mr5}
              />
              <CText>
                {formattedNumber} {PaymentInterval?.const_value}
              </CText>
            </View>
          </View>
        </View>
        <View style={[styles.flexCenter]}>
          <Image
            source={{
              uri: `${variables.IMAGEURL}/${instructor?.image}`,
            }}
            style={[
              localStyles.productImageStyle,
              {backgroundColor: colors.dark ? colors.imageBg : colors.white},
            ]}
          />
          {/* <CText>active 2 hours ago </CText> */}
        </View>
      </View>
      <View style={[styles.rowSpaceBetween, styles.pb10, styles.ph20]}>
        <TouchableOpacity
          style={[
            localStyles.button,
            {backgroundColor: colors.carelyLogoColor},
          ]}>
          <CText
            color={colors.white}
            type={'m14'}
            style={[styles.p5, styles.ph10]}>
            Захиалах
          </CText>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPressLike}>
          <AntDesign
            name={liked === false ? 'hearto' : 'heart'}
            size={moderateScale(20)}
            color={colors.orange}
            style={styles.mr5}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  button: {
    borderRadius: moderateScale(20),
  },
  productContainer: {
    ...styles.mt15,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
    width: deviceWidth - moderateScale(25),
  },
  productImageStyle: {
    height: moderateScale(60),
    width: moderateScale(60),
    borderRadius: moderateScale(15),
  },
  titleContainer: {
    ...styles.flex,
    ...styles.p5,
  },
});
