import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// custom imports
import {styles} from '../../themes';
import {
  HeartDark,
  HeartLight,
  NotificationDark,
  NotificationLight,
} from '../../assets/svgs';
import {moderateScale} from '../../common/constants';
import {StackNav} from '../../navigation/NavigationKeys';
import images from '../../assets/images';

function HomeHeader() {
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const onPressNotification = () => navigation.navigate(StackNav.Notification);
  const onPressLike = () => navigation.navigate(StackNav.MyWishlist);

  return (
    <View style={localStyles.headerContainer}>
      <View style={localStyles.userImageContainer}>
        <Image
          source={colors.dark ? images.carelyLogo : images.carelyLogo}
          style={localStyles.userImageStyle}
        />
      </View>
      <View style={styles.rowCenter}>
        <TouchableOpacity onPress={onPressNotification} style={styles.mh10}>
          {colors.dark ? <NotificationDark /> : <NotificationLight />}
        </TouchableOpacity>
        <TouchableOpacity onPress={onPressLike}>
          {colors.dark ? (
            <HeartDark height={moderateScale(23)} width={moderateScale(23)} />
          ) : (
            <HeartLight height={moderateScale(23)} width={moderateScale(23)} />
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default React.memo(HomeHeader);

const localStyles = StyleSheet.create({
  headerContainer: {
    ...styles.rowSpaceBetween,
    ...styles.mv5,
  },
  userImageStyle: {
    width: moderateScale(100),
    height: moderateScale(60),
  },
  userImageContainer: {
    width: moderateScale(120),
    height: moderateScale(50),
  },
});
