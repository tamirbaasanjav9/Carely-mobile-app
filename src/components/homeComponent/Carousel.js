import React, {useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';

//custom imports
import CText from '../common/CText';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {colors, commonColor, styles} from '../../themes';
import variables from '../../api/variables';

// dependencies
import {useSelector} from 'react-redux';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {useNavigation} from '@react-navigation/native';
import {StackNav} from '../../navigation/NavigationKeys';

const MyCarousel = ({company}) => {
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const [activeIndex, setActiveIndex] = useState(0);
  const handleButton = item => {
    navigation.navigate(StackNav.CompanyDetail, {item: item});
  };

  const renderItem = ({item}) => {
    const {name, logo} = item;
    return (
      <View
        style={[
          localStyles.container,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.white,
          },
        ]}>
        <Image
          resizeMode="cover"
          source={{uri: `${variables.IMAGEURL}/${logo}`}}
          style={[
            localStyles.swiperImageStyle,
            {
              backgroundColor: colors.dark ? colors.dark2 : colors.white,
            },
          ]}
        />
        <TouchableOpacity
          onPress={() => handleButton(item)}
          style={[
            {
              backgroundColor: colors.dark
                ? commonColor.darkButton
                : commonColor.grayScale1,
            },
            localStyles.buttonContainer,
          ]}>
          <CText type={'b14'} style={[styles.pv15]}>
            {name}
          </CText>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.flexCenter}>
      <Carousel
        data={company}
        renderItem={renderItem}
        sliderWidth={deviceWidth - moderateScale(20)} // Adjust width as needed
        itemWidth={deviceWidth - moderateScale(60)} // Adjust width as needed
        loop={true}
        autoplay={true}
        autoplayInterval={3000}
        onSnapToItem={index => setActiveIndex(index)}
      />
      <Pagination
        dotsLength={company.length}
        activeDotIndex={activeIndex}
        containerStyle={localStyles.paginationContainer}
        dotStyle={localStyles.paginationDot}
        inactiveDotStyle={localStyles.paginationInactiveDot}
        inactiveDotOpacity={0.9}
        inactiveDotScale={0.8}
      />
    </View>
  );
};
const localStyles = StyleSheet.create({
  container: {
    ...styles.flex,
    borderRadius: moderateScale(10),
  },
  swiperImageStyle: {
    width: deviceWidth - moderateScale(60),
    height: getHeight(160),
    borderRadius: moderateScale(10),
  },
  buttonContainer: {
    ...styles.flex,
    ...styles.mv10,
    ...styles.mh10,
    borderRadius: moderateScale(20),
    ...styles.center,
  },
  paginationContainer: {
    marginTop: -20, // Adjust as needed
  },
  paginationDot: {
    width: moderateScale(10),
    height: moderateScale(10),
    borderRadius: moderateScale(5),
    backgroundColor: colors.dark ? commonColor.dark2 : commonColor.grayScale1,
  },
});
export default MyCarousel;
