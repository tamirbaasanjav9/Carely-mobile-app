import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {FlashList} from '@shopify/flash-list';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {mostPopularData} from '../../api/constant';
import {styles} from '../../themes';
import {moderateScale} from '../../common/constants';
import strings from '../../i18n/strings';
import images from '../../assets/images';

export default function MostPopularCategory(props) {
  const {chipsData, isStar = false, data} = props;
  console.log(data);

  const colors = useSelector(state => state.theme.theme);
  const initialSelectedChips = chipsData
    ? [chipsData[0]]
    : data
    ? [data[0]]
    : [];
  const [selectedChips, setSelectedChips] = useState(initialSelectedChips);
  const [extraData, setExtraData] = useState(true);

  useEffect(() => {
    setExtraData(!extraData);
  }, [selectedChips, colors]);

  const onPressChips = value => {
    if (selectedChips.includes(value)) {
      setSelectedChips(selectedChips.filter(item => item !== value));
    } else {
      setSelectedChips([value]);
    }
  };

  const renderChips = ({item}) => {
    console.log(item);
    return (
      <TouchableOpacity
        onPress={() => onPressChips(item)}
        style={[
          localStyles.chipsContainer,
          {borderColor: colors.dark ? colors.dark3 : colors.black},
          {
            backgroundColor: selectedChips.includes(item)
              ? colors.dark
                ? colors.dark3
                : colors.black
              : colors.tranparent,
          },
        ]}>
        {!!isStar && (
          <Image
            source={images.starFill}
            style={[
              localStyles.starStyle,
              {
                tintColor: selectedChips.includes(item)
                  ? colors.dark
                    ? colors.white
                    : colors.white
                  : colors.dark
                  ? colors.white
                  : colors.black,
              },
            ]}
          />
        )}
        <CText
          type={'b12'}
          color={
            selectedChips.includes(item)
              ? colors.dark
                ? colors.white
                : colors.white
              : colors.dark
              ? colors.white
              : colors.black
          }>
          {item?.name}
        </CText>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <FlashList
        data={!!chipsData ? chipsData : data}
        renderItem={renderChips}
        extraData={extraData}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.mb15}
        estimatedItemSize={10}
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  chipsContainer: {
    ...styles.ph10,
    ...styles.pv5,
    ...styles.mr5,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(5),
    ...styles.rowCenter,
  },
  starStyle: {
    width: moderateScale(16),
    height: moderateScale(16),
    resizeMode: 'contain',
    ...styles.mr10,
  },
});
