// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {colors, styles} from '../../themes';
import strings from '../../i18n/strings';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {dateText} from '../../api/constant';

export default function SearchRenderItems(props) {
  const colors = useSelector(state => state.theme.theme);
  const {item, isTrash = false, trashIcon = true, onPressFindedItem} = props;
  return (
    <TouchableOpacity
      onPress={onPressFindedItem}
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.grayScale1},
      ]}>
      <View style={localStyles.titleContainer}>
        <Image
          source={{uri: item?.productImage}}
          style={[
            localStyles.productImageStyle,
            {backgroundColor: colors.dark ? colors.imageBg : colors.white},
          ]}
        />
        <View style={localStyles.titleContainer}>
          <CText style={styles.flex} numberOfLines={1} type={'b16'}>
            {item?.product}
          </CText>
        </View>
      </View>
      <View
        style={[
          localStyles.rightContainer,
          {backgroundColor: colors.dark ? colors.imageBg : colors.white},
        ]}>
        <View style={localStyles.subItemStyle}>
          <View style={localStyles.subItemStyleText}>
            <View style={[localStyles.circleContainer]}>
              <Ionicons
                name="person"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'} style={{...styles.mr5}}>
              1 Хүүхэд{' '}
            </CText>
            <CText type={'s5'} style={{...styles.mr5}}>
              {'\u2B24'}
            </CText>
            {!!item?.size && (
              <CText type={'b13'}>
                {strings.Toddler + ' , ' + strings.junnior}
              </CText>
            )}
          </View>
          <View style={localStyles.subItemStyleTextLocation}>
            <View style={[localStyles.circleContainer]}>
              <Ionicons
                name="location"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'} style={{...styles.mr5}}>
              Сүхбаатар, УБ{' '}
            </CText>
            <CText type={'s5'} style={{...styles.mr5}}>
              {'\u2B24'}
            </CText>
            {!!item?.size && <CText type={'b13'}>6 {strings.mile}</CText>}
          </View>
          <View style={localStyles.subItemStyleTextStart}>
            <View style={localStyles.dateText}>
              <View style={[localStyles.circleContainer]}>
                <Ionicons
                  name="calendar"
                  size={moderateScale(18)}
                  color={colors.grayScale5}
                />
              </View>
              <View>
                <CText type={'b13'} style={{...styles.mr5}}>
                  ЭХЛЭХ{' '}
                </CText>
                {!!item?.size && (
                  <CText
                    type={'b13'}
                    style={{color: colors.carelyLogoColor, ...styles.mt5}}>
                    Tue, Oct 14
                  </CText>
                )}
              </View>
            </View>
            <View style={localStyles.dateText}>
              <View style={[localStyles.circleContainer]}>
                <Ionicons
                  name="calendar"
                  size={moderateScale(18)}
                  color={colors.white}
                />
              </View>
              <View>
                <CText type={'b13'} style={{...styles.mr5}}>
                  ЦАГ{' '}
                </CText>
                {!!item?.size && (
                  <CText
                    type={'b13'}
                    color={colors.carelyLogoColor}
                    style={{...styles.mt5}}>
                    08:00 - 12:00
                  </CText>
                )}
              </View>
            </View>
          </View>
          <View
            style={[
              {
                flexDirection: 'row',
              },
              {...styles.mt10},
              {paddingLeft: 28},
            ]}>
            {dateText.map(item => (
              <View
                style={[
                  localStyles.circleDate,
                  {
                    backgroundColor: item.backgroundColor
                      ? colors.carelyLogoColor
                      : '#BDBDBD',
                  },
                ]}>
                <CText align={'center'} type={'b12'} color={colors.white}>
                  {item.weekday}
                  {'   '}
                </CText>
              </View>
            ))}
          </View>
        </View>
        <View style={localStyles.btnContainer}>
          <TouchableOpacity
            style={{backgroundColor: colors.carelyLogoColor, borderRadius: 5}}>
            <CText type={'b13'} style={[{...styles.m5}, {color: colors.white}]}>
              Тогтмол
            </CText>
          </TouchableOpacity>
          <View style={{justifyContent: 'flex-end'}}>
            <CText type={'b14'}>₮10000-₮15000 / Цагт</CText>
            <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
              <CText type={'m12'}>2 хүсэлт гаргасан</CText>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    // ...styles.p15,
    ...styles.mt15,
    borderRadius: moderateScale(20),
    ...styles.shadowStyle,
    ...styles.selfCenter,
    width: deviceWidth - moderateScale(40),
    // minHeight: moderateScale(130),
  },
  productImageStyle: {
    height: moderateScale(50),
    width: moderateScale(50),
    borderRadius: moderateScale(20),
    resizeMode: 'contain',
    marginRight: moderateScale(15),
  },
  rightContainer: {
    ...styles.flex,
    ...styles.justifyBetween,
    ...styles.p10,
    borderBottomLeftRadius: moderateScale(20),
    borderBottomRightRadius: moderateScale(20),
  },
  titleContainer: {
    ...styles.rowSpaceBetween,
    ...styles.flex,
    ...styles.p10,
  },
  circleContainer: {
    ...styles.mr10,
  },
  circleDate: {
    ...styles.mr5,
    borderRadius: 10,
    ...styles.p5,
  },
  subItemStyle: {
    ...styles.mv10,
  },
  subItemStyleText: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mv5,
  },
  subItemStyleTextLocation: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mb5,
  },
  subItemStyleTextStart: {
    ...styles.flexRow,
    ...styles.itemsCenter,
  },
  dateText: {
    ...styles.flexRow,
  },
  btnContainer: {
    ...styles.rowSpaceBetween,
    ...styles.mb10,
    paddingLeft: 28,
  },
});
