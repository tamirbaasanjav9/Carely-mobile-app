import {Image, StyleSheet, View} from 'react-native';
import React, {memo} from 'react';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {useSelector} from 'react-redux';

// Custom Imports
import {styles} from '../../themes';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import variables from '../../api/variables';
import CButton from '../common/CButton';
import strings from '../../i18n/strings';

const HomeBanner = ({image, company}) => {
  console.log(company);
  const colors = useSelector(state => state.theme.theme);
  const renderSwiperItem = ({item}) => {
    return (
      <View style={localStyles.swiperItemContainer}>
        <Image
          resizeMode="cover"
          source={{uri: `${variables.IMAGEURL}/${item?.image}`}}
          style={[
            localStyles.swiperImageStyle,
            {
              backgroundColor: colors.dark
                ? colors.dark3
                : colors.transparentSilver,
            },
          ]}
        />
        <CButton
          // onPress={onPressLike}
          type={'b16'}
          title={'Үзэх'}
          style={[styles.mh10]}
          containerStyle={localStyles.buttonContainer}
        />
      </View>
    );
  };

  return (
    <View>
      <SwiperFlatList
        data={company}
        autoplay
        autoplayDelay={2}
        autoplayLoop
        index={0}
        pagingEnabled={true}
        showPagination
        renderItem={renderSwiperItem}
        paginationStyleItemActive={{
          ...localStyles.paginationStyleItemActive,
          backgroundColor: colors.dark ? colors.grayScale4 : colors.dark2,
        }}
        paginationStyleItemInactive={{
          ...localStyles.paginationStyleItemInactive,
          backgroundColor: colors.dark ? colors.dark2 : colors.grayScale4,
        }}
        paginationStyleItem={localStyles.paginationStyleItem}
        style={localStyles.swiperStyle}
      />
    </View>
  );
};

const localStyles = StyleSheet.create({
  paginationStyleItem: {
    ...styles.mh5,
    ...styles.mt15,
  },
  paginationStyleItemActive: {
    height: getHeight(6),
    width: moderateScale(16),
  },
  paginationStyleItemInactive: {
    height: moderateScale(6),
    width: moderateScale(6),
  },
  swiperItemContainer: {
    ...styles.mb10,
    ...styles.mt10,
    ...styles.mr20,
    ...styles.ml5,
    width: deviceWidth - moderateScale(25),
    ...styles.center,
  },
  swiperStyle: {
    overflow: 'hidden',
  },
  swiperImageStyle: {
    width: deviceWidth - moderateScale(25),
    height: getHeight(160),
    borderRadius: moderateScale(20),
  },
  buttonContainer: {
    ...styles.mv10,
    height: getHeight(40),
    width: deviceWidth - moderateScale(50),
  },
});

export default memo(HomeBanner);
