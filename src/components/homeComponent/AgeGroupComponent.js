import {FlatList, StyleSheet, View} from 'react-native';
import React from 'react';
import {moderateScale} from '../../common/constants';
import {commonColor, styles} from '../../themes';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CText from '../common/CText';

const AgeGroupComponent = ({AgeGroup}) => {
  const renderChildAge = ({item, index}) => {
    const {AgeGroupRef} = item;
    return (
      <View key={index} style={[localStyles.categoryRoot]}>
        <View style={[localStyles.iconContainer]}>
          <MaterialIcons
            name="child-friendly"
            size={moderateScale(30)}
            color={'white'}
            style={styles.p15}
          />
        </View>
        <CText
          type="b13"
          numberOfLines={3}
          align={'center'}
          style={[styles.mt5]}>
          {AgeGroupRef?.AgeGroup?.age_group_name}
        </CText>
        <View style={styles.rowCenter}>
          <CText
            type="b13"
            align={'center'}
            color={commonColor.grayScale5}
            style={[styles.mt2]}>
            {AgeGroupRef?.AgeGroup?.age_range_min}
          </CText>
          <CText
            type="b13"
            align={'center'}
            color={commonColor.grayScale5}
            style={[styles.mt2]}>
            {AgeGroupRef?.AgeGroup?.age_range_max === null ? null : '-'}
          </CText>
          <CText
            type="b13"
            align={'center'}
            color={commonColor.grayScale5}
            style={[styles.mt2]}>
            {AgeGroupRef?.AgeGroup?.age_range_max === null
              ? '+'
              : AgeGroupRef?.AgeGroup?.age_range_max}
          </CText>
        </View>
      </View>
    );
  };

  return (
    <View>
      <FlatList
        data={AgeGroup}
        renderItem={renderChildAge}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
      />
    </View>
  );
};

export default AgeGroupComponent;

const localStyles = StyleSheet.create({
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mr5,
    ...styles.pv5,
    ...styles.pr10,
  },
  iconContainer: {
    borderRadius: moderateScale(25),
    ...styles.center,
    backgroundColor: commonColor.carelyLogoColor,
  },
});
