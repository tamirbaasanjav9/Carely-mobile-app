import React from 'react';
import {FlashList} from '@shopify/flash-list';
import {useNavigation} from '@react-navigation/native';

// Custom Imports
import {homeProductData} from '../../api/constant';
import ProductShortDetail from './ProductShortDetail';
import {StackNav} from '../../navigation/NavigationKeys';
import {ActivityIndicator} from 'react-native';

export default function HomeProductComponent(route) {
  console.log(route);
  const {data, isLoading, loadMoreData} = route;
  const navigation = useNavigation();
  const onPressDetail = itm =>
    navigation.navigate(StackNav.ProductDetail, {item: itm});

  const renderItem = ({item, index}) => {
    return (
      <ProductShortDetail
        item={item}
        index={index}
        onPress={() => onPressDetail(item)}
      />
    );
  };

  return (
    <FlashList
      data={data}
      renderItem={renderItem}
      numColumns={2}
      keyExtractor={(item, index) => index.toString()}
      estimatedItemSize={10}
      onEndReached={loadMoreData}
      onEndReachedThreshold={0.1}
      ListFooterComponent={isLoading ? <ActivityIndicator /> : null}
    />
  );
}
