import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';
import {moderateScale} from '../../common/constants';
import CText from '../common/CText';
import {colors, styles} from '../../themes';
import {Search_Icon} from '../../assets/svgs';
import strings from '../../i18n/strings';
import {useNavigation} from '@react-navigation/native';
import {StackNav} from '../../navigation/NavigationKeys';

const SearchComponentButton = ({}) => {
  const colors = useSelector(state => state.theme.theme);
  const navigation = useNavigation();

  const onPressCategoryChild = () => navigation.navigate(StackNav.Searta);
  return (
    <TouchableOpacity
      onPress={onPressCategoryChild}
      style={[
        localStyles.contactUsContainer,
        {
          backgroundColor: colors.dark ? colors.dark2 : colors.white,
        },
      ]}>
      <Search_Icon />
      <CText
        type={'r16'}
        style={[{color: colors.grayScale5}, {...styles.pl25}]}>
        {strings.search}
      </CText>
    </TouchableOpacity>
  );
};

export default SearchComponentButton;

const localStyles = StyleSheet.create({
  contactUsContainer: {
    ...styles.mt20,
    ...styles.mb20,
    ...styles.pv15,
    borderRadius: moderateScale(17),
    ...styles.flexRow,
    ...styles.ph10,
    borderWidth: 1,
    borderColor: '#E0E0E0',
  },
});
