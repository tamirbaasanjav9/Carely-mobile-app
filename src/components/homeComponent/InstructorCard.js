// Library Imports
import {Image, StyleSheet, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {deviceWidth, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import variables from '../../api/variables';

// Vector icons
import Zocial from 'react-native-vector-icons/Zocial';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function InstructorCard(route) {
  console.log(route);
  const {instructor} = route;
  console.log(instructor);
  const colors = useSelector(state => state.theme.theme);
  return (
    <View
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.white},
      ]}>
      <View style={localStyles.headerColumn}>
        <View style={[styles.pt10]}>
          <Image
            source={{
              uri: `${variables.IMAGEURL}/${instructor?.Instructor?.image}`,
            }}
            style={[
              localStyles.productImageStyle,
              {backgroundColor: colors.dark ? colors.imageBg : colors.white},
            ]}
          />
        </View>
        <View style={localStyles.titleContainer}>
          <View style={[styles.rowSpaceBetween, styles.pb10]}>
            <CText style={{}} numberOfLines={2} type={'b16'}>
              {instructor?.Instructor?.last_name}
              {'  '}
              {instructor?.Instructor?.first_name}
            </CText>
          </View>
          <View style={localStyles.subItemStyleTextLocation}>
            <View style={[localStyles.circleContainer]}>
              <Zocial
                name="email"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'}>{instructor?.Instructor?.email}</CText>
          </View>
          <View style={localStyles.subItemStyleTextLocation}>
            <View style={[localStyles.circleContainer]}>
              <AntDesign
                name="phone"
                size={moderateScale(18)}
                color={colors.grayScale5}
              />
            </View>
            <CText type={'b13'}>{instructor?.Instructor?.phone_number}</CText>
          </View>
        </View>
      </View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    ...styles.mb15,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
    width: deviceWidth - moderateScale(20),
  },
  productImageStyle: {
    height: moderateScale(60),
    width: moderateScale(60),
    borderRadius: moderateScale(20),
    marginRight: moderateScale(5),
  },
  titleContainer: {
    ...styles.flex,
    ...styles.p10,
  },
  headerColumn: {
    flexDirection: 'row',
    ...styles.flex,
    ...styles.ph10,
    ...styles.pt10,
    justifyContent: 'space-between',
  },
  circleContainer: {
    ...styles.mr10,
  },
  subItemStyleTextLocation: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mb5,
  },
  descriptionContainer: {
    ...styles.ph10,
  },
});
