// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../common/CText';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {commonColor, styles} from '../../themes';
import variables from '../../api/variables';

// Vector icons
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function DetialHeader(route) {
  const {onPressFindedItem, data} = route;
  const aimag = data?.Aimag;
  const district = data?.District;
  const Company = data?.Company;
  const Schedules = data?.Schedules;
  const image = data?.Images;
  const formattedNumber = data?.class_price.toLocaleString(); // Formats as per user's locale
  const dateStart = new Date(data?.class_start_date);
  const formattedDate = dateStart.toLocaleDateString();

  const colors = useSelector(state => state.theme.theme);
  return (
    <TouchableOpacity
      key={data?.id}
      onPress={onPressFindedItem}
      style={[
        localStyles.productContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.white},
      ]}>
      <View>
        <Image
          resizeMode="cover"
          // source={images.bag5}
          // source={{uri: `${variables.IMAGEURL}/${image[0]?.image_url}`}}
          style={[
            localStyles.swiperImageStyle,
            {
              backgroundColor: colors.dark
                ? commonColor.carelyLight
                : colors.transparentSilver,
            },
          ]}
        />
        <View style={[localStyles.onImage]}>
          <Image
            source={{
              uri: `${variables.IMAGEURL}/${Company?.image}`,
            }}
            style={[
              localStyles.productImageStyle,
              {backgroundColor: colors.dark ? colors.imageBg : colors.white},
            ]}
          />
        </View>
        <View
          style={[
            localStyles.onText,
            {
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
            },
          ]}>
          <CText
            color={commonColor.carelyLight}
            style={[styles.ph10, styles.pv5]}
            type={'b15'}>
            ₮{formattedNumber}
          </CText>
        </View>
      </View>
      <View style={styles.rowSpaceBetween}>
        <View style={[styles.center, styles.ph10]}>
          <View style={styles.center}>
            <CText style={styles.mr10} type={'b13'}>
              ЭХЛЭХ
            </CText>
            <CText type={'b13'}>ХУГАЦАА</CText>
          </View>
          <CText
            style={styles.pt10}
            color={commonColor.carelyLogoColor}
            type={'b13'}>
            {formattedDate}
          </CText>
        </View>
        <View style={localStyles.headerColumn}>
          <View style={localStyles.titleContainer}>
            <View style={[styles.rowSpaceBetween, styles.pb5]}>
              <CText style={{}} numberOfLines={2} type={'b16'}>
                {data?.class_name}
              </CText>
              <View style={[styles.rowSpaceBetween]}>
                <Octicons
                  name="star-fill"
                  size={moderateScale(15)}
                  color={colors.orange}
                  style={styles.mr5}
                />
                <CText type={'b13'}>4.85</CText>
              </View>
            </View>
            <View style={localStyles.subItemStyleTextLocation}>
              <View style={[localStyles.circleContainer]}>
                <Ionicons
                  name="location"
                  size={moderateScale(18)}
                  color={commonColor.carelyLogoColor}
                />
              </View>
              <CText type={'m14'} style={{...styles.mr5}}>
                {district?.district_name}
              </CText>
              <CText type={'b5'} style={{...styles.mr5}}>
                {'\u2B24'}
              </CText>
              <CText type={'m14'}>{aimag?.aimag_name}</CText>
            </View>
            <View style={localStyles.subItemStyleTextLocation}>
              <View style={[localStyles.circleContainer]}>
                <Ionicons
                  name="ribbon-outline"
                  size={moderateScale(18)}
                  color={commonColor.carelyLogoColor}
                />
              </View>
              <CText type={'m14'}>Туршлага ( {data?.experience} ) жил </CText>
            </View>
            <View style={localStyles.subItemStyleTextLocation}>
              <CText type={'m14'} style={{...styles.mr5}}>
                {Schedules[0]?.StartTime?.const_value}
              </CText>
              <CText style={{...styles.mr5}}>-</CText>
              <CText type={'m14'}>{Schedules[0]?.EndTime?.const_value}</CText>
              <CText type={'b5'} style={{...styles.mh5}}>
                {'\u2B24'}
              </CText>
              <CText type={'m14'}>{Schedules[0]?.Day?.const_value}</CText>
            </View>
            <View style={localStyles.desc}>
              <CText numberOfLines={1} type={'m14'}>
                {data?.class_description}
              </CText>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  onImage: {
    position: 'absolute',
    top: moderateScale(10),
    left: moderateScale(10),
  },
  onText: {
    position: 'absolute',
    ...styles.selfEnd,
    right: moderateScale(10),
    borderRadius: moderateScale(5),
    top: moderateScale(10),
  },
  productContainer: {
    ...styles.mt15,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
    width: deviceWidth - moderateScale(25),
  },
  productImageStyle: {
    height: moderateScale(45),
    width: moderateScale(45),
    borderRadius: moderateScale(15),
  },
  titleContainer: {
    ...styles.flex,
    ...styles.p5,
  },
  swiperImageStyle: {
    width: deviceWidth - moderateScale(25),
    height: getHeight(200),
    borderTopStartRadius: moderateScale(20),
    borderTopEndRadius: moderateScale(20),
  },
  headerColumn: {
    flexDirection: 'row',
    ...styles.flex,
    ...styles.ph10,
    justifyContent: 'space-between',
  },
  circleContainer: {
    ...styles.mr5,
  },
  subItemStyleTextLocation: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mb5,
  },
  desc: {
    ...styles.flexRow,
    ...styles.itemsCenter,
    ...styles.mb10,
  },
});
