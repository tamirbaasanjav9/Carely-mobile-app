// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';

// Custom Imports
import {commonColor, styles} from '../../themes';
import {deviceWidth, moderateScale} from '../../common/constants';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CText from '../common/CText';
import {useSelector} from 'react-redux';

export default function ServiceGoing({typeCare, onSelectItem}) {
  const colors = useSelector(state => state.theme.theme);

  return (
    <TouchableOpacity
      key={typeCare?.id}
      onPress={onSelectItem}
      style={[localStyles.categoryRoot]}>
      <View
        style={[
          localStyles.iconContainer,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.carelyCbg,
          },
        ]}>
        <MaterialCommunityIcons
          name={typeCare?.mobile_icon}
          size={moderateScale(50)}
          color={commonColor.carelyLogoColor}
        />
      </View>
      <CText
        type="b13"
        numberOfLines={3}
        align={'center'}
        color={colors.dark ? commonColor.grayScale5 : commonColor.grayScale5}
        style={[styles.mt10]}>
        {typeCare?.name}
      </CText>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mt15,
    width: deviceWidth / 2 - moderateScale(20),
  },
  iconContainer: {
    padding: moderateScale(45),
    borderRadius: moderateScale(45),
    ...styles.center,
  },
});
