// Library import
import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';

// Custom imports
import CText from '../common/CText';
import {StackNav} from '../../navigation/NavigationKeys';
import {
  Menu_Dark,
  Menu_Light,
  Search_Dark,
  Search_Light,
} from '../../assets/svgs';
import CSafeAreaView from '../common/CSafeAreaView';
import CHeader from '../common/CHeader';
import {moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import {TabView} from 'react-native-tab-view';
import {useNavigation} from '@react-navigation/native';
import ServiceGoing from './ServiceGoing';
import ServiceGive from './ServiceGive';

export default function SelectedTypeScreen({route}) {
  const {ChildSaleType, sale_type_name} = route?.params?.selectedType;
  console.log(ChildSaleType);
  const colors = useSelector(state => state.theme.theme);
  const navigation = useNavigation();
  const [isSelect, setIsSelect] = useState({
    index: 0,
    routes: [
      {
        key: 'first',
        title: ChildSaleType[0]?.sale_type_name,
        detail: ChildSaleType[0]?.SaleTypeDetail,
      },
      {
        key: 'second',
        title: ChildSaleType[1]?.sale_type_name,
        detail1: ChildSaleType[1]?.SaleTypeDetail,
      },
    ],
  });

  const _handleIndexChange = index => {
    setIsSelect({...isSelect, index: index});
  };

  const HeaderCetegoryItem = ({title, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleIndexChange(index)}
        style={[
          localStyles.root,
          {
            borderBottomColor:
              isSelect.index === index ? colors.textColor : colors.dark3,
          },
        ]}>
        <CText
          type={'b15'}
          align={'center'}
          style={styles.pb20}
          color={
            isSelect.index === index ? colors.textColor : colors.grayScale7
          }>
          {title}
        </CText>
      </TouchableOpacity>
    );
  };

  const _renderTabBar = props => {
    return (
      <View style={localStyles.mainContainer}>
        {props.navigationState.routes.map((item, index) => {
          return <HeaderCetegoryItem title={item.title} index={index} />;
        })}
      </View>
    );
  };

  const _renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <ServiceGoing data={route?.detail} />;
      case 'second':
        return <ServiceGive data={route?.detail1} />;
      default:
        return null;
    }
  };

  const onPressSearch = () => navigation.navigate(StackNav.Search);

  const RightIcon = () => {
    return (
      <View style={styles.rowCenter}>
        <TouchableOpacity onPress={onPressSearch}>
          {colors.dark ? <Search_Dark /> : <Search_Light />}
        </TouchableOpacity>
        <TouchableOpacity style={styles.ph10}>
          {colors.dark ? <Menu_Dark /> : <Menu_Light />}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader title={sale_type_name} rightIcon={<RightIcon />} />
      <TabView
        navigationState={isSelect}
        renderScene={_renderScene}
        renderTabBar={_renderTabBar}
        onIndexChange={_handleIndexChange}
        activeColor={{color: colors.primary}}
        navigation={navigation}
      />
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    borderBottomWidth: moderateScale(2),
    width: '50%',
  },
  mainContainer: {
    ...styles.rowSpaceBetween,
    ...styles.ph20,
    ...styles.mt10,
    width: '100%',
  },
  userImageStyle: {
    width: moderateScale(100),
    height: moderateScale(60),
  },
});
