import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {moderateScale} from '../../common/constants';
import {styles} from '../../themes';

// Custom imports

const SelectedCategoryPost = ({onPressItem, image, index, name}) => {
  const colors = useSelector(state => state.theme.theme);

  return (
    <TouchableOpacity
      key={index}
      onPress={onPressItem}
      style={localStyles.categoryRoot}>
      <View
        style={[
          localStyles.iconContainer,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.white,
          },
        ]}>
        <Image
          source={{uri: `http://192.168.2.2:3093/${image}`}}
          style={[localStyles.productImageStyle]}
        />
      </View>
      <CText
        type="b14"
        numberOfLines={2}
        align={'center'}
        color={colors.textColor}
        style={[styles.mt10]}>
        {name}
      </CText>
    </TouchableOpacity>
  );
};

export default SelectedCategoryPost;

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryRoot: {
    width: '100%',
    ...styles.itemsCenter,
    ...styles.mt20,
  },
  productImageStyle: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(35),
    ...styles.selfCenter,
  },
  uppercaseText: {
    textTransform: 'uppercase',
  },
  iconContainer: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(35),
    ...styles.center,
  },
});
