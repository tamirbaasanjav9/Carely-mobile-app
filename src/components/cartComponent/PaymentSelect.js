import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

// Custom Imports
import CText from '../common/CText';
import {moderateScale} from '../../common/constants';
import {commonColor, styles} from '../../themes';

export default function PaymentSelect(props) {
  const {item, isSelected, onPressItem, ratio = true} = props;
  const radioButton = isSelected ? 'radio-button-on' : 'radio-button-off';
  const checkButton = isSelected ? 'check-box' : 'check-box-outline-blank';
  return (
    <TouchableOpacity
      onPress={onPressItem}
      style={[localStyles.selectContainer]}>
      <View style={styles.rowCenter}>
        {item?.icon1 && <View style={styles.mh10}>{item?.icon1}</View>}
        <CText type={'m16'}>
          {item &&
            (item?.qualification_name ||
              item?.responsibility_name ||
              item?.responsibility_option)}
        </CText>
      </View>
      <MaterialIcons
        name={ratio === false ? checkButton : radioButton}
        size={moderateScale(24)}
        color={
          isSelected ? commonColor.carelyLogoColor : commonColor.grayScale5
        }
      />
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  selectContainer: {
    ...styles.rowSpaceBetween,
    ...styles.pv15,
    borderRadius: moderateScale(16),
  },
});
