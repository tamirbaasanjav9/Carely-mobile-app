import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

// Custom Imports
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import CSafeAreaView from '../../common/CSafeAreaView';
import CHeader from '../../common/CHeader';
import MapComponent from '../../detailProduct/MapComponent';
import CDivider from '../../common/CDivider';
import CText from '../../common/CText';
import CButton from '../../common/CButton';
import {deviceWidth, moderateScale} from '../../../common/constants';
import {styles} from '../../../themes';
import CInput from '../../common/CInput';
import strings from '../../../i18n/strings';
import Profileheader from './postingComponents/Profileheader';
import CalendarShort from '../../detailProduct/CalendarShort';
import {FlatList} from 'react-native';
import CalendarReview from '../../detailProduct/CalendarReview';

export default function PostingReview({navigation, route}) {
  const {receivedData} = route?.params;
  console.log(receivedData);
  const colors = useSelector(state => state.theme.theme);
  const counter = {
    maxLength: 100,
    descriptionLength: 500,
  };
  const [description, setDescription] = useState(
    receivedData?.receivedData.description,
  );
  const [title, setTitle] = useState(receivedData?.receivedData.title);

  // input
  const onChangedTitle = text => {
    if (text.length <= counter.maxLength) {
      setTitle(text);
    }
  };
  const onChangedDescription = text => {
    if (text.length <= counter.descriptionLength) {
      setDescription(text);
    }
  };
  // ------------------------custom functions
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  //------------------render screen options
  return (
    <CSafeAreaView>
      <CHeader title={'Зар'} rightIcon={<RightIcon />} />
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        ListHeaderComponent={
          <View style={styles.mh15}>
            <Profileheader image={receivedData?.receivedData?.image} />
            <View>
              <CInput
                placeHolder={'Ажлын нэр...'}
                _value={title}
                toGetTextFieldValue={onChangedTitle}
                inputContainerStyle={[
                  {
                    backgroundColor: colors.dark
                      ? colors.backgroundColor
                      : colors.white,
                  },
                  localStyles.inputContainerStyle,
                ]}
              />
              <CText align={'right'} style={[styles.flex]} type={'r12'}>
                {title.length}/{counter.maxLength}
              </CText>
            </View>
            <CalendarReview
              timeData={receivedData?.receivedData?.Schedules}
              startDate={receivedData?.receivedData?.startDate}
              endDate={receivedData?.receivedData?.endDate}
            />
            <View style={localStyles.productText}>
              <CText style={[styles.flex, styles.mb5]} type={'b15'}>
                Дэлгэрэнгүй
              </CText>
              <CInput
                placeHolder={'Дэлгэрэнгүй...'}
                _value={description}
                multiline
                toGetTextFieldValue={onChangedDescription}
                inputContainerStyle={[
                  {
                    backgroundColor: colors.dark
                      ? colors.backgroundColor
                      : colors.white,
                  },
                  localStyles.inputContainerStyle,
                ]}
              />
              <CText align={'right'} style={[styles.flex]} type={'r12'}>
                {description.length}/{counter.descriptionLength}
              </CText>
            </View>
            <CText style={[styles.flex, styles.mb15]} type={'b15'}>
              Байршил
            </CText>
            <CText style={[styles.flex]} type={'m14'}>
              {receivedData?.receivedData?.address}
            </CText>
            <CText style={[styles.flex]} type={'m14'}>
              {receivedData?.receivedData?.locationIds?.aimag_id?.aimag_name}{' '}
              {
                receivedData?.receivedData?.locationIds?.district_id
                  ?.district_name
              }{' '}
              {receivedData?.receivedData?.locationIds?.khoroo_id?.khoroo_name}
            </CText>

            <MapComponent />
            <CDivider />

            <View style={localStyles.productText}>
              <CText style={[styles.flex, styles.mb15]} type={'b15'}>
                Нэмэлт мэдээлэл
              </CText>
              {/* {isSelected &&
              isSelected.map(item => (
                <View style={[styles.rowStart, styles.mb5]}>
                  <CText type={'s5'} style={{...styles.mr10}}>
                    {'\u2B24'}
                  </CText>
                  <CText style={[styles.flex, styles.mb5]} type={'m14'}>
                    {item?.qualification_name}
                  </CText>
                </View>
              ))} */}
            </View>
            <CDivider />

            <View style={localStyles.productText}>
              <CText style={[styles.flex, styles.mb15]} type={'b15'}>
                Нэмэлт мэдээлэл
              </CText>
              {/* {selectRes.map(item => (
              <View style={[styles.rowStart, styles.mb5]}>
                <CText type={'s5'} style={{...styles.mr10}}>
                  {'\u2B24'}
                </CText>
                <CText style={[styles.flex, styles.mb5]} type={'m14'}>
                  {item?.responsibility_name}
                </CText>
              </View>
            ))} */}
            </View>
          </View>
        }
      />
      <View style={styles.ph10}>
        <View style={localStyles.bottomContainer}>
          {/* <CText style={[styles.flex]} type={'b14'}>
            ₮{total?.maxPrice}-₮{total?.maxPrice}/цагт
          </CText> */}
          <CButton
            type={'b15'}
            title={strings.PostNow}
            containerStyle={localStyles.addToCartContainer}
            // frontIcon={colors.dark ? <Cart_Light /> : <Cart_Dark />}
          />
        </View>
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  productText: {
    ...styles.mt10,
  },
  bottomContainer: {
    ...styles.pv10,
    ...styles.rowSpaceBetween,
  },
  addToCartContainer: {
    width: deviceWidth / 2 - moderateScale(15),
    ...styles.shadowStyle,
    borderRadius: moderateScale(15),
  },
  inputContainerStyle: {
    borderWidth: 0,
    borderBottomWidth: 1,
  },
});
