import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
// custom imports
import {moderateScale} from '../../../../common/constants';
import {commonColor, styles} from '../../../../themes';
import CText from '../../../common/CText';

const Quantity = ({onDuration, title}) => {
  const [quantity, setQuantity] = useState(1);

  const onPressRemove = () => {
    if (quantity !== 1) {
      setQuantity(prev => prev - 1);
    }
  };
  const onPressAdd = () => {
    if (quantity !== 25) {
      setQuantity(prev => prev + 1);
    }
  };
  useEffect(() => {
    onDuration(quantity);
  }, [quantity]);

  return (
    <View style={[localStyles.quantityButton]}>
      <CText type={'m16'} align={'center'}>
        {title}
      </CText>
      <View style={{...styles.rowCenter}}>
        <TouchableOpacity onPress={onPressRemove} style={localStyles.remove}>
          <Ionicons
            name={'remove'}
            size={moderateScale(30)}
            color={commonColor.carelyLogoColor}
          />
        </TouchableOpacity>
        <CText type={'b20'} align={'center'} style={{...styles.ph20}}>
          {quantity}
        </CText>
        <TouchableOpacity onPress={onPressAdd} style={localStyles.remove}>
          <Ionicons
            name={'add'}
            size={moderateScale(30)}
            color={commonColor.carelyLogoColor}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Quantity;

const localStyles = StyleSheet.create({
  quantityButton: {
    ...styles.rowSpaceBetween,
    ...styles.mv10,
  },
  remove: {
    borderWidth: moderateScale(1),
    borderColor: commonColor.carelyLogoColor,
    borderRadius: moderateScale(10),
  },
});
