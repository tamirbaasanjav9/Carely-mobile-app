import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
//custom imports
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {
  deviceWidth,
  getHeight,
  moderateScale,
} from '../../../../common/constants';
import {styles} from '../../../../themes';
import CText from '../../../common/CText';
import {useSelector} from 'react-redux';
import strings from '../../../../i18n/strings';

const DateContainer = ({onDateDay, placeHolder}) => {
  const colors = useSelector(state => state.theme.theme);

  const [datePickerVisible, setDatePickerVisible] = useState(false);
  const [dateOfBirth, setDateOfBirth] = useState('');
  const handleDateConfirm = date => {
    var expiryDate = date.toISOString().split('T')[0];
    const day = expiryDate.split('-')[2];
    const month = expiryDate.split('-')[1];
    const year = expiryDate.split('-')[0];
    setDateOfBirth(day + '/' + month + '/' + year);
    setDatePickerVisible(false);
    onDateDay(date.toISOString());
  };
  const hideDatePicker = () => setDatePickerVisible(false);
  const onPressCalender = () => setDatePickerVisible(true);
  return (
    <View>
      <TouchableOpacity
        onPress={onPressCalender}
        style={[
          localStyles.dobStyle,
          {
            borderColor: colors.bColor,
            backgroundColor: colors.input2,
          },
        ]}>
        <Ionicons
          name="calendar"
          size={moderateScale(20)}
          color={colors.grayScale5}
          style={styles.mr10}
        />
        <CText
          type={'r16'}
          color={dateOfBirth ? colors.textColor : colors.grayScale5}>
          {dateOfBirth ? dateOfBirth : placeHolder}
        </CText>
      </TouchableOpacity>
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode="date"
        onConfirm={handleDateConfirm}
        onCancel={hideDatePicker}
        date={new Date()}
        minimumDate={new Date()}
      />
    </View>
  );
};

export default DateContainer;

const localStyles = StyleSheet.create({
  dobStyle: {
    height: getHeight(60),
    borderRadius: moderateScale(12),
    borderWidth: moderateScale(1),
    width: deviceWidth / 2 - moderateScale(20),
    ...styles.mv10,
    ...styles.rowCenter,
  },
});
