import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {Dropdown} from 'react-native-element-dropdown';
import {moderateScale} from '../../../../../common/constants';
import {commonColor, styles} from '../../../../../themes';
import {useSelector} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useEffect} from 'react';
import useService from '../../../../../contexts/useService';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CText from '../../../../common/CText';

const PaymentInterval = ({onRecievedData}) => {
  const [isFocus, setIsFocus] = useState(false);
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData, isLoading} = useService();
  const [value, setValue] = useState(null);

  useEffect(() => {
    fetchData('GET', `/get_const_config_list_fordropdown/4`, true);
  }, []);
  // recieved data
  const lastSelect = item => {
    setValue(item);
    setIsFocus(false);
    onRecievedData(item);
  };
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.const_value}</CText>
        {item.id === value?.id && (
          <AntDesign
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  return (
    <View style={{}}>
      {isLoading !== true ? (
        <Dropdown
          style={[
            localStyles.dropdown,
            isFocus && {borderColor: commonColor.carelyLogoColor},
            {backgroundColor: colors.input2},
          ]}
          placeholderStyle={[
            localStyles.placeholderStyle,
            {
              color: colors.textColor,
            },
          ]}
          selectedTextStyle={[
            localStyles.selectedTextStyle,
            {
              color: colors.textColor,
            },
          ]}
          inputSearchStyle={localStyles.inputSearchStyle}
          iconStyle={localStyles.iconStyle}
          data={data}
          search
          containerStyle={[
            localStyles.dropdownstyle,
            {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
          ]}
          maxHeight={300}
          labelField="const_value"
          valueField="id"
          placeholder={!isFocus ? 'Анги сонгох' : '...'}
          searchPlaceholder="Хайх..."
          activeColor={colors.dark3}
          value={value}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            lastSelect(item);
          }}
          renderLeftIcon={() => (
            <MaterialIcons
              style={{...styles.mr10}}
              color={commonColor.carelyLogoColor}
              name="more-time"
              size={moderateScale(25)}
            />
          )}
          renderItem={renderItem}
        />
      ) : null}
    </View>
  );
};

export default PaymentInterval;

const localStyles = StyleSheet.create({
  dropdown: {
    height: moderateScale(50),
    borderColor: commonColor.grayScale5,
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.mt10,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  dropdownstyle: {
    borderRadius: moderateScale(10),
    borderWidth: 1,
    borderColor: commonColor.carelyLogoColor,
  },
});
