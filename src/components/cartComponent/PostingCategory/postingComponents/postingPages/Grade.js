import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

// dependencies npm
import {Dropdown} from 'react-native-element-dropdown';

// icon import dependencies
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// Custom Imports
import CText from '../../../../common/CText';

// redux
import useService from '../../../../../contexts/useService';
import {colors, commonColor, styles} from '../../../../../themes';
import {moderateScale} from '../../../../../common/constants';
import {useSelector} from 'react-redux';

const Grade = ({onRecievedData}) => {
  // onFocus change color boolean
  const [isFocus, setIsFocus] = useState(false);
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData, isLoading} = useService();
  const [value, setValue] = useState(null);

  // fetch data
  useEffect(() => {
    fetchData('GET', `/get_const_config_list_fordropdown/6`, true);
  }, []);
  // recieved data
  const lastSelect = item => {
    setValue(item);
    setIsFocus(false);
    onRecievedData(item);
  };

  // render data on dropDown
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.const_value}</CText>
        {item.id === value?.id && (
          <AntDesign
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };

  // Render on screen items -------------------------------------------
  return (
    <View style={styles.mt10}>
      {isLoading !== true ? (
        <Dropdown
          style={[
            localStyles.dropdown,
            isFocus && {borderColor: commonColor.carelyLogoColor},
            {backgroundColor: colors.input2},
          ]}
          placeholderStyle={[
            localStyles.placeholderStyle,
            {
              color: colors.textColor,
            },
          ]}
          selectedTextStyle={[
            localStyles.selectedTextStyle,
            {
              color: colors.textColor,
            },
          ]}
          inputSearchStyle={localStyles.inputSearchStyle}
          iconStyle={localStyles.iconStyle}
          data={data}
          search
          containerStyle={[
            localStyles.dropdownstyle,
            {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
          ]}
          maxHeight={300}
          labelField="const_value"
          valueField="id"
          placeholder={!isFocus ? 'Анги сонгох' : '...'}
          searchPlaceholder="Хайх..."
          activeColor={colors.dark3}
          value={value}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            lastSelect(item);
          }}
          renderLeftIcon={() => (
            <MaterialCommunityIcons
              style={{...styles.mr10}}
              color={commonColor.carelyLogoColor}
              name="selection-search"
              size={moderateScale(25)}
            />
          )}
          renderItem={renderItem}
        />
      ) : null}
    </View>
  );
};
export default Grade;

const localStyles = StyleSheet.create({
  dropdown: {
    height: moderateScale(60),
    borderColor: commonColor.grayScale5,
    borderRadius: moderateScale(12),
    ...styles.mb25,
    ...styles.ph10,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  dropdownstyle: {
    borderRadius: moderateScale(10),
    borderWidth: 1,
    borderColor: commonColor.carelyLogoColor,
  },
});
