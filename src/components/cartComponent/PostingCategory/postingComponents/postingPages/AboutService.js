import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {StackNav} from '../../../../../navigation/NavigationKeys';
import {Search_Dark, Search_Light} from '../../../../../assets/svgs';
import CSwitch from '../CSwitch';
import CSafeAreaView from '../../../../common/CSafeAreaView';
import CHeader from '../../../../common/CHeader';
import CButton from '../../../../common/CButton';
import strings from '../../../../../i18n/strings';
import {styles} from '../../../../../themes';
import CText from '../../../../common/CText';
import SubHeader from '../../../../SubHeader';
import AddChild from './AddChild';

export default function AboutService({navigation, route}) {
  console.log(route);
  const [passSelect, setPassSelect] = useState(route?.params?.passSelect);

  // add child name and age
  const [plusedChild, setPlusedChild] = useState([]);
  const [ref_id, setRef_id] = useState(route?.params?.ref_id);

  // passing data from previous screen
  // const {ref_id} = route?.params;
  const colors = useSelector(state => state.theme.theme);

  // toggle pets boolean
  const [dog, setDog] = useState(false);
  const [cat, setCat] = useState(false);
  const [other, setOther] = useState(false);

  // add child array
  useEffect(() => {
    if (route?.params?.passData) {
      setPlusedChild(prevPlusedChild => [
        ...prevPlusedChild,
        route.params.passData,
      ]);
    }
  }, [route?.params?.passData]);
  // onPress sections -------------------------------------------------
  const onPressDog = () => setDog(!dog ? true : false);
  const onPressCat = () => setCat(!cat ? true : false);
  const onPressOther = () => setOther(!other ? true : false);

  // add child detail component
  const onPressAddChild = () => {
    // navigation.navigate(StackNav.AboutChild);
  };

  // bottom button
  const onPressAdd = () => {
    navigation.navigate(StackNav.HourlyRate, {
      ref_id: ref_id,
      plusedChild,
      passSelect,
    });
  };

  //header item
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader rightIcon={<RightIcon />} />
      <ScrollView style={localStyles.root}>
        <View>
          <CText type={'b20'} align={'left'} style={styles.mv25}>
            {'Гэр бүлийн тухай'}
          </CText>
          <SubHeader title1={'Хүүхдүүд'} />
          <AddChild
            onPressAddChild={onPressAddChild}
            plusedChild={plusedChild}
          />
          <SubHeader title1={'Тэжээвэр амьтад'} />
          <CSwitch value={'Нохой'} onPressSwitch={onPressDog} isEnabled={dog} />
          <CSwitch value={'Муур'} onPressSwitch={onPressCat} isEnabled={cat} />
          <CSwitch
            value={'Бусад'}
            onPressSwitch={onPressOther}
            isEnabled={other}
          />
        </View>
      </ScrollView>
      <View style={styles.ph20}>
        <CButton
          title={strings.apply}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
});
