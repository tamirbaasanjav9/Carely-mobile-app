import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import Foundation from 'react-native-vector-icons/Foundation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

// Custom Imports
import {StackNav} from '../../../../../navigation/NavigationKeys';
import {Search_Dark, Search_Light} from '../../../../../assets/svgs';
import CSafeAreaView from '../../../../common/CSafeAreaView';
import CHeader from '../../../../common/CHeader';
import CButton from '../../../../common/CButton';
import {commonColor, styles} from '../../../../../themes';
import CText from '../../../../common/CText';
import CInput from '../../../../common/CInput';
import {moderateScale} from '../../../../../common/constants';
import strings from '../../../../../i18n/strings';
import SubHeader from '../../../../SubHeader';
import {SelectPaymentType} from '../../../../../api/constant';
import useService from '../../../../../contexts/useService';
import PaymentInterval from './PaymentInterval';

export default function HourlyRate({navigation, route}) {
  const {receivedData} = route?.params;
  const colors = useSelector(state => state.theme.theme);
  const [select, setSelect] = useState([]);
  const [startPrice, setStartPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const {data, fetchData, isLoading} = useService();
  console.log(data);
  // fetch data
  useEffect(() => {
    fetchData('GET', `/get_const_config_list_fordropdown/4`, true);
  }, []);
  // typing prices
  const onChangedStart = text => {
    setStartPrice(text);
  };
  const onChangedMax = text => {
    setMaxPrice(text);
  };

  //choose payment type
  const onSelectItems = value => {
    if (select.includes(value)) {
      setSelect(select.filter(item => item !== value));
    } else {
      setSelect([setSelect, value]);
    }
  };

  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };

  const onPressAdd = () => {
    navigation.navigate(StackNav.PostingReview, {
      receivedData: {
        receivedData,
        priceDate: {
          maxPrice,
          select,
        },
      },
    });
  };
  const onRecievedData = () => {
    console.log('jey');
  };

  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <View style={{}}>
          <CText type={'b18'} style={styles.mb10}>
            Цагийн хэмжээ
          </CText>
          <CText type={'r16'} style={[styles.font16, styles.mb10]}>
            {strings.noResultFoundDesc}
          </CText>
          <View style={styles.rowSpaceBetween}>
            <CInput
              insideLeftIcon={() => (
                <Foundation
                  name="dollar"
                  size={moderateScale(24)}
                  color={commonColor.grayScale5}
                />
              )}
              placeHolder={'Эхлэх дүн'}
              _value={startPrice}
              toGetTextFieldValue={onChangedStart}
              keyBoardType={'numeric'}
              inputContainerStyle={[
                {backgroundColor: colors.dark ? colors.dark2 : colors.white},
                localStyles.inputContainerStyle,
              ]}
            />
            <CInput
              insideLeftIcon={() => (
                <Foundation
                  name="dollar"
                  size={moderateScale(24)}
                  color={commonColor.grayScale5}
                />
              )}
              placeHolder={'Дуусах дүн'}
              _value={maxPrice}
              keyBoardType={'numeric'}
              toGetTextFieldValue={onChangedMax}
              inputContainerStyle={[
                {backgroundColor: colors.dark ? colors.dark2 : colors.white},
                localStyles.inputContainerStyle,
              ]}
            />
          </View>
          <PaymentInterval onRecievedData={onRecievedData} />

          <SubHeader title1={strings.paymentType} type={'b14'} />
          <View style={[styles.rowSpaceBetween, styles.ph25, styles.pt20]}>
            {SelectPaymentType.map(item => (
              <TouchableOpacity
                key={item.id}
                onPress={() => onSelectItems(item)}
                style={[localStyles.categoryRoot]}>
                <View
                  style={[
                    localStyles.iconContainer,
                    {
                      backgroundColor: colors.dark
                        ? colors.dark2
                        : colors.white,
                    },
                    {
                      backgroundColor: select.includes(item)
                        ? colors.dark
                          ? colors.carelyLogoColor
                          : colors.carelyLogoColor
                        : colors.dark3,
                    },
                  ]}>
                  <FontAwesome5
                    name={item.icon}
                    size={moderateScale(40)}
                    color={
                      select.includes(item) ? 'white' : commonColor.grayScale5
                    }
                  />
                </View>
                <View style={[{...styles.justifyBetween}]}>
                  <CText
                    type="b13"
                    numberOfLines={3}
                    align={'center'}
                    color={
                      select.includes(item)
                        ? commonColor.carelyLogoColor
                        : commonColor.grayScale5
                    }
                    style={[styles.mt10]}>
                    {item.title}
                  </CText>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </View>
      <View style={styles.ph20}>
        <CButton
          title={'Мэргэшил сонгох'}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  inputContainerStyle: {
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(1),
    width: moderateScale(160),
  },
  categoryRoot: {
    ...styles.itemsCenter,
    ...styles.mh5,
    ...styles.mb15,
    width: moderateScale(100),
  },
  iconContainer: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(35),
    ...styles.center,
  },
});
