import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {StackNav} from '../../../../../navigation/NavigationKeys';
import {Search_Dark, Search_Light} from '../../../../../assets/svgs';
import CSafeAreaView from '../../../../common/CSafeAreaView';
import CHeader from '../../../../common/CHeader';
import CButton from '../../../../common/CButton';
import {styles} from '../../../../../themes';
import CText from '../../../../common/CText';
import PaymentSelect from '../../../PaymentSelect';
import useService from '../../../../../contexts/useService';

export default function Qualifications({navigation, route}) {
  const {ref_id, total, receivedData} = route?.params;
  const colors = useSelector(state => state.theme.theme);
  const {data, fetchData} = useService(); // call service custom
  console.log(data);
  //-------------------selecting qualification's value
  const [isSelected, setIsSelected] = useState([]);

  const onSelectItems = value => {
    if (isSelected.includes(value)) {
      // If the value is already in the array, remove it
      setIsSelected(prevSelectedData =>
        prevSelectedData.filter(item => item !== value),
      );
    } else {
      // If the value is not in the array, add it
      setIsSelected(prevSelectedData => [...prevSelectedData, value]);
    }
  };

  //fetch data qualification's values from api
  useEffect(() => {
    fetchData(
      'GET',
      `/get_ref_responsibility_list/${receivedData?.receivedData?.refId}`,
      false,
    );
  }, [receivedData?.receivedData?.refId]); // Empty dependency array means it runs once when the component mounts

  // onpress buttons
  const onPressAdd = () => {
    navigation.navigate(StackNav.Responsibilities, {
      ref_id: ref_id,
      total,
      isSelected,
    });
  };
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  // top header icon
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  // render header section
  const renderHeader = () => {
    return (
      <View>
        <CText type={'b20'} align={'left'} style={styles.mv25}>
          {'Шаардлага'}
        </CText>
      </View>
    );
  };

  const renderPaymentItem = ({item, index}) => {
    const {Qualification} = item;

    return (
      <PaymentSelect
        item={Qualification}
        isSelected={isSelected.includes(Qualification)}
        onPressItem={() => onSelectItems(Qualification)}
      />
    );
  };

  return (
    <CSafeAreaView>
      <CHeader rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlatList
          data={data}
          renderItem={renderPaymentItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={renderHeader}
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={styles.ph20}>
        <CButton
          title={'Хүүхэд хадгалах'}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
});
