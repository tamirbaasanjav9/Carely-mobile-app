import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

// dependencies npm
import {Dropdown} from 'react-native-element-dropdown';

// icon import dependencies
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

// Custom Imports
import CText from '../../../../common/CText';

// redux
import useBoolean from '../../../../../contexts/useBoolean';
import useService from '../../../../../contexts/useService';
import {colors, commonColor, styles} from '../../../../../themes';
import {moderateScale} from '../../../../../common/constants';
import {useSelector} from 'react-redux';

const SelectLocation = ({onDataReceived, isbuttonBoolean, onRadioData}) => {
  // onFocus change color boolean
  const [isFocus, setIsFocus] = useState(false);
  const colors = useSelector(state => state.theme.theme);

  // render selected value
  const [value, setValue] = useState(null);
  const [districtValue, setDistrictValue] = useState(null);
  const [khorooValue, setKhorooValue] = useState(null);

  //render select dropdown boolean
  const districtState = useBoolean();
  const districtKhoroo = useBoolean();
  const loadingState = useBoolean();
  const confirmBoolean = useBoolean();

  // fetch data
  const {data: aimagData, fetchData: fetchAimag, isLoading} = useService();
  const {
    data: districtData,
    fetchData: fetchDistrict,
    isLoading: districtLoading,
  } = useService();
  const {
    data: khorooData,
    fetchData: fetchKhoroo,
    isLoading: khorooLoading,
  } = useService();

  // Call service
  useEffect(() => {
    fetchAimag('POST', '/get_aimag_list', false, {});
  }, []);
  const reOnPress = () => {
    confirmBoolean.setFalse();
  };
  const lastSelect = item => {
    setKhorooValue(item);
    setIsFocus(false);
    confirmBoolean.setTrue();
    onDataReceived({
      khoroo_id: item,
      district_id: districtValue,
      aimag_id: value,
    });
  };
  // handle on district item call service
  const handleDistrict = async id => {
    loadingState.setTrue();
    await fetchDistrict('POST', '/get_district_list', false, {
      filter: [
        {
          field_name: 'aimag_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  // handle on Khoroo item call service
  const handleKhoroo = async id => {
    loadingState.setTrue();
    await fetchKhoroo('POST', '/get_khoroo_list', false, {
      filter: [
        {
          field_name: 'district_id',
          operation: '=',
          value: `${id}`,
        },
      ],
    });
  };

  // render data on dropDown
  const renderItem = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.aimag_name}</CText>
        {item.id === value?.id && (
          <AntDesign
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderDistrict = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.district_name}</CText>
        {item.id === districtValue?.id && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderKhoroo = item => {
    return (
      <View style={localStyles.item}>
        <CText style={localStyles.textItem}>{item.khoroo_name}</CText>
        {item.id === khorooValue?.id && (
          <AntDesign
            style={localStyles.icon}
            color={commonColor.carelyLogoColor}
            name="checkcircleo"
            size={20}
          />
        )}
      </View>
    );
  };

  // Render on screen items -------------------------------------------
  return (
    <View>
      {confirmBoolean.value === false ? (
        <View>
          {isLoading !== true ? (
            <Dropdown
              style={[
                localStyles.dropdown,
                isFocus && {borderColor: commonColor.carelyLogoColor},
                {backgroundColor: colors.input2},
              ]}
              placeholderStyle={[
                localStyles.placeholderStyle,
                {
                  color: colors.textColor,
                },
              ]}
              selectedTextStyle={[
                localStyles.selectedTextStyle,
                {
                  color: colors.textColor,
                },
              ]}
              inputSearchStyle={localStyles.inputSearchStyle}
              iconStyle={localStyles.iconStyle}
              data={aimagData?.data}
              search
              containerStyle={[
                localStyles.dropdownstyle,
                {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
              ]}
              maxHeight={300}
              labelField="aimag_name"
              valueField="id"
              placeholder={!isFocus ? 'Аймаг, хот сонгох' : '...'}
              searchPlaceholder="Хайх..."
              activeColor={colors.dark3}
              value={value}
              onFocus={() => setIsFocus(true)}
              onBlur={() => setIsFocus(false)}
              onChange={item => {
                setValue(item);
                handleDistrict(item?.id);
                setIsFocus(false);
                districtState.setTrue();
                districtKhoroo.setFalse();
              }}
              renderLeftIcon={() => (
                <EvilIcons
                  style={{...styles.mr10}}
                  color={commonColor.carelyLogoColor}
                  name="location"
                  size={moderateScale(25)}
                />
              )}
              renderItem={renderItem}
            />
          ) : null}
          {districtState.value === true && districtLoading !== true ? (
            <Dropdown
              style={[
                localStyles.dropdown1,
                isFocus && {borderColor: commonColor.carelyLogoColor},
                {backgroundColor: colors.input2},
              ]}
              placeholderStyle={[
                localStyles.placeholderStyle,
                {
                  color: colors.textColor,
                },
              ]}
              selectedTextStyle={[
                localStyles.selectedTextStyle,
                {
                  color: colors.textColor,
                },
              ]}
              inputSearchStyle={localStyles.inputSearchStyle}
              iconStyle={localStyles.iconStyle}
              data={districtData?.data}
              search
              containerStyle={[
                localStyles.dropdownstyle,
                {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
              ]}
              maxHeight={300}
              activeColor={colors.dark3}
              labelField="district_name"
              valueField="id"
              placeholder={!isFocus ? 'Сум, дүүрэг сонгох' : '...'}
              searchPlaceholder="Хайх..."
              value={districtValue}
              onFocus={() => setIsFocus(true)}
              onBlur={() => setIsFocus(false)}
              onChange={item => {
                setDistrictValue(item);
                handleKhoroo(item.id);
                setIsFocus(false);
                districtKhoroo.setTrue();
              }}
              renderLeftIcon={() => (
                <EvilIcons
                  style={{...styles.mr10}}
                  color={commonColor.carelyLogoColor}
                  name="location"
                  size={moderateScale(25)}
                />
              )}
              renderItem={renderDistrict}
            />
          ) : null}
          {districtKhoroo.value === true && khorooLoading !== true ? (
            <Dropdown
              style={[
                localStyles.dropdownLast,
                isFocus && {borderColor: commonColor.carelyLogoColor},
                {backgroundColor: colors.input2},
              ]}
              placeholderStyle={[
                localStyles.placeholderStyle,
                {
                  color: colors.textColor,
                },
              ]}
              selectedTextStyle={[
                localStyles.selectedTextStyle,
                {
                  color: colors.textColor,
                },
              ]}
              inputSearchStyle={localStyles.inputSearchStyle}
              iconStyle={localStyles.iconStyle}
              data={khorooData?.data}
              search
              activeColor={colors.dark3}
              maxHeight={300}
              containerStyle={[
                localStyles.dropdownstyle,
                {backgroundColor: colors.dark ? colors.dark2 : colors.input2},
              ]}
              labelField="khoroo_name"
              valueField="id"
              placeholder={!isFocus ? 'Баг, Хороо сонгох' : '...'}
              searchPlaceholder="Хайх..."
              value={khorooValue}
              onFocus={() => setIsFocus(true)}
              onBlur={() => setIsFocus(false)}
              onChange={item => {
                lastSelect(item);
              }}
              renderLeftIcon={() => (
                <EvilIcons
                  style={{...styles.mr10}}
                  color={commonColor.carelyLogoColor}
                  name="location"
                  size={moderateScale(25)}
                />
              )}
              renderItem={renderKhoroo}
            />
          ) : null}
        </View>
      ) : (
        <TouchableOpacity
          onPress={reOnPress}
          style={[
            localStyles.confirmed,
            {
              backgroundColor: colors.input2,
            },
          ]}>
          <View style={styles.rowStart}>
            <EvilIcons
              color={commonColor.carelyLogoColor}
              name="location"
              style={styles.mr5}
              size={moderateScale(25)}
            />
            <CText align={'left'} type={'m16'}>
              {value?.aimag_name}, {districtValue?.district_name}
            </CText>
          </View>
          <EvilIcons
            color={commonColor.carelyLogoColor}
            name="refresh"
            size={moderateScale(30)}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};
export default SelectLocation;

const localStyles = StyleSheet.create({
  dropdown: {
    height: moderateScale(60),
    borderRadius: moderateScale(12),
    ...styles.mb25,
    ...styles.ph10,
  },
  dropdown1: {
    height: moderateScale(60),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.mb25,
  },
  confirmed: {
    height: moderateScale(60),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.rowSpaceBetween,
    ...styles.mb25,
  },
  dropdownLast: {
    height: moderateScale(60),
    borderRadius: moderateScale(12),
    ...styles.ph10,
    ...styles.mb25,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  dropdownstyle: {
    borderRadius: moderateScale(10),
    borderWidth: 1,
    borderColor: commonColor.carelyLogoColor,
  },
});
