import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, View, FlatList} from 'react-native';

// icon import dependencies
import EvilIcons from 'react-native-vector-icons/EvilIcons';

// Custom Imports
import CText from '../../../../common/CText';

// redux
import {commonColor, styles} from '../../../../../themes';
import {moderateScale} from '../../../../../common/constants';
import {useSelector} from 'react-redux';

const SelectChildCategory = ({child, onDataReceived}) => {
  // onFocus change color boolean
  const [categories, setCategories] = useState(child?.ChildRef);
  const [name, setName] = useState([]);
  const colors = useSelector(state => state.theme.theme);

  const onSelect = value => {
    onDataReceived(value);
    setCategories(value?.ChildRef);
    setName([...name, value]);
  };
  const onPressChips = value => {
    const selectedIndex = name.indexOf(value);
    if (selectedIndex !== -1) {
      const updatedName = name.slice(0, selectedIndex + 1);
      setName(updatedName);
      setCategories(value?.ChildRef);
    }
  };
  const onSelectedCategory = () => {
    setCategories(child?.ChildRef);
    setName([]);
    onDataReceived(child);
  };
  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        key={index}
        onPress={() => onPressChips(item)}
        style={styles.rowStart}>
        <EvilIcons
          color={commonColor.grayScale5}
          name="chevron-right"
          size={moderateScale(25)}
        />
        <CText
          color={index === name.length - 1 ? commonColor.carelyLogoColor : null}
          type={'m15'}>
          {item?.name}
        </CText>
      </TouchableOpacity>
    );
  };
  // Render on screen items -------------------------------------------
  return (
    <View style={styles.mb15}>
      <View style={[styles.flex, styles.mt10]}>
        <View style={styles.rowStart}>
          <EvilIcons
            color={commonColor.grayScale5}
            name="chevron-right"
            size={moderateScale(25)}
          />
          <CText
            color={name.length === 0 ? commonColor.carelyLogoColor : null}
            onPress={onSelectedCategory}
            type={'m15'}>
            {child?.name}
          </CText>
        </View>
        <FlatList
          data={name}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsHorizontalScrollIndicator={false}
        />
      </View>

      <FlatList
        data={categories}
        renderItem={({item}) => (
          <View style={[styles.flexRow, styles.mt5]}>
            <TouchableOpacity
              onPress={() => onSelect(item)}
              style={[
                localStyles.dropdown,
                {
                  backgroundColor: colors.input2,
                },
              ]}>
              <CText type={'m14'} style={[styles.mv5, styles.ph5]}>
                {item?.name}
              </CText>
            </TouchableOpacity>
          </View>
        )}
        style={[styles.mt10]}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        // horizontal
      />
    </View>
  );
};
export default SelectChildCategory;

const localStyles = StyleSheet.create({
  dropdown: {
    borderRadius: moderateScale(10),
    ...styles.mr5,
  },
});
