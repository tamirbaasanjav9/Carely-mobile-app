import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {StackNav} from '../../../../../navigation/NavigationKeys';
import {Search_Dark, Search_Light} from '../../../../../assets/svgs';
import CSafeAreaView from '../../../../common/CSafeAreaView';
import CHeader from '../../../../common/CHeader';
import CButton from '../../../../common/CButton';
import {styles} from '../../../../../themes';
import CText from '../../../../common/CText';
import PaymentSelect from '../../../PaymentSelect';
import {AuthContext} from '../../../../../contexts/authContext';

export default function Responsibilities({navigation, route}) {
  console.log(route);
  const {ref_id, total, isSelected} = route?.params;
  const colors = useSelector(state => state.theme.theme);
  const [data, setData] = useState();
  //-------------------selecting qualification's value
  const [selectRes, setIsSelected] = useState([]);
  console.log(selectRes);

  const onSelectItems = value => {
    if (selectRes.includes(value)) {
      // If the value is already in the array, remove it
      setIsSelected(prevSelectedData =>
        prevSelectedData.filter(item => item !== value),
      );
    } else {
      // If the value is not in the array, add it
      setIsSelected(prevSelectedData => [...prevSelectedData, value]);
    }
  };
  //fetch data qualification's values from api
  const {
    handlers: {GETPROD},
  } = useContext(AuthContext);
  useEffect(() => {
    getQualificate();
  }, []);
  const getQualificate = async () => {
    try {
      await GETPROD(`/get_ref_responsibility_list/${ref_id}`)
        .then(res => {
          setData(res?.data);
        })
        .catch(err => {
          return err;
        });
    } catch (error) {
      return;
    }
  };
  // onpress buttons
  const onPressAdd = () => {
    navigation.navigate(StackNav.PostingReview, {
      data: {
        total,
        selectRes,
      },
    });
  };
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  // top header icon
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  // render header section
  const renderHeader = () => {
    return (
      <View>
        <CText type={'b20'} align={'left'} style={styles.mv25}>
          {'Хариуцлага'}
        </CText>
      </View>
    );
  };

  const renderPaymentItem = ({item, index}) => {
    const {Responsibility} = item;

    return (
      <PaymentSelect
        item={Responsibility}
        isSelected={selectRes.includes(Responsibility)}
        onPressItem={() => onSelectItems(Responsibility)}
      />
    );
  };

  return (
    <CSafeAreaView>
      <CHeader rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlatList
          data={data}
          renderItem={renderPaymentItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={renderHeader}
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={styles.ph20}>
        <CButton
          title={'Хүүхэд хадгалах'}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
});
