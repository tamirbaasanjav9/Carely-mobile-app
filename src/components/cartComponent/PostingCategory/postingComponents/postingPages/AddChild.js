import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

import CText from '../../../../common/CText';
import {commonColor, styles} from '../../../../../themes';
import {moderateScale} from '../../../../../common/constants';

const AddChild = ({onPressAddChild, plusedChild}) => {
  return (
    <View>
      {plusedChild.map((item, index) => (
        <View key={index} onPress={onPressAddChild} style={[localStyles.root]}>
          <AntDesign
            name="user"
            size={moderateScale(22)}
            color={commonColor.grayScale5}
            style={styles.mr10}
          />
          <CText type={'m16'}>{item?.nameChild}</CText>
          <CText type={'s5'} style={{...styles.mh15}}>
            {'\u2B24'}
          </CText>
          <CText type={'m16'}>{item?.ageRange?.title}</CText>
        </View>
      ))}
      <TouchableOpacity onPress={onPressAddChild} style={[localStyles.root]}>
        <AntDesign
          name="adduser"
          size={moderateScale(22)}
          color={commonColor.carelyLogoColor}
          style={styles.mr10}
        />
        <CText
          type={'m16'}
          style={[styles.flex]}
          color={commonColor.carelyLogoColor}>
          Хүүхэд нэмэх
        </CText>
      </TouchableOpacity>
    </View>
  );
};

export default AddChild;

const localStyles = StyleSheet.create({
  root: {
    ...styles.rowStart,
    ...styles.mv15,
  },
});
