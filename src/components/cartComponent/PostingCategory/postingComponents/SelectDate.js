import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import CText from '../../../common/CText';
import {styles} from '../../../../themes';
import {moderateScale} from '../../../../common/constants';
import useService from '../../../../contexts/useService';
import {FlatList} from 'react-native';

export default function SelectDate({onDateUpdate}) {
  const colors = useSelector(state => state.theme.theme);
  const [selectedChips, setSelectedChips] = useState([]);
  const {data, fetchData} = useService(); // call service custom
  useEffect(() => {
    fetchData('GET', `/get_const_config_list_fordropdown/9`, true);
  }, []);
  const onPressChips = value => {
    if (selectedChips.includes(value)) {
      setSelectedChips(selectedChips.filter(item => item !== value));
      onDateUpdate(value);
    } else {
      onDateUpdate(value);
      setSelectedChips([...selectedChips, value]);
    }
  };

  return (
    <View style={localStyles.container}>
      <FlatList
        data={data}
        renderItem={({item}) => (
          <TouchableOpacity
            key={item.id}
            onPress={() => onPressChips(item)}
            style={[
              localStyles.chipsContainer,
              {
                backgroundColor: selectedChips.includes(item)
                  ? colors.dark
                    ? colors.carelyLogoColor
                    : colors.carelyLogoColor
                  : colors.input2,
              },
            ]}>
            <CText
              type={'m13'}
              color={
                selectedChips.includes(item)
                  ? colors.dark
                    ? colors.white
                    : colors.white
                  : colors.dark
                  ? colors.white
                  : colors.textcolors
              }>
              {item.const_value}
            </CText>
          </TouchableOpacity>
        )}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    ...styles.pb5,
    ...styles.pt15,
    ...styles.rowSpaceBetween,
  },
  chipsContainer: {
    ...styles.ph10,
    ...styles.pv10,
    borderRadius: moderateScale(10),
    ...styles.rowCenter,
    ...styles.mr5,
  },
});
