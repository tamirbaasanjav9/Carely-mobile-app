import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
//custom imports
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {
  deviceWidth,
  getHeight,
  moderateScale,
} from '../../../../common/constants';
import {styles} from '../../../../themes';
import CText from '../../../common/CText';
import {useSelector} from 'react-redux';
import useBoolean from '../../../../contexts/useBoolean';

const TimePicker = ({onClock, timePlaceHolder, endtimePlaceHolder}) => {
  const colors = useSelector(state => state.theme.theme);
  const startBoolean = useBoolean();
  const endBoolean = useBoolean();
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  // start time
  const showTimePicker = () => {
    startBoolean.setTrue();
  };

  const hideTimePicker = () => {
    startBoolean.setFalse();
  };
  const handleTimeConfirm = start => {
    setStartTime(start);
    hideTimePicker();
  };

  // end time
  const showTimePickerSecond = () => {
    endBoolean.setTrue();
  };

  const hideTimePickerSecond = () => {
    endBoolean.setFalse();
  };

  const handleTimeConfirmSecond = end => {
    setEndTime(end);
    hideTimePickerSecond();
    onClock({startTime, end});
  };
  return (
    <View style={styles.rowSpaceBetween}>
      <View style={styles.mr10}>
        <TouchableOpacity
          onPress={showTimePicker}
          style={[
            localStyles.dobStyle,
            {
              borderColor: colors.bColor,
              backgroundColor: colors.input2,
            },
          ]}>
          <Ionicons
            name="time"
            size={moderateScale(20)}
            color={colors.grayScale5}
            style={styles.mr10}
          />
          <CText
            type={'r16'}
            color={startTime ? colors.textColor : colors.grayScale5}>
            {startTime
              ? startTime.toLocaleTimeString([], {
                  hour: '2-digit',
                  minute: '2-digit',
                })
              : timePlaceHolder}
          </CText>
        </TouchableOpacity>
        <DateTimePickerModal
          isVisible={startBoolean.value}
          mode="time"
          onConfirm={handleTimeConfirm}
          onCancel={handleTimeConfirm}
          minuteInterval={30}
        />
      </View>
      <View>
        <TouchableOpacity
          onPress={showTimePickerSecond}
          style={[
            localStyles.dobStyle,
            {
              borderColor: colors.bColor,
              backgroundColor: colors.input2,
            },
          ]}>
          <Ionicons
            name="time"
            size={moderateScale(20)}
            color={colors.grayScale5}
            style={styles.mr10}
          />
          <CText
            type={'r16'}
            color={endTime ? colors.textColor : colors.grayScale5}>
            {endTime
              ? endTime.toLocaleTimeString([], {
                  hour: '2-digit',
                  minute: '2-digit',
                })
              : endtimePlaceHolder}
          </CText>
        </TouchableOpacity>
        <DateTimePickerModal
          isVisible={endBoolean.value}
          mode="time"
          onConfirm={handleTimeConfirmSecond}
          onCancel={handleTimeConfirmSecond}
          minuteInterval={30}
        />
      </View>
    </View>
  );
};

export default TimePicker;

const localStyles = StyleSheet.create({
  dobStyle: {
    height: getHeight(45),
    borderRadius: moderateScale(12),
    borderWidth: moderateScale(1),
    ...styles.mv10,
    ...styles.rowCenter,
    ...styles.ph10,
  },
});
