import {StyleSheet, Switch, View} from 'react-native';
import React from 'react';
import CText from '../../../common/CText';
import {useSelector} from 'react-redux';
import {commonColor, styles} from '../../../../themes';

const CSwitch = ({isEnabled, onPressSwitch, value, ...props}) => {
  const colors = useSelector(state => state.theme.theme);

  return (
    <View style={localStyles.mainContainer}>
      <CText type={'m16'}>{value}</CText>
      <Switch
        trackColor={{
          false: colors.grayScale3,
          true: commonColor.carelyLogoColor,
        }}
        thumbColor={colors.white}
        onValueChange={onPressSwitch}
        value={isEnabled}
      />
    </View>
  );
};

export default CSwitch;

const localStyles = StyleSheet.create({
  mainContainer: {
    ...styles.rowSpaceBetween,
    ...styles.mv10,
  },
});
