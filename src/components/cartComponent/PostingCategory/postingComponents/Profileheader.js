import {Image, StyleSheet, View} from 'react-native';
import React, {useState, useContext, useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {useSelector} from 'react-redux';

//custom imports
import {AuthContext} from '../../../../contexts/authContext';
import images from '../../../../assets/images';
import {getUserId} from '../../../../utils/asyncstorage';
import CText from '../../../common/CText';
import {commonColor, styles} from '../../../../themes';
import {moderateScale} from '../../../../common/constants';
import {EditDark, EditLight} from '../../../../assets/svgs';
import {ImageBackground} from 'react-native';

const Profileheader = ({image}) => {
  console.log(image);
  const [user, setUser] = useState();
  const colors = useSelector(state => state.theme.theme);
  const {
    handlers: {GETUSER},
  } = useContext(AuthContext);
  useFocusEffect(
    useCallback(() => {
      if (getCustomer) {
        getUserId()
          .then(id => {
            getCustomer(id);
          })
          .catch(error => {
            return;
          });
      }
    }, [getCustomer]),
  );
  const getCustomer = async id => {
    try {
      const userId = id;
      await GETUSER(`/get_customer/${userId}`, true).then(res => {
        setUser(res?.data);
      });
    } catch (e) {
      return;
    }
  };
  return (
    <ImageBackground
      source={{uri: `http://192.168.2.2:3093/${image}`}}
      style={localStyles.backgroundImage}>
      <View style={[styles.selfCenter, {top: moderateScale(60)}]}>
        <Image
          source={colors.dark ? images.userDark : images.userLight}
          style={localStyles.userImage}
        />
        <CText align={'center'} style={[styles.flex]} type={'b17'}>
          {user?.name}
        </CText>
      </View>
    </ImageBackground>
  );
};

export default Profileheader;

const localStyles = StyleSheet.create({
  userImage: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(50),
    borderWidth: moderateScale(2),
    borderColor: commonColor.carelyLogoColor,
  },
  editIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch' or 'contain'
    ...styles.mb55,
  },
});
