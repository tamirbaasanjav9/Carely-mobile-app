import {StyleSheet, View, FlatList, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import CText from '../../../common/CText';

// others
import {styles} from '../../../../themes';
import useService from '../../../../contexts/useService';
import {moderateScale} from '../../../../common/constants';
import {useSelector} from 'react-redux';

const Subjects = ({filter_id, onSubject}) => {
  const colors = useSelector(state => state.theme.theme);

  const {data, fetchData} = useService(); // call service custom
  const [selectedChips, setSelectedChips] = useState([]);
  const onPressChips = value => {
    if (selectedChips.includes(value)) {
      setSelectedChips(selectedChips.filter(item => item !== value));
      onSubject(value?.subject_id);
    } else {
      setSelectedChips([...selectedChips, value]);
      onSubject(value?.subject_id);
    }
  };
  useEffect(() => {
    fetchData('GET', `/get_filter_list_fordropdown/${filter_id}`, false);
  }, [filter_id]); // Empty dependency array means it runs once when the component mounts

  const renderChild = ({item, index}) => {
    const {option_name} = item;
    return (
      <TouchableOpacity
        onPress={() => onPressChips(item)}
        style={[
          localStyle.child,
          {
            backgroundColor: selectedChips.includes(item)
              ? colors.dark
                ? colors.carelyLogoColor
                : colors.carelyLogoColor
              : colors.input2,
          },
        ]}
        key={index}>
        <CText type={'m13'} style={styles.ph10}>
          {option_name}
        </CText>
      </TouchableOpacity>
    );
  };
  const renderSubject = ({item, index}) => {
    const {filter_name, FilterOption} = item;
    return (
      <View key={index} style={styles.pv5}>
        <CText type={'M16'}>{filter_name}</CText>
        <FlatList
          data={FilterOption}
          renderItem={renderChild}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          numColumns={2}
          columnWrapperStyle
        />
      </View>
    );
  };
  return (
    <View style={[localStyle.root]}>
      <FlatList
        data={data}
        renderItem={renderSubject}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

export default Subjects;

const localStyle = StyleSheet.create({
  root: {
    ...styles.flex,
  },
  child: {
    ...styles.pv10,
    ...styles.mt10,
    ...styles.mr5,
    borderRadius: moderateScale(10),
  },
});
