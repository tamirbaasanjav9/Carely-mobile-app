import {StyleSheet, TouchableOpacity, View, FlatList} from 'react-native';
import React, {useState} from 'react';
import {StackNav} from '../../../navigation/NavigationKeys';

// Custom Imports
import CSafeAreaView from '../../common/CSafeAreaView';
import CHeader from '../../common/CHeader';
import CSwitch from './postingComponents/CSwitch';
import CButton from '../../common/CButton';
import SubHeader from '../../SubHeader';

// main components
import SelectChildCategory from './postingComponents/postingPages/SelectChildCategory';
import SelectLocation from './postingComponents/postingPages/SelectLocation';
import DateContainer from './postingComponents/DateContainer';
import SelectDate from './postingComponents/SelectDate';
import ClassInputs from './ClassInputs';
import TimePicker from './postingComponents/TimePicker';
import Quantity from './postingComponents/Quantity';

// reducers &&
import useBoolean from '../../../contexts/useBoolean';
import {useSelector} from 'react-redux';

// styling and images
import {styles} from '../../../themes';
import strings from '../../../i18n/strings';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import Grade from './postingComponents/postingPages/Grade';
import CDivider from '../../common/CDivider';
import CText from '../../common/CText';
import {getHeight, moderateScale} from '../../../common/constants';

export default function PostFourthCate({navigation, route}) {
  const {ref} = route?.params;
  const colors = useSelector(state => state.theme.theme);
  const enableBoolean = useBoolean();
  const [receivedData, setReceivedData] = useState({
    refId: ref?.id,
    title: '',
    description: '',
    locationIds: null,
    startDate: null,
    endDate: null,
    Schedules: [],
    experience: 0,
    image: ref?.image,
    address: '',
    grade: null,
    selectedDay: [],
  });
  console.log(receivedData.selectedDay);
  console.log(receivedData.Schedules);

  // toggle has meal ... pass next screen to boolean value
  const onPressSwitch = () => enableBoolean.toggle();

  // received from location ids --------- function khoroo, aimag and district id
  const onLocationSelect = value => {
    setReceivedData(prevData => ({
      ...prevData,
      locationIds: value, // Assuming locationIds is an array
    }));
  };

  // received start && end date from child component
  const onStartDate = value => {
    setReceivedData(prevData => ({
      ...prevData,
      startDate: value, // Assuming locationIds is an array
    }));
  };
  const onEndDate = value => {
    setReceivedData(prevData => ({
      ...prevData,
      endDate: value, // Assuming locationIds is an array
    }));
  };

  // class description and title received data from child component
  const onTitle = value => {
    setReceivedData(prevData => ({
      ...prevData,
      title: value, // Assuming locationIds is an array
    }));
  };
  const onAddress = value => {
    setReceivedData(prevData => ({
      ...prevData,
      address: value, // Assuming locationIds is an array
    }));
  };
  const onDescription = value => {
    setReceivedData(prevData => ({
      ...prevData,
      description: value, // Assuming locationIds is an array
    }));
  };

  //
  const onRefDataId = value => {
    setReceivedData(prevData => ({
      ...prevData,
      refId: value?.id, // Assuming locationIds is an array
    }));
  };

  const onDateUpdate = value => {
    setReceivedData(prevData => {
      const updatedSelectedDay = prevData.selectedDay.includes(value)
        ? prevData.selectedDay.filter(item => item !== value)
        : [...prevData.selectedDay, value];

      return {
        ...prevData,
        selectedDay: updatedSelectedDay,
      };
    });
  };
  const onStartTime = value => {
    console.log(value);
    setReceivedData(prevData => {
      const updatedDayIntervel = prevData.Schedules.includes(value)
        ? prevData.Schedules.filter(item => item !== value)
        : [...prevData.Schedules, value];

      return {
        ...prevData,
        Schedules: updatedDayIntervel,
      };
    });
  };
  const onExperience = value => {
    setReceivedData(prevData => ({
      ...prevData,
      experience: value, // Assuming locationIds is an array
    }));
  };
  const onGrade = value => {
    setReceivedData(prevData => ({
      ...prevData,
      grade: value, // Assuming locationIds is an array
    }));
  };
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };
  const onPressAdd = () => {
    navigation.navigate(StackNav.HourlyRate, {
      receivedData,
    });
  };
  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader title={ref?.name} rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          bounces={false}
          numColumns={2}
          contentContainerStyle={localStyles.contentContainerStyle}
          estimatedItemSize={20}
          ListHeaderComponent={
            <View style={styles.mb30}>
              <SubHeader type={'m16'} title1={'Байршил сонгох'} />
              <ClassInputs
                onReceivedDescription={onDescription}
                onReceivedTitle={onAddress}
                placeHolder={'Хаяг, байршил '}
              />
              <SelectLocation onDataReceived={onLocationSelect} />

              <CDivider />

              <SubHeader type={'m16'} title1={'Дэд ангилалуудаас сонгох'} />
              <SelectChildCategory child={ref} onDataReceived={onRefDataId} />
              <CDivider />
              <SubHeader type={'m16'} title1={'Огноо сонгох'} />
              <View style={styles.rowSpaceBetween}>
                <DateContainer
                  onDateDay={onStartDate}
                  placeHolder={strings.dob}
                />
                <DateContainer
                  onDateDay={onEndDate}
                  placeHolder={strings.endDate}
                />
              </View>
              <SelectDate onDateUpdate={onDateUpdate} />
              {receivedData?.selectedDay &&
                receivedData?.selectedDay.map((day, index) => (
                  <View key={index} style={styles.rowSpaceBetween}>
                    <View
                      style={[
                        {backgroundColor: colors.input2},
                        localStyles.day,
                      ]}>
                      <CText type={'m15'}>{day?.const_value} </CText>
                    </View>
                    <TimePicker
                      onClock={timeInterval => onStartTime({day, timeInterval})}
                      timePlaceHolder={strings.startTime}
                      endtimePlaceHolder={strings.endTime}
                    />
                  </View>
                ))}

              <CDivider />
              <SubHeader type={'m16'} title1={'Анги сонгох'} />
              <Grade onRecievedData={onGrade} />
              <ClassInputs
                onReceivedDescription={onDescription}
                onReceivedTitle={onTitle}
                checkMultiple={true}
                placeHolder={'Ажлын нэр... '}
              />
              <Quantity onDuration={onExperience} title={'Туршлага'} />
              <CSwitch
                value={'Хоолтой эсэх'}
                onPressSwitch={onPressSwitch}
                isEnabled={enableBoolean.value}
              />
            </View>
          }
        />
      </View>
      <View style={styles.ph20}>
        <CButton
          title={strings.apply}
          type={'m16'}
          containerStyle={styles.mv10}
          onPress={onPressAdd}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh15,
    ...styles.flex,
  },
  day: {
    height: getHeight(45),
    borderRadius: moderateScale(12),
    ...styles.center,
    ...styles.ph10,
  },
});
