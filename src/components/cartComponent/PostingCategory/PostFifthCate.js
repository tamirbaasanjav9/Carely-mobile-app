import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useCallback, useMemo} from 'react';
import {useSelector} from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';

// Custom Imports
import images from '../../../assets/images';
import {StackNav} from '../../../navigation/NavigationKeys';
import SubHeader from '../../SubHeader';
import HomeBanner from '../../homeComponent/HomeBanner';
import {styles} from '../../../themes';
import CSafeAreaView from '../../common/CSafeAreaView';
import CHeader from '../../common/CHeader';
import {moderateScale} from '../../../common/constants';
import {Search_Dark, Search_Light} from '../../../assets/svgs';
import CText from '../../common/CText';

export default function PostFifthCate({navigation, route}) {
  const {ChildRef, name} = route?.params;
  console.log(ChildRef);
  const colors = useSelector(state => state.theme.theme);
  const bannerImage = useMemo(() => {
    return colors.dark ? images.swiperImageDark1 : images.swiperImageLight2;
  }, [colors]);
  const onPressSearch = () => {
    navigation.navigate(StackNav.Search);
  };

  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };

  const renderHeader = () => {
    const onPressSpecialOffer = useCallback(
      () => navigation.navigate(StackNav.SpecialOffers),
      [],
    );
    return (
      <View>
        <SubHeader
          title1={'Ангилал'}
          title2={strings.seeAll}
          onPressSeeAll={onPressSpecialOffer}
        />
      </View>
    );
  };
  const renderFooter = () => {
    return (
      <View style={[styles.pv10, styles.mt20]}>
        <HomeBanner image={bannerImage} />
      </View>
    );
  };
  const renderItem = ({item}) => {
    const {name, has_child} = item;
    const onPressCategoryChild = () =>
      navigation.navigate(
        has_child === true ? StackNav.ProductCategoryThird : StackNav.Search,
        {
          ChildData: item?.item,
        },
      );
    return (
      <TouchableOpacity
        onPress={onPressCategoryChild}
        style={[
          localStyles.contactUsContainer,
          {
            backgroundColor: colors.dark ? colors.dark2 : colors.white,
          },
        ]}>
        <CText type={'b15'}>{name}</CText>
        {has_child === true ? (
          <Feather
            name="chevron-right"
            size={moderateScale(20)}
            color={colors.textColor}
          />
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <CSafeAreaView>
      <CHeader title={name} rightIcon={<RightIcon />} />
      <View style={localStyles.root}>
        <FlatList
          data={ChildRef}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={renderHeader}
          ListFooterComponent={renderFooter}
        />
      </View>
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  contactUsContainer: {
    ...styles.mt20,
    ...styles.pv20,
    borderRadius: moderateScale(15),
    ...styles.flexRow,
    ...styles.ph10,
    ...styles.justifyBetween,
  },
});
