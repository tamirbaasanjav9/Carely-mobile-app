import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {styles} from '../../../themes';
import CText from '../../common/CText';
import {deviceWidth, moderateScale} from '../../../common/constants';
import {useSelector} from 'react-redux';
import CInput from '../../../components/common/CInput';

const ClassInputs = ({
  onReceivedTitle,
  onReceivedDescription,
  checkMultiple = false,
  placeHolder,
}) => {
  const colors = useSelector(state => state.theme.theme);
  const [textconfirm, setText] = useState({
    title: '',
    description: '',
    descriptionLength: 500,
    maxLength: 50,
  });

  const onChangedTitle = text => {
    if (text.length <= textconfirm?.maxLength) {
      setText(prevData => ({
        ...prevData,
        title: text, // Assuming locationIds is an array
      }));
      onReceivedTitle(text);
    }
  };
  const onChangedDescription = text => {
    if (text.length <= textconfirm.descriptionLength) {
      setText(prevData => ({
        ...prevData,
        description: text, // Assuming locationIds is an array
      }));
      onReceivedDescription(text);
    }
  };
  return (
    <View style={styles.mb20}>
      <CInput
        placeHolder={placeHolder}
        _value={textconfirm.title}
        toGetTextFieldValue={onChangedTitle}
        inputContainerStyle={[
          {
            backgroundColor: colors.dark
              ? colors.backgroundColor
              : colors.white,
          },
          localStyles.inputContainerStyle,
        ]}
      />
      <CText align={'right'} style={[styles.flex]} type={'r12'}>
        {textconfirm?.title.length}/{textconfirm.maxLength}
      </CText>
      {checkMultiple && (
        <View style={localStyles.productText}>
          <CInput
            placeHolder={'Дэлгэрэнгүй...'}
            _value={textconfirm?.description}
            multiline
            toGetTextFieldValue={onChangedDescription}
            inputContainerStyle={[
              {
                backgroundColor: colors.dark
                  ? colors.backgroundColor
                  : colors.white,
              },
              localStyles.inputContainerStyle,
            ]}
          />
          <CText align={'right'} style={[styles.flex]} type={'r12'}>
            {textconfirm?.description.length}/{textconfirm?.descriptionLength}
          </CText>
        </View>
      )}
    </View>
  );
};

export default ClassInputs;

const localStyles = StyleSheet.create({
  productText: {
    ...styles.mt2,
  },
  addToCartContainer: {
    width: deviceWidth / 2 - moderateScale(15),
    ...styles.shadowStyle,
    borderRadius: moderateScale(15),
  },
  inputContainerStyle: {
    borderWidth: 0,
    borderBottomWidth: 1,
  },
});
