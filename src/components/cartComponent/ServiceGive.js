// Library Imports
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {FlashList} from '@shopify/flash-list';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

// Custom Imports
import {styles} from '../../themes';
import {deviceWidth, moderateScale} from '../../common/constants';
import CText from '../common/CText';
import {StackNav} from '../../navigation/NavigationKeys';
import variables from '../../api/variables';

export default function ServiceGive(props) {
  const navigation = useNavigation();

  const ArrayItems = props?.data;
  const renderItem = ({item, index}) => {
    const {name, image, id} = item?.Ref;
    const onPressItem = () => {
      navigation.navigate(StackNav.PostFirstCate, {ref_id: id, name: name});
    };
    return (
      <TouchableOpacity
        key={index}
        onPress={onPressItem}
        style={localStyles.categoryRoot}>
        <View style={[localStyles.iconContainer]}>
          <Image
            source={{uri: `${variables.GENERAL_IMAGE_URL}/${image}`}}
            style={[localStyles.productImageStyle]}
          />
          <View style={localStyles.containerIn}>
            <Feather name="thumbs-up" size={moderateScale(25)} color={'#fff'} />
            <CText
              type="b14"
              numberOfLines={2}
              align={'center'}
              color={'#ffffff'}
              style={[styles.mh10]}>
              {name}
            </CText>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={localStyles.root}>
      <FlashList
        data={ArrayItems}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        bounces={false}
        contentContainerStyle={localStyles.contentContainerStyle}
        estimatedItemSize={20}
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.flex,
    ...styles.ph10,
  },
  contentContainerStyle: {
    ...styles.pb20,
  },
  categoryRoot: {
    width: '100%',
    ...styles.itemsCenter,
    ...styles.mt20,
  },
  productImageStyle: {
    width: deviceWidth - moderateScale(40),
    height: moderateScale(170),
    borderRadius: moderateScale(35),
    ...styles.selfCenter,
    resizeMode: 'cover',
    // opacity: 0.5,
  },
  iconContainer: {
    width: deviceWidth - moderateScale(40),
    height: moderateScale(170),
    borderRadius: moderateScale(35),
    ...styles.justifyEnd,
  },
  containerIn: {
    ...styles.ph20,
    ...styles.pv15,
    ...styles.rowStart,
    position: 'absolute',
    width: deviceWidth - moderateScale(40),
    borderBottomLeftRadius: moderateScale(35),
    borderBottomRightRadius: moderateScale(35),
    backgroundColor: 'rgba(50, 50, 50, 0.7)',
  },
});
