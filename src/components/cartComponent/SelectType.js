import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

// Custom Imports
import {styles} from '../../themes';
import {deviceWidth, moderateScale} from '../../common/constants';
import CText from '../common/CText';
import {StackNav} from '../../navigation/NavigationKeys';
import images from '../../assets/images';

export default function SelectType({item}) {
  console.log(item);
  const {sale_type_name, image} = item;
  const navigation = useNavigation();

  const onPressType = item => {
    // navigation.navigate(StackNav.SelectedTypeScreen, {selectedType: item});
  };
  return (
    <TouchableOpacity
      onPress={() => onPressType(item)}
      style={localStyles.categoryRoot}>
      <View style={[localStyles.iconContainer]}>
        <Image
          source={images.CareBack}
          // source={{uri: `http://192.168.2.2:3093/${image}`}}
          style={[localStyles.productImageStyle]}
        />
        <View style={localStyles.containerIn}>
          <CText
            type="b14"
            numberOfLines={2}
            align={'left'}
            color={'#ffffff'}
            style={[styles.mh10]}>
            {sale_type_name}
          </CText>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  categoryRoot: {
    width: '100%',
    ...styles.itemsCenter,
    ...styles.mt20,
  },
  productImageStyle: {
    width: deviceWidth - moderateScale(40),
    height: moderateScale(170),
    borderRadius: moderateScale(35),
    ...styles.selfCenter,
    resizeMode: 'cover',
  },
  iconContainer: {
    width: deviceWidth - moderateScale(40),
    height: moderateScale(170),
    borderRadius: moderateScale(35),
    ...styles.justifyEnd,
  },
  containerIn: {
    ...styles.ph20,
    ...styles.pv15,
    ...styles.rowStart,
    position: 'absolute',
    width: deviceWidth - moderateScale(40),
    borderBottomLeftRadius: moderateScale(35),
    borderBottomRightRadius: moderateScale(35),
    backgroundColor: 'rgba(50, 50, 50, 0.7)',
  },
});
