import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {memo} from 'react';
import {useSelector} from 'react-redux';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import SimilarShortDetail from './SimilarShortDetail';
import {useNavigation} from '@react-navigation/native';
import {StackNav} from '../../navigation/NavigationKeys';
import {useEffect} from 'react';
import {useState} from 'react';
import {AuthContext} from '../../contexts/authContext';
import BrandSimilarShort from './BrandSimilarShort';

const BrandSimilar = props => {
  const {brand_id} = props;
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const [product, setProduct] = useState();
  const {
    handlers: {POSTPRODUCT},
  } = useContext(AuthContext);

  // handle item function
  const onPressDetail = itm =>
    navigation.navigate(StackNav.ProductDetail, {item: itm});
  useEffect(() => {
    getProduct();
  }, [brand_id]);
  // call similar product from api
  const getProduct = async () => {
    try {
      await POSTPRODUCT('/get_active_product_list', false, {
        filter: [
          {
            field_name: 'brand_id',
            operation: '=',
            value: `${brand_id}` ? `${brand_id}` : null,
          },
        ],
      })
        .then(res => {
          setProduct(res?.data);
        })
        .catch(err => {
          return err;
        });
    } catch (error) {
      return;
    }
  };
  const renderSwiperItem = ({item, index}) => {
    return (
      <BrandSimilarShort
        item={item}
        index={index}
        onPress={() => onPressDetail(item)}
      />
    );
  };

  return (
    <SwiperFlatList
      data={product?.data}
      autoplay
      autoplayDelay={3}
      autoplayLoop
      renderItem={renderSwiperItem}
      paginationStyleItemActive={{
        ...localStyles.paginationStyleItemActive,
        backgroundColor: colors.dark ? colors.grayScale4 : colors.dark2,
      }}
      paginationStyleItemInactive={{
        ...localStyles.paginationStyleItemInactive,
        backgroundColor: colors.dark ? colors.dark2 : colors.grayScale4,
      }}
      paginationStyleItem={localStyles.paginationStyleItem}
      style={localStyles.swiperStyle}
    />
  );
};

const localStyles = StyleSheet.create({
  paginationStyleItem: {
    ...styles.mh5,
  },
  paginationStyleItemActive: {
    height: getHeight(6),
    width: moderateScale(16),
    borderRadius: moderateScale(3),
  },
  paginationStyleItemInactive: {
    height: moderateScale(6),
    width: moderateScale(6),
    borderRadius: moderateScale(3),
  },
  swiperItemContainer: {
    ...styles.mb10,
    width: deviceWidth - moderateScale(40),
    height: getHeight(160),
  },
  swiperStyle: {
    overflow: 'hidden',
  },
  swiperImageStyle: {
    width: deviceWidth - moderateScale(40),
    height: getHeight(160),
    borderRadius: moderateScale(20),
  },
});
export default memo(BrandSimilar);
