import {StyleSheet, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {styles} from '../../themes';
import Entypo from 'react-native-vector-icons/Entypo';
import {moderateScale} from '../../common/constants';
import CText from '../common/CText';
import {TouchableOpacity} from 'react-native';

const SortProduct = props => {
  const {item, isCompleted = false, onPressComplete, hasChildData} = props;
  const color = useSelector(state => state.theme.theme);
  const onPressBtn = () => {
    if (!isCompleted) {
      onPressComplete(item);
    } else null;
  };
  return (
    <View>
      <TouchableOpacity onPress={onPressBtn} style={localStyles.mainContainer}>
        <CText numberOfLines={1} type={'b14'}>
          Эрэмблэх
        </CText>
        <Entypo
          name="chevron-small-down"
          size={moderateScale(28)}
          color={color.dark ? color.white : color.darkColor}
        />
      </TouchableOpacity>
    </View>
  );
};

export default SortProduct;

const localStyles = StyleSheet.create({
  mainContainer: {
    ...styles.rowEnd,
  },
  btnContainer: {
    ...styles.rowSpaceBetween,
  },
});
