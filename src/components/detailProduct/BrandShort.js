// Library import
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';

// Local import
import React from 'react';
import {getHeight, moderateScale} from '../../common/constants';
import {commonColor, styles} from '../../themes';
import CText from '../common/CText';
import {memo} from 'react';

export default function BrandShort({
  userName,
  userImage,
  userDescription,
  brandCountDescription,
  shopDetail,
}) {
  const colors = useSelector(state => state.theme.theme);
  const ShopImage = memo(({item, index}) => {
    return (
      <View key={index} style={localStyles.categoryRoot}>
        {item !== null ? (
          <View
            style={[
              localStyles.iconContainer,
              {
                backgroundColor: colors.dark
                  ? colors.dark3
                  : colors.transparentSilver,
              },
            ]}>
            <Image
              source={{uri: `http://192.168.2.2:8010/${item}`}}
              style={[
                localStyles.productImageStyle,
                {
                  backgroundColor: colors.dark
                    ? colors.imageBg
                    : colors.grayScale1,
                },
              ]}
            />
          </View>
        ) : null}
      </View>
    );
  });
  return (
    <View
      style={[
        localStyles.rootContainer,
        {backgroundColor: colors.dark ? colors.dark2 : colors.grayScale1},
      ]}>
      <TouchableOpacity style={localStyles.userItem}>
        <Image
          source={{uri: `http://192.168.2.2:8033/${userImage}`}}
          style={localStyles.imageStyle}
        />
        <View style={localStyles.userDescription}>
          <CText type="b18" numberOfLines={1}>
            {userName}
          </CText>
          {!!userDescription && (
            <CText
              type="m14"
              style={styles.mt5}
              color={colors.dark ? colors.grayScale3 : colors.grayScale7}
              numberOfLines={1}>
              Нийт {userDescription} {brandCountDescription}
            </CText>
          )}
        </View>
      </TouchableOpacity>
      <FlatList
        data={shopDetail?.banners}
        renderItem={({item, index}) => <ShopImage item={item} index={index} />}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  rootContainer: {
    ...styles.mt15,
    ...styles.flex,
    ...styles.pv10,
    ...styles.ph10,
    borderRadius: moderateScale(10),
  },
  userItem: {
    flex: 1,
    ...styles.rowCenter,
  },
  imageStyle: {
    width: moderateScale(40),
    height: moderateScale(40),
    borderRadius: moderateScale(10),
    resizeMode: 'cover',
  },
  userDescription: {
    ...styles.mh10,
    ...styles.flex,
  },
  buttonContainer: {
    ...styles.ph15,
    height: getHeight(35),
    borderRadius: moderateScale(17),
    borderWidth: moderateScale(1),
  },
  categoryRoot: {
    width: '100%',
    ...styles.mt15,
    // height: getHeight(130),
  },
  iconContainer: {
    width: moderateScale(50),
    height: moderateScale(35),
    backgroundColor: commonColor.grayScale3,
    ...styles.center,
  },
  productImageStyle: {
    width: moderateScale(50),
    height: moderateScale(35),
    ...styles.selfCenter,
  },
});
