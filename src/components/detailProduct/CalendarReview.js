import {FlatList, StyleSheet, View} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
//custom imports
import {moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import CText from '../common/CText';

const CalendarReview = props => {
  // from main component passed data
  const {timeData, startDate, endDate} = props;
  console.log(props);
  const colors = useSelector(state => state.theme.theme);

  // set Time formater
  const dateStart = new Date(startDate);
  const dateEnd = new Date(endDate);
  const formattedDate = dateStart.toLocaleDateString();
  const formattedDateEnd = dateEnd.toLocaleDateString();
  // format time

  return (
    <View style={[localStyles.rightContainer]}>
      <View style={localStyles.subItemStyleTextStart}>
        <View style={[styles.mr25]}>
          <View style={[styles.rowStart]}>
            <Ionicons
              name="calendar-outline"
              size={moderateScale(18)}
              color={colors.grayScale5}
            />
            <CText style={styles.ml10} type={'b13'}>
              ЭХЛЭХ ХУГАЦАА{' '}
            </CText>
          </View>
          <View style={[styles.mt10, styles.rowEnd]}>
            <CText type={'b13'} style={{color: colors.carelyLogoColor}}>
              {formattedDate}
            </CText>
          </View>
        </View>
        <View>
          <View style={[styles.rowStart]}>
            <Ionicons
              name="calendar-outline"
              size={moderateScale(18)}
              color={colors.grayScale5}
            />
            <CText style={styles.ml10} type={'b13'}>
              ДУУСАХ ХУГАЦАА{' '}
            </CText>
          </View>
          <View style={[styles.mt10, styles.rowEnd]}>
            <CText type={'b13'} style={{color: colors.carelyLogoColor}}>
              {formattedDateEnd}
            </CText>
          </View>
        </View>
      </View>
      <View style={styles.rowSpaceBetween}>
        <View style={[styles.mv15]}>
          <View style={[styles.rowStart]}>
            <Ionicons
              name="time-outline"
              size={moderateScale(18)}
              color={colors.grayScale5}
            />
            <CText style={styles.ml10} type={'b13'}>
              ЦАГИЙН ХУВААРЬ
            </CText>
          </View>
          <FlatList
            data={timeData}
            renderItem={({item, index}) => {
              const schedule = new Date(item?.timeInterval?.startTime);
              const scheduleEnd = new Date(item?.timeInterval?.end);
              const formattedTime = schedule.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
              });
              const formattedTimeEnd = scheduleEnd.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
              });
              return (
                <View key={index} style={[styles.mt10]}>
                  <CText
                    type={'b13'}
                    style={[{color: colors.carelyLogoColor}, styles.selfEnd]}>
                    {item?.day?.const_value} {formattedTime} -{' '}
                    {formattedTimeEnd}
                  </CText>
                </View>
              );
            }}
            style={styles.alignStart}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    </View>
  );
};

export default CalendarReview;

const localStyles = StyleSheet.create({
  rightContainer: {
    ...styles.flex,
    ...styles.justifyBetween,
  },
  subItemStyleTextStart: {
    ...styles.rowSpaceBetween,
    ...styles.mt10,
  },
  circleDate: {
    ...styles.mr5,
    borderRadius: 10,
    ...styles.p5,
    flex: 1,
  },
});
