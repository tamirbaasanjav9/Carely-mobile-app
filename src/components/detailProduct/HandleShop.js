import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {memo} from 'react';
import {colors, commonColor, styles} from '../../themes';
import {moderateScale} from '../../common/constants';
import CText from '../common/CText';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {useSelector} from 'react-redux';
import {LikeWithBg, UnLikeWithBg} from '../../assets/svgs';
import {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HandleShop = ({shopDetial}) => {
  const colors = useSelector(state => state.theme.theme);
  const onPressLike = () => setIsLiked(!isLiked);
  const onPressShare = () => {
    setIsShare(!isShare);
  };
  const [isLiked, setIsLiked] = useState(false);
  const [isShare, setIsShare] = useState(false);

  return (
    <View style={[localStyles.ShopContainer]}>
      <TouchableOpacity
        style={[localStyles.shop]}
        // onPress={() => {
        //   has_child === true ? onPressCategoryChild(item) : onPressSearch();
        // }}
      >
        <Image
          source={{uri: `http://192.168.2.2:8010/${shopDetial?.images[0]}`}}
          style={localStyles.userImageStyle}
        />
        <CText type={'b16'} style={(styles.ph20, styles.flex)}>
          {shopDetial?.shop_name}
        </CText>
      </TouchableOpacity>
      <TouchableOpacity onPress={onPressShare}>
        <Ionicons
          name={'share-social-outline'}
          size={moderateScale(20)}
          color={colors.dark ? colors.white : colors.black}
          style={styles.mr15}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={onPressLike}>
        {isLiked ? (
          <LikeWithBg width={moderateScale(28)} height={moderateScale(28)} />
        ) : (
          <UnLikeWithBg width={moderateScale(28)} height={moderateScale(28)} />
        )}
      </TouchableOpacity>
    </View>
  );
};

export default HandleShop;

const localStyles = StyleSheet.create({
  ShopContainer: {
    ...styles.mt10,
    ...styles.flexRow,
    ...styles.flexCenter,
  },
  shop: {
    ...styles.flexRow,
    ...styles.flexCenter,
  },
  userImageStyle: {
    ...styles.mr8,
    width: moderateScale(35),
    height: moderateScale(35),
    borderRadius: moderateScale(20),
  },
});
