import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import CText from '../common/CText';
import {styles} from '../../themes';

function SubHeaderDetial({title1, title2, onPressSeeAll, style, titlePlus}) {
  return (
    <View style={[localStyles.root, {...style}]}>
      <CText type={'b15'} style={styles.flex}>
        "{title1}" {titlePlus}
      </CText>
      {!!title2 && (
        <TouchableOpacity onPress={onPressSeeAll}>
          <CText type={'M16'} style={styles.flex}>
            {title2}
          </CText>
        </TouchableOpacity>
      )}
    </View>
  );
}

export default React.memo(SubHeaderDetial);

const localStyles = StyleSheet.create({
  root: {
    ...styles.rowSpaceBetween,
    ...styles.mv15,
  },
});
