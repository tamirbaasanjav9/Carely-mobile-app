import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import {useContext} from 'react';
import {FlashList} from '@shopify/flash-list';
import {Search_Dark, Search_Light} from '../../assets/svgs';
import CSafeAreaView from '../common/CSafeAreaView';
import CHeader from '../common/CHeader';
import {styles} from '../../themes';
import {moderateScale} from '../../common/constants';
import {AuthContext} from '../../contexts/authContext';
import SubHeader from '../SubHeader';
import strings from '../../i18n/strings';
import images from '../../assets/images';
import CText from '../common/CText';
import {useState} from 'react';
import {SceneMap, TabView} from 'react-native-tab-view';
import OnGoing from '../../containers/tabbar/order/OnGoing';
import Completed from '../../containers/tabbar/order/Completed';
import {useEffect} from 'react';

export default function ShopDetail({navigation, route}) {
  const {shop_name, banners, id} = route?.params?.item;
  const colors = useSelector(state => state.theme.theme);
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState();
  console.log(data);

  const {
    handlers: {GET},
  } = useContext(AuthContext);
  const [isSelect, setIsSelect] = useState({
    index: 0,
    routes: [
      {key: 'first', title: strings.ongoing},
      {key: 'second', title: strings.completed},
    ],
  });
  const _handleIndexChange = index => {
    setIsSelect({...isSelect, index: index});
  };
  const _renderScene = SceneMap({
    first: OnGoing,
    second: Completed,
  });
  const _renderTabBar = props => {
    return (
      <View style={localStyles.mainContainer}>
        {props.navigationState.routes.map((item, index) => {
          return <HeaderCetegoryItem title={item.title} index={index} />;
        })}
      </View>
    );
  };
  // get shop detail from API
  useEffect(() => {
    getShopDetail();
  }, []);
  const getShopDetail = async () => {
    setLoading(true);
    try {
      await GET(`/get_shop/${id}`, false)
        .then(res => {
          setData(res?.data);
          setLoading(false);
        })
        .catch(err => {
          setLoading(false);
          console.log(err);
        });
    } catch (error) {
      setLoading(false);
      return;
    }
  };
  const HeaderCetegoryItem = ({title, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleIndexChange(index)}
        style={[
          localStyles.rootBar,
          {
            borderBottomColor:
              isSelect.index === index ? colors.textColor : colors.dark3,
          },
        ]}>
        <CText
          type={'b18'}
          align={'center'}
          style={styles.pb20}
          color={
            isSelect.index === index ? colors.textColor : colors.grayScale7
          }>
          {title}
        </CText>
      </TouchableOpacity>
    );
  };

  const onPressSearch = () => {
    // navigation.navigate(StackNav.Search, {id: sorted_id});
  };

  const RightIcon = () => {
    return (
      <TouchableOpacity onPress={onPressSearch}>
        {colors.dark ? <Search_Dark /> : <Search_Light />}
      </TouchableOpacity>
    );
  };
  const onPressDetail = itm =>
    navigation.navigate(StackNav.ProductDetail, {item: itm});
  //   const renderItem = ({item, index}) => {
  //     console.log(item);
  //     return (
  //       <View>
  //         {item !== 0 ? (
  //           <ProductShortDetail
  //             item={item}
  //             index={index}
  //             onPress={() => onPressDetail(item)}
  //           />
  //         ) : null}
  //       </View>
  //     );
  //   };

  const renderHeaderItem = () => {
    return (
      <View>
        <TouchableOpacity>
          <Image
            source={
              banners !== null
                ? {
                    uri: `http://192.168.2.2:8010/${
                      banners ? banners[0] : null
                    }`,
                  }
                : images.connectScreenImg
            }
            style={localStyles.creditCardImage}
          />
        </TouchableOpacity>
        <SubHeader
          title1={strings.transactionHistory}
          title2={strings.seeAll}
          // onPressSeeAll={onPressSeeAll}
          style={styles.ph20}
        />
      </View>
    );
  };
  return (
    <CSafeAreaView>
      <CHeader title={shop_name} rightIcon={<RightIcon />} />
      <View style={[styles.ph15]}>
        <Image
          source={
            data?.banners !== null
              ? {uri: `http://192.168.2.2:8010/${data?.banners[0]}`}
              : null
          }
          style={localStyles.creditCardImage}
        />
      </View>
      <View
        style={[
          localStyles.rootContainer,
          {backgroundColor: colors.dark ? colors.dark2 : colors.grayScale1},
        ]}>
        <TouchableOpacity style={localStyles.userItem}>
          <Image
            source={
              data?.images !== null
                ? {uri: `http://192.168.2.2:8010/${data?.images[0]}`}
                : null
            }
            style={localStyles.imageStyle}
          />
          <View style={localStyles.userDescription}>
            <CText type="b18" numberOfLines={1}>
              {data?.shop_name}
            </CText>
            <CText
              type="m14"
              style={styles.mt5}
              color={colors.dark ? colors.grayScale3 : colors.grayScale7}
              numberOfLines={1}>
              {data?.product_count}
            </CText>
          </View>
        </TouchableOpacity>
      </View>
      <TabView
        navigationState={isSelect}
        renderScene={_renderScene}
        renderTabBar={_renderTabBar}
        onIndexChange={_handleIndexChange}
        activeColor={{color: colors.primary}}
        navigation={navigation}
      />
    </CSafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  root: {
    ...styles.mh20,
    ...styles.flex,
  },
  contactUsContainer: {
    ...styles.mt20,
    ...styles.pv20,
    borderRadius: moderateScale(15),
    ...styles.flexRow,
    ...styles.flexCenter,
  },
  creditCardImage: {
    width: moderateScale(380),
    height: moderateScale(200),
    resizeMode: 'cover',
    ...styles.selfCenter,
  },
  mainContainer: {
    ...styles.rowSpaceBetween,
    ...styles.ph20,
    ...styles.mt10,
    width: '100%',
  },
  rootBar: {
    borderBottomWidth: moderateScale(2),
    width: '50%',
  },
  rootContainer: {
    ...styles.mt15,
    // ...styles.flex,
    ...styles.pv10,
    ...styles.ph10,
    borderRadius: moderateScale(10),
  },
  userItem: {
    ...styles.rowCenter,
  },
  imageStyle: {
    width: moderateScale(80),
    height: moderateScale(80),
    borderRadius: moderateScale(50),
    resizeMode: 'cover',
  },
  userDescription: {
    ...styles.mh10,
    ...styles.flex,
  },
});
