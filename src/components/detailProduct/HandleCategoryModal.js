// Library Imports
import {StyleSheet, TouchableOpacity, View, FlatList} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Custom Imports
import CText from '../common/CText';
import {styles} from '../../themes';
import {moderateScale} from '../../common/constants';
import strings from '../../i18n/strings';

const HandleCategoryModal = ({mappedData, onArrivedArray}) => {
  const colors = useSelector(state => state.theme.theme);
  const [selectedChips, setSelectedChips] = useState([]);
  const [isDescShow, setIsDescShow] = useState(true);
  const onPressChips = value => {
    if (selectedChips.includes(value)) {
      setSelectedChips(
        selectedChips.filter(item => item?.subject_id !== value?.subject_id),
      );
      onArrivedArray(value);
    } else {
      setSelectedChips([...selectedChips, value]);
      onArrivedArray(value);
    }
  };
  // Call category service

  const onPressShow = () => setIsDescShow(!isDescShow);
  // call api from ref tree

  const renderChips = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => onPressChips(item)}
        style={[
          localStyles.chipsContainer,
          {borderColor: colors.dark ? colors.dark3 : colors.textColor},
          {
            backgroundColor: selectedChips.includes(item)
              ? colors.dark
                ? colors.dark3
                : colors.textColor
              : colors.tranparent,
          },
        ]}>
        <CText
          type={'M13'}
          color={
            selectedChips.includes(item)
              ? colors.dark
                ? colors.white
                : colors.white
              : colors.dark
              ? colors.white
              : colors.textColor
          }>
          {item?.name ? item.name : item?.option_name}
        </CText>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.flex}>
      <TouchableOpacity
        style={[localStyles.helperContainer]}
        onPress={onPressShow}>
        <View style={localStyles.helperInnerContainer}>
          <CText type={'b14'} style={(styles.ph20, styles.flex)}>
            {mappedData?.filter_name
              ? mappedData?.filter_name
              : strings.category}
          </CText>
          {isDescShow ? (
            <Ionicons
              name="caret-down-outline"
              size={moderateScale(15)}
              color={colors.textColor}
              style={styles.mr5}
            />
          ) : (
            <Ionicons
              name="caret-forward-outline"
              size={moderateScale(15)}
              color={colors.textColor}
              style={styles.mr5}
            />
          )}
        </View>
      </TouchableOpacity>
      {!!isDescShow && (
        <View style={styles.mb15}>
          <FlatList
            data={mappedData?.FilterOption}
            renderItem={renderChips}
            keyExtractor={(item, index) => index.toString()}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
      )}
    </View>
  );
};

export default HandleCategoryModal;

const localStyles = StyleSheet.create({
  helperContainer: {
    ...styles.pv20,
    borderRadius: moderateScale(10),
  },
  helperInnerContainer: {
    ...styles.rowCenter,
  },
  categoryContainer: {
    ...styles.rowStart,
    ...styles.wrap,
    ...styles.mt5,
  },

  textContainer: {},
  paidContainer: {
    ...styles.mv5,
    ...styles.mr5,
    ...styles.ph10,
    ...styles.pv5,
    borderRadius: moderateScale(6),
  },
  helperDescription: {
    ...styles.rowStart,
  },
  chipsContainer: {
    ...styles.ph15,
    ...styles.pv10,
    ...styles.mr5,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(25),
    ...styles.rowCenter,
  },
});
