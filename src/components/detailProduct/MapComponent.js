import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import React, {useState} from 'react';
//custom import
import MapView, {Marker} from 'react-native-maps';
import {styles} from '../../themes';
import {
  deviceHeight,
  deviceWidth,
  getHeight,
  getWidth,
} from '../../common/constants';
import CText from '../common/CText';
import {useEffect} from 'react';
import images from '../../assets/images';

// map aspect ratio

const ASPECT_RATIO = deviceWidth / deviceHeight;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INITIAL_POSITION = {
  latitude: 47.8864,
  longitude: 106.9057,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

const MapComponent = () => {
  const [selectedLocation, setSelectedLocation] = useState(null);
  const [address, setAddress] = useState(null);
  useEffect(() => {
    if (selectedLocation) {
      reverseGeocode(selectedLocation.latitude, selectedLocation.longitude);
    }
  }, [selectedLocation]);
  const handleMapPress = event => {
    const {coordinate} = event.nativeEvent;
    setSelectedLocation(coordinate);
  };
  const reverseGeocode = async (latitude, longitude) => {
    try {
      const apiKey = 'YOUR_GOOGLE_MAPS_API_KEY';
      const response = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${apiKey}`,
      );

      if (!response.ok) {
        throw new Error('Geocoding request failed');
      }

      const data = await response.json();
      if (data.results && data.results.length > 0) {
        const formattedAddress = data.results[0].formatted_address;
        setAddress(formattedAddress);
      } else {
        setAddress(null);
      }
    } catch (error) {
      console.error('Reverse geocoding error:', error);
    }
  };
  return (
    <View style={localStyles.container}>
      <MapView
        onPress={handleMapPress}
        style={localStyles.map}
        initialRegion={INITIAL_POSITION}>
        <Marker
          coordinate={{
            latitude: 37.78825,
            longitude: -122.4324,
          }}
          title="Marker Title"
          description="Marker Description"
        />
        {selectedLocation && (
          <Marker
            coordinate={selectedLocation}
            title="Selected Location"
            // image={images.bag1}
            // pinColor="red"
            // style={{
            //   width: 0,
            //   height: 0,
            //   resizeMode: 'contain', // Adjust the resizeMode as needed
            // }}
          >
            {/* <View>
              <Image
                source={images.bag1}
                style={[localStyles.productImageStyle]}
              />
            </View> */}
          </Marker>
        )}
      </MapView>
      {selectedLocation && (
        <View style={localStyles.confirmButtonContainer}>
          <TouchableOpacity
            style={localStyles.confirmButton}
            onPress={() =>
              console.log(
                'Selected Location:',
                selectedLocation,
                'Address:',
                address,
              )
            }>
            <CText style={localStyles.confirmButtonText}>
              Confirm Location
            </CText>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default MapComponent;

const localStyles = StyleSheet.create({
  container: {
    width: '100%',
    height: getHeight(150),
    ...styles.mv10,
  },
  map: {
    ...styles.flex,
  },
  confirmButtonContainer: {
    position: 'absolute',
    bottom: 16,
    alignSelf: 'center',
  },
  confirmButton: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 8,
  },
  confirmButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});
