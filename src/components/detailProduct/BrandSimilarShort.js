import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';

// Custom Imports
import numeral from 'numeral';
import {LikeWithBg, UnLikeWithBg} from '../../assets/svgs';
import CText from '../common/CText';
import {deviceWidth, moderateScale} from '../../common/constants';
import {styles} from '../../themes';

export default function BrandSimilarShort(props) {
  const colors = useSelector(state => state.theme.theme);
  const [isLiked, setIsLiked] = useState(false);
  const {item, index, onPress} = props;
  const onPressLike = () => setIsLiked(!isLiked);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[localStyles.productContainer, styles.mr5]}>
      <TouchableOpacity style={localStyles.likeContainer} onPress={onPressLike}>
        {isLiked ? <LikeWithBg /> : <UnLikeWithBg />}
      </TouchableOpacity>
      <Image
        source={{uri: `http://192.168.2.2:8033/${item?.Brand?.brand_logo}`}}
        style={[
          localStyles.brandContainer,
          {backgroundColor: colors.dark ? colors.imageBg : colors.grayScale1},
        ]}
      />
      <Image
        source={{uri: `http://192.168.2.2:8010/${item?.images[0]}`}}
        style={[
          localStyles.productImageStyle,
          {backgroundColor: colors.dark ? colors.imageBg : colors.grayScale1},
        ]}
      />
      <CText style={[styles.flex, styles.mt10]} numberOfLines={1} type={'b16'}>
        {item?.name}
      </CText>
      <View style={localStyles.subItemStyle}>
        <CText type={'b14'}>₮{numeral(item?.sale_price).format('0,0')}</CText>
        {item?.sale_price !== item?.regular_price ? (
          <CText
            type={'r14'}
            style={[localStyles.textCross, {color: colors.redColor}]}>
            ₮{numeral(item?.regular_price).format('0,0')}
          </CText>
        ) : null}
      </View>
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    width: (deviceWidth - moderateScale(50)) / 2,
    ...styles.mt5,
  },
  subItemStyle: {
    ...styles.mt5,
    ...styles.ph5,
    ...styles.mb5,
    ...styles.flexRow,
    ...styles.justifyBetween,
  },
  starStyle: {
    width: moderateScale(20),
    height: moderateScale(20),
    resizeMode: 'contain',
    ...styles.mr5,
  },
  paidContainer: {
    ...styles.ph10,
    ...styles.pv5,
    borderRadius: moderateScale(6),
  },
  productImageStyle: {
    width: (deviceWidth - moderateScale(50)) / 2,
    height: (deviceWidth - moderateScale(50)) / 2,
    borderRadius: moderateScale(15),
    resizeMode: 'contain',
    ...styles.selfCenter,
  },
  likeContainer: {
    position: 'absolute',
    top: moderateScale(10),
    right: moderateScale(10),
    zIndex: 1,
  },
  brandContainer: {
    width: moderateScale(50) / 2,
    height: moderateScale(50) / 2,
    position: 'absolute',
    top: moderateScale(10),
    left: moderateScale(10),
    zIndex: 1,
    borderRadius: moderateScale(5),
  },
  textCross: {
    textDecorationLine: 'line-through',
  },
});
