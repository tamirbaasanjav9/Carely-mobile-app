import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Custom Imports

import {moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import CText from '../common/CText';
import {StackNav} from '../../navigation/NavigationKeys';
import {useNavigation} from '@react-navigation/native';

export default function ShopButton(props) {
  const {Shop, titlePlus} = props;
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const onPressAddress = () => {
    navigation.navigate(StackNav.ShopDetail, {item: Shop});
  };
  return (
    <TouchableOpacity
      onPress={onPressAddress}
      style={[
        localStyles.addressContainer,
        {backgroundColor: colors.dark ? colors.inputBg : colors.grayScale1},
      ]}>
      <View style={localStyles.innerContainer}>
        <Image
          source={{uri: `http://192.168.2.2:8010/${Shop?.images[0]}`}}
          style={localStyles.userImageStyle}
        />
        <View style={localStyles.defaultTextContainer}>
          <View style={localStyles.titleStyle}>
            <CText type={'B18'}>{Shop?.shop_name}</CText>
          </View>
          <CText type={'r14'} style={styles.mt2}>
            {titlePlus}:{'  '}
            {Shop?.product_count}
          </CText>
        </View>
      </View>
      <Ionicons
        name={'caret-forward-outline'}
        size={moderateScale(20)}
        color={colors.dark ? colors.white : colors.black}
      />
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  addressContainer: {
    ...styles.p10,
    ...styles.mv15,
    ...styles.rowSpaceBetween,
    borderRadius: moderateScale(15),
    ...styles.shadowStyle,
  },
  defaultTextContainer: {
    ...styles.mh10,
    ...styles.flex,
  },
  defaultContainer: {
    ...styles.ml10,
    ...styles.selfStart,
    ...styles.ph10,
    ...styles.pv5,
    borderRadius: moderateScale(6),
  },
  titleStyle: {
    ...styles.flexRow,
    ...styles.flex,
  },
  innerContainer: {
    ...styles.rowCenter,
    ...styles.flex,
  },
  userImageStyle: {
    ...styles.mr8,
    width: moderateScale(35),
    height: moderateScale(35),
    borderRadius: moderateScale(5),
  },
});
