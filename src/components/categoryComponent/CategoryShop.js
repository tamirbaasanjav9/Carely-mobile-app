// Library Imports
import {Image, StyleSheet, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// Custom Imports
import CText from '../common/CText';
import strings from '../../i18n/strings';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {styles} from '../../themes';
import {StackNav} from '../../navigation/NavigationKeys';
import {FlashList} from '@shopify/flash-list';
import {useState} from 'react';
import {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {AuthContext} from '../../contexts/authContext';
import {useContext} from 'react';
import {FlatList} from 'react-native';

export default function CategoryShop(props) {
  const {item, isCompleted = false, onPressComplete, shop_id} = props;
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const [extraData, setExtraData] = useState(true);
  const [shopList, setShopList] = useState();
  console.log(shopList);
  const {
    handlers: {POSTPRODUCT},
  } = useContext(AuthContext);
  useEffect(() => {
    setExtraData(!extraData);
    getShop();
  }, [colors]);
  const onPressBtn = () => {
    if (isCompleted) {
      onPressComplete(item);
    } else {
      navigation.navigate(StackNav.TrackOrder, {item: item});
    }
  };

  const getShop = async () => {
    const body = {
      filter: [
        {
          field_name: 'ref_id',
          operation: '=',
          value: `${shop_id}`,
        },
      ],
    };
    try {
      await POSTPRODUCT('/get_active_shop_list', false, body)
        .then(res => {
          setShopList(res?.data);
        })
        .catch(err => {
          return err;
        });
    } catch (error) {
      return;
    }
  };
  const onPressSearch = item =>
    navigation.navigate(StackNav.ProductCategory, {item: item});
  const renderItem = ({item}) => {
    console.log(item);
    const {images, shop_name} = item;
    return (
      <TouchableOpacity
        onPress={() => onPressSearch(item)}
        style={[localStyles.productContainer]}>
        <Image
          source={{
            uri: `http://192.168.2.2:8010/${images[0]}`,
          }}
          style={[
            localStyles.productImageStyle,
            {backgroundColor: colors.dark ? colors.imageBg : colors.white},
          ]}
        />
        <View style={[styles.mt15]}>
          <CText numberOfLines={1} type={'b13'}>
            {shop_name}
          </CText>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View>
      <FlatList
        data={shopList?.data}
        extraData={extraData}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        horizontal
        estimatedItemSize={10}
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    ...styles.mt15,
    ...styles.m5,
    ...styles.columnCenter,
    borderRadius: moderateScale(20),
    ...styles.selfCenter,
    width: moderateScale(130),
    height: moderateScale(150),
  },
  productImageStyle: {
    height: moderateScale(130),
    width: moderateScale(130),
    borderRadius: moderateScale(20),
    resizeMode: 'contain',
  },
});
