// Library Imports
import {Image, StyleSheet, View} from 'react-native';
import React, {useCallback} from 'react';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// Custom Imports
import CText from '../common/CText';
import strings from '../../i18n/strings';
import {deviceWidth, getHeight, moderateScale} from '../../common/constants';
import {colors, styles} from '../../themes';
import {StackNav} from '../../navigation/NavigationKeys';
import {FlashList} from '@shopify/flash-list';
import {useState} from 'react';
import {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import images from '../../assets/images';

export default function CategoryListChild(props) {
  const {item} = props;
  const dataArray = item;
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);
  const [extraData, setExtraData] = useState(true);
  useEffect(() => {
    setExtraData(!extraData);
  }, [colors]);
  const onPressCategoryChild = item =>
    navigation.navigate(StackNav.Search, {item: item});

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => onPressCategoryChild(item)}
        style={[
          localStyles.productContainer,
          {
            backgroundColor: colors.dark
              ? colors.imageBg
              : colors.carelyLogoColor,
          },
        ]}>
        <View style={[{...styles.ph5}]}>
          <CText numberOfLines={1} type={'b13'}>
            {item?.name}
          </CText>
        </View>
        <Image
          source={images.categoryImage}
          style={[localStyles.productImageStyle]}
        />
      </TouchableOpacity>
    );
  };
  return (
    <View>
      <FlashList
        data={dataArray}
        extraData={extraData}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        estimatedItemSize={10}
        numColumns={2}
        // horizontal
      />
    </View>
  );
}

const localStyles = StyleSheet.create({
  productContainer: {
    ...styles.mr5,
    ...styles.columnCenter,
    width: moderateScale(130),
    height: moderateScale(160),
    borderRadius: moderateScale(20),
  },
  productImageStyle: {
    height: moderateScale(130),
    width: moderateScale(130),
    resizeMode: 'contain',
  },

  //
});
