//Library Imports
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import {useSelector} from 'react-redux';
import {moderateScale} from '../../common/constants';
import {colors} from '../../themes';

//Local Imports

export default function CDropDown({
  data,
  title,
  type,
  color,
  onPress,
  containerStyle,
  style,
  icon = null,
  frontIcon = null,
  bgColor = null,
  children,
  labelField,
  valueField,
  isFocus = false,
  ...props
}) {
  return (
    <Dropdown
      style={[
        localStyles.dropdown,
        isFocus && {borderColor: commonColor.carelyLogoColor},
      ]}
      placeholderStyle={localStyles.placeholderStyle}
      selectedTextStyle={localStyles.selectedTextStyle}
      inputSearchStyle={localStyles.inputSearchStyle}
      iconStyle={localStyles.iconStyle}
      data={data}
      search
      maxHeight={300}
      labelField={labelField}
      valueField={valueField}
      placeholder={!isFocus ? 'Байршил сонгох' : '...'}
      searchPlaceholder="Хайх..."
      value={value}
      onFocus={() => setIsFocus(true)}
      onBlur={() => setIsFocus(false)}
      onChange={item => {
        setValue(item.id);
        handleDistrict(item?.id);
        setIsFocus(false);
        districtState.setTrue();
      }}
      renderLeftIcon={() => (
        <EvilIcons
          style={{...styles.mr10}}
          color={commonColor.carelyLogoColor}
          name="location"
          size={moderateScale(25)}
        />
      )}
      renderItem={renderItem}
    />
  );
}

const localStyles = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundColor,
    borderRadius: moderateScale(10),
  },
  dropdown: {
    height: moderateScale(50),
    borderColor: commonColor.grayScale5,
    borderWidth: 1,
    borderRadius: moderateScale(10),
    ...styles.ph10,
    ...styles.mb25,
  },
  placeholderStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  selectedTextStyle: {
    fontSize: 16,
    fontWeight: '400',
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: moderateScale(40),
    fontSize: 16,
  },
  item: {
    ...styles.rowSpaceBetween,
    ...styles.p15,
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
});
