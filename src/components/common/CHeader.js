import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {styles} from '../../themes';
import {useNavigation} from '@react-navigation/native';
import CText from './CText';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import {moderateScale} from '../../common/constants';

export default function CHeader(props) {
  const {title, total, onPressBack, rightIcon, isHideBack, isLeftIcon, type} =
    props;
  const navigation = useNavigation();
  const colors = useSelector(state => state.theme.theme);

  const goBack = () => navigation.goBack();
  return (
    <View style={[localStyles.container, !!isHideBack && styles.pr10]}>
      <View style={[styles.rowStart, styles.flex]}>
        {!isHideBack && (
          <TouchableOpacity
            style={[
              styles.mr10,
              {
                borderRadius: 10,
                borderWidth: 0.5,
                borderColor: colors.textColor,
                padding: 3,
              },
            ]}
            onPress={onPressBack || goBack}>
            <Ionicons
              name="arrow-back-outline"
              size={moderateScale(20)}
              color={colors.textColor}
            />
          </TouchableOpacity>
        )}
        {!!isLeftIcon && isLeftIcon}

        {!!total ? (
          <View style={[styles.flexColumn, styles.flex]}>
            <CText
              numberOfLines={1}
              style={[styles.pr10, styles.mr10]}
              type={'b14'}>
              {title}
            </CText>
            <CText
              numberOfLines={1}
              style={[styles.pr10, styles.mr10]}
              type={'b12'}>
              {total}
            </CText>
          </View>
        ) : (
          <CText
            numberOfLines={1}
            style={[styles.pr10, styles.mr10]}
            type={'B15'}>
            {title}
          </CText>
        )}
      </View>
      {!!rightIcon && rightIcon}
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    ...styles.rowSpaceBetween,
    ...styles.ph15,
    ...styles.pv15,
    ...styles.center,
  },
});
