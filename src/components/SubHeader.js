import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {commonColor, styles} from '../themes';
import CText from './common/CText';
import {useSelector} from 'react-redux';

function SubHeader({
  title1,
  title2,
  onPressSeeAll,
  style,
  type,
  header = false,
}) {
  const colors = useSelector(state => state.theme.theme);

  return (
    <View style={[localStyles.root, {...style}]}>
      <>
        {!!header && (
          <View
            style={[
              localStyles.paragraph,
              {
                borderColor: colors.dark
                  ? colors.carelyLogoColor
                  : colors.carelyLogoColor,
              },
            ]}>
            <CText type={type ? type : 'b16'} style={[styles.flex]}></CText>
          </View>
        )}
        <CText type={type ? type : 'b16'} style={[styles.flex]}>
          {title1}
        </CText>
      </>
      {!!title2 && (
        <TouchableOpacity onPress={onPressSeeAll}>
          <CText
            type={'M14'}
            color={commonColor.carelyLogoColor}
            style={[styles.flex]}>
            {title2}
          </CText>
        </TouchableOpacity>
      )}
    </View>
  );
}

export default React.memo(SubHeader);

const localStyles = StyleSheet.create({
  root: {
    ...styles.rowSpaceBetween,
    ...styles.mv15,
  },
  paragraph: {
    borderLeftWidth: 3,
    ...styles.mr10,
  },
});
